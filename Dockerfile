#FROM golang:1.20.1 AS builder
FROM registry.fabmation.info/20042/oci-images/golang:v1.20-default AS builder

WORKDIR /app

ENV GONOSUMDB="gitlab.fabmation.info/*"
ENV GOPROXY="https://athens.clu"
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64

COPY go.mod ./
COPY go.sum ./
RUN go mod download -x \
    && go install -v github.com/hibiken/asynq/tools/asynq@latest

COPY . ./
RUN --mount=type=cache,target=/root/.cache/go-build \
    go build -ldflags="-extldflags=-static" -o /bookmarker .

#######################
##    Prod. Stage    ##
#######################

FROM alpine:3.17


COPY ./migrations ./migrations
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /bookmarker /bookmarker
COPY --from=builder /go/bin/asynq /asynq

COPY ./entrypoint.sh /

RUN apk add bash

VOLUME [ "/tmp" ]

ENTRYPOINT ["/entrypoint.sh"]
CMD [ "/bookmarker", "serve" ]
