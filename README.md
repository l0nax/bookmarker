## Installation

## DB Requirements

```bash
CREATE EXTENSION IF NOT EXISTS pgroonga;
```

## License

The license can be found in the [LICENSE](./LICENSE) file.

NOTE: Our `pkg/safeurl` package is based on https://github.com/doyensec/safeurl.
We had to do some customizations and performance optimizations which are specific for our needs.
