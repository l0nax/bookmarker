package apiv1

import (
	"net/http"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"

	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

// CreateAnnotation is the annotation create handler.
func CreateAnnotation(c *fiber.Ctx) error {
	var anot models.Annotation

	if err := c.BodyParser(&anot); err != nil {
		return rerr.InternalServerError.With(err)
	}

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.QuoteMarking) {
		return rerr.FeatureNotAvail
	}

	anot.OwnerID = user.ID
	if !anot.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation failed")
	}

	if err := anot.Create(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{
		"status": "ok",
	})
}

func UpdateAnnotation(c *fiber.Ctx) error {
	var anot models.Annotation

	if err := c.BodyParser(&anot); err != nil {
		return rerr.InternalServerError.With(err)
	}

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.QuoteMarking) {
		return rerr.FeatureNotAvail
	}

	anot.OwnerID = user.ID
	if !anot.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation failed")
	}

	if err := anot.Update(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{
		"status": "ok",
	})
}

// GetAnnotations is the list annotation handler
func GetAnnotations(c *fiber.Ctx) error {
	var req v1.AnnotationGetReq

	if err := c.BodyParser(&req); err != nil {
		return rerr.InternalServerError.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("request body invalid")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.QuoteMarking) {
		return rerr.FeatureNotAvail
	}

	anot, err := models.GetAnnotationsOfURL(ctx, req.PageURL, user.ID)
	if err != nil {
		if models.IsNoRows(err) {
			// TODO: Return no data found
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(anot)
}

// GetAnnotationByURL is the annotation received endpoint.
func GetAnnotationByURL(c *fiber.Ctx) error {
	var req v1.AnnotationIDGetReq

	if err := c.BodyParser(&req); err != nil {
		return rerr.InternalServerError.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("request body invalid")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.QuoteMarking) {
		return rerr.FeatureNotAvail
	}

	anot, err := models.GetAnnotationsOfIDURL(ctx, req.PageURL, user.ID)
	if err != nil {
		if models.IsNoRows(err) {
			// TODO: Return no data found
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(anot)
}

func DeleteAnnotation(c *fiber.Ctx) error {
	var req v1.AnnotationIDDeleteReq

	if err := c.BodyParser(&req); err != nil {
		return rerr.InternalServerError.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("request body invalid")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.QuoteMarking) {
		return rerr.FeatureNotAvail
	}

	err := models.DeleteAnnotationByURL(ctx, user.ID, req.URL)
	if err != nil {
		if !models.IsNoRows(err) {
			return rerr.InternalServerError.With(err)
		}
	}

	return c.SendStatus(http.StatusNoContent)
}
