package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/middleware"
)

// AddApiV1 will add all v1 API handlers to the Mux router.
// Requires that the Mux router adds this function via PathPrefix()
func AddApiV1(r fiber.Router) {
	r.Post("/register", RegisterHandler)
	middleware.RegisterAnonymousRoute("/api/v1/register")

	r.Post("/system/user/self/extension_token", CreateExtensionToken)
	r.Get("/system/info", GetSystemInfo)
	r.Get("/system/user/subscription", GetUserSubscriptionInfo)
	r.Get("/system/user/token", GetUserTokens)
	r.Delete("/system/user/token/:id", RevokeUserToken)

	r.Post("/bookmarks", AddBookmarkEntry)
	r.Post("/bookmarks/-/check", CheckBookmark)
	r.Put("/bookmarks", UpdateBookmark)
	r.Get("/bookmarks/-/broken", GetBrokenBookmarks)
	r.Get("/bookmarks/:id/thumbnail", GetBookmarkThumbnail)
	r.Delete("/bookmarks/:id", DeleteBookmark)

	r.Post("/collections", AddCollection)
	r.Get("/collections", ListCollection)
	r.Delete("/collections/:id", DeleteCollection)
	r.Put("/collections/:id", UpdateCollection)
	r.Get("/collections/-/merge_suggestions", GetMergeSuggestions)
	r.Put("/collections/-/merge_suggestions", UpdateMergeSuggestion)
	r.Post("/collections/-/merge_suggestions", MergeSuggestions)
	r.Post("/collections/-/merge_suggestions/-/reset", ResetMergeSuggestions)

	r.Get("/collections/:id/bookmarks", GetBookmarksOfCollection)

	r.Post("/import", ImportBookmarks)

	r.Get("/search", SimpleSearchBookmark)

	r.Post("/annotation", CreateAnnotation)
	r.Put("/annotation", UpdateAnnotation)
	r.Delete("/annotation", DeleteAnnotation)
	r.Post("/annotation/-/list", GetAnnotations)
	r.Post("/annotation/-/by_url", GetAnnotationByURL)

	r.Get("/dashboard/stats", GetDashboardStats)
}
