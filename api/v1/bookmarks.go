package apiv1

import (
	"context"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/zeebo/xxh3"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/internal/cache"
	"gitlab.com/l0nax/bookmarker/internal/s3"
	"gitlab.com/l0nax/bookmarker/models"
	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
	"gitlab.com/l0nax/bookmarker/pkg/importer/chrome"
	"gitlab.com/l0nax/bookmarker/pkg/importer/webext"
	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

func AddBookmarkEntry(c *fiber.Ctx) error {
	var b v1.BookmarkSimple

	if err := c.BodyParser(&b); err != nil {
		return rerr.InternalServerError.With(err)
	} else if !b.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation failed")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	// TODO: Add check if provided collection_id is owned/ accessible by the user.

	bm := b.ToModel()
	bm.OwnerID = user.ID

	if err := bm.Create(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{
		"status": "ok",
	})
}

func ListBookmarks(c *fiber.Ctx) error {
	return nil
}

func CheckBookmark(c *fiber.Ctx) error {
	var req v1.BookmarkCheck

	if err := c.BodyParser(&req); err != nil {
		return rerr.InternalServerError.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation failed")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	b, err := models.GetBookmarkByURL(ctx, user.ID, req.URL)
	if err != nil {
		if models.IsNoRows(err) {
			return c.SendStatus(http.StatusNotFound)
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(b)
}

func UpdateBookmark(c *fiber.Ctx) error {
	user := getEntity(c)
	ctx := c.UserContext()

	var req models.Bookmark

	if err := c.BodyParser(&req); err != nil {
		return rerr.RequestMalformed.With(err)
	}

	if err := req.UpdateForUser(ctx, user.ID); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{})
}

func GetBookmarksOfCollection(c *fiber.Ctx) error {
	colID, err := ParseUUID(c, "id")
	if err != nil {
		return err
	}

	ctx := c.UserContext()
	user := getEntity(c)

	col, err := models.GetCollectionByID(ctx, user.ID, colID)
	if err != nil {
		if models.IsNoRows(err) {
			return rerr.EntityNotFound.WithLogMsg("collection not known")
		}

		return rerr.InternalServerError.With(err)
	}

	b, err := models.GetBookmarksOfCollection(ctx, user.ID, colID, nil)
	if err != nil {
		if models.IsNoRows(err) {
			return c.JSON([]models.Bookmark{})
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(v1.CollectionBookmarks{
		ID:        col.ID,
		CreatedAt: col.CreatedAt,
		UpdatedAt: col.UpdatedAt,
		OwnerID:   col.OwnerID,
		ParentID:  col.ParentID,
		Title:     col.Title,
		Bookmarks: b,
	})
}

func ImportBookmarks(c *fiber.Ctx) error {
	switch strings.ToLower(c.Query("browser")) {
	case "chrome":
		return importChromeBookmarks(c)
	case "webext":
		return importWebExtBookmarks(c)
	default:
		return rerr.RequestMalformed.WithLogMsg("unsupported browser")
	}
}

func importChromeBookmarks(c *fiber.Ctx) error {
	const maxSize = 1024 * 1024 * 100 // 100 MiB

	ctx := c.UserContext()
	user := getEntity(c)

	file, err := c.FormFile("bookmark-file")
	if err != nil {
		return rerr.RequestMalformed.With(err).WithLogMsg("file not found")
	} else if file.Size > maxSize {
		return rerr.RequestMalformed.WithLogMsg("file too big")
	}

	f, err := file.Open()
	if err != nil {
		return rerr.InternalServerError.With(err)
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return rerr.RequestMalformed.With(err).WithLogMsg("unable to read file")
	}

	var b chrome.BookmarkRoot

	if err := json.Unmarshal(data, &b); err != nil {
		return rerr.RequestMalformed.With(err)
	}

	if err := chrome.Import(ctx, user.ID, b); err != nil {
		return rerr.InternalServerError.With(err)
	}

	if err := models.RecalcCollectionAuths(ctx, user.ID); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{"status": "ok"})
}

func importWebExtBookmarks(c *fiber.Ctx) error {
	const maxSize = 1024 * 1024 * 100 // 100 MiB

	ctx := c.UserContext()
	user := getEntity(c)

	file, err := c.FormFile("bookmark-file")
	if err != nil {
		return rerr.RequestMalformed.With(err).WithLogMsg("missing file")
	} else if file.Size > maxSize {
		return rerr.RequestMalformed.WithLogMsg("file too big")
	}

	f, err := file.Open()
	if err != nil {
		return rerr.InternalServerError.With(err)
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return rerr.RequestMalformed.With(err).WithLogMsg("unable to read file")
	}

	b := make([]webext.Entry, 0, 1)

	if err := json.Unmarshal(data, &b); err != nil {
		return rerr.RequestMalformed.With(err).WithLogMsg("unable to decode message")
	}

	if err := webext.Import(ctx, user.ID, b); err != nil {
		return rerr.InternalServerError.With(err)
	}

	// XXX: Remove me!
	time.Sleep(10 * time.Millisecond)

	t, err := tasks.NewRecalcCollectionAuth(user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err).WithLogMsg("unable to create auth recalc task")
	}

	if err := task.Enqueue(t); err != nil {
		return rerr.InternalServerError.With(err).WithLogMsg("unable to enqueue new task")
	}

	if err := models.RecalcCollectionAuths(ctx, user.ID); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{"status": "ok"})
}

func GetBookmarkThumbnail(c *fiber.Ctx) error {
	bid, err := ParseUUID(c, "id")
	if err != nil {
		return rerr.RequestMalformed.With(err)
	}

	asBase64 := c.Query("as_base64", "true") == "true"

	ctx := c.UserContext()
	user := getEntity(c)

	exists, err := checkBookmarkThumbnailExistence(ctx, user.ID, bid)
	if err != nil {
		return err
	} else if !exists {
		// NOTE: We already scheduled a fetch
		return rerr.EntityNotFound.WithLogMsg("fetching scheduled")
	}

	asset, err := models.GetBookmarkThumbnail(ctx, user.ID, bid)
	if err != nil {
		if models.IsNoRows(err) {
			return c.SendStatus(404)
		}

		return rerr.InternalServerError.With(err)
	}

	etag := c.Get("ETag")
	if etag != "" {
		uptoDate, err := tryCachedThubnail(c, bid, etag, "")
		if err != nil {
			return rerr.InternalServerError.With(err)
		} else if uptoDate {
			return nil
		}
	}

	pref := s3.GenThumbnailPref(asset.CreatedAt)

	tn, err := s3.GetThumbnail(ctx, bid, pref)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	etag = calcThumbnailETag(bid, asset.CreatedAt)
	cache.SetThumbnailETag(ctx, bid, etag, "")
	c.Set(fiber.HeaderETag, strconv.Quote(etag))
	c.Set(fiber.HeaderCacheControl, "max-age="+strconv.FormatInt(int64((time.Hour*24*14).Seconds()), 10))

	if asBase64 {
		c.Set(fiber.HeaderContentType, "text/plain")

		return c.Send([]byte(base64.StdEncoding.EncodeToString(tn)))
	}

	c.Set(fiber.HeaderContentType, "image/png")

	return c.Send(tn)
}

func checkBookmarkThumbnailExistence(ctx context.Context, userID, bookmarkID uuid.UUID) (bool, error) {
	b, err := models.GetBookmarkByIDAuth(ctx, userID, bookmarkID)
	if err != nil {
		if models.IsNoRows(err) {
			return false, rerr.EntityNotFound
		}

		return false, rerr.InternalServerError.With(err)
	}

	if b.ThumbnailID.Valid {
		return true, nil
	}

	t, err := tasks.NewThumbnailFetchTask(b)
	if err != nil {
		return false, rerr.InternalServerError.With(err).WithLogMsg("unable to create task")
	}

	if err := task.Enqueue(t); err != nil {
		if errors.Is(err, asynq.ErrDuplicateTask) {
			// We already enqueued a task
			return false, nil
		}

		return false, rerr.InternalServerError.With(err)
	}

	return false, nil
}

// calcThumbnailETag returns the ETag of the provided thumbnail data.
// The assetCreated parameter is the reference parameter meaning that, if e.g. the user provided
// a target size, the original asset (and created_at timestamp) will not change.
func calcThumbnailETag(bookmarkID uuid.UUID, assetCreated time.Time) string {
	h := xxh3.New()
	h.WriteString(bookmarkID.String())
	h.WriteString(s3.GenThumbnailPref(assetCreated))
	sum := h.Sum128().Bytes()

	return hex.EncodeToString(append([]byte{sum[0]}, sum[1:]...))
}

// tryCachedThubnail checks whether the provided ETag is known is and not stale.
// It returns true if the provided information is still up-to-date.
//
// If it's up-to-date, the function automatically returns 304 to the client.
func tryCachedThubnail(c *fiber.Ctx, userID uuid.UUID, etag, size string) (bool, error) {
	ctx := c.UserContext()

	isLatest := cache.ExistsThumbnailETag(ctx, userID, etag, size)
	if isLatest {
		return true, c.SendStatus(fiber.StatusNotModified)
	}

	return false, nil
}

func GetBrokenBookmarks(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	b, err := models.GetBrokenBookmarksOfUser(ctx, user.ID)
	if err != nil {
		if models.IsNoRows(err) {
			return c.JSON([]string{})
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(v1.BrokenBookmarksFromModels(b))
}

func DeleteBookmark(c *fiber.Ctx) error {
	bid, err := ParseUUID(c, "id")
	if err != nil {
		return err
	}

	ctx := c.UserContext()
	user := getEntity(c)

	err = models.DeleteBookmark(ctx, user.ID, bid)
	if err != nil {
		if models.IsNoRows(err) {
			return rerr.EntityNotFound
		}

		return rerr.InternalServerError.With(err)
	}

	return c.SendStatus(http.StatusNoContent)
}
