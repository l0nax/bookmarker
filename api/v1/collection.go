package apiv1

import (
	"context"
	"encoding/hex"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/samber/lo"
	"github.com/zeebo/xxh3"
	"go.uber.org/zap"

	"gitlab.fabmation.info/20043/errors"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/internal/cache"
	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

func AddCollection(c *fiber.Ctx) error {
	var col v1.Collection

	if err := c.BodyParser(&col); err != nil {
		return rerr.RequestMalformed.With(err)
	} else if !col.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation of collection failed")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	// TODO: Validate if user can access parent_id

	collection := col.ToModel()
	collection.OwnerID = user.ID

	if err := collection.Create(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	if err := scheduleUserColAuthRecalc(user.ID); err != nil {
		// TODO: Rollback
		return err
	}

	// invalidate user collection list cache
	cache.DeleteUserCollectionList(ctx, user.ID)

	return c.JSON(collection)
}

func ListCollection(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	etag := getClientETag(c)
	if etag != "" {
		uptoDate, err := tryCachedUserCollectionList(c, user.ID, etag)
		if err != nil {
			return rerr.InternalServerError.With(err)
		} else if uptoDate {
			return nil
		}
	}

	fmt.Printf("==> %s\n", etag)

	if opt := c.Query("as_simple_tree"); opt == "true" {
		return listCollectionSimpleTree(c, ctx, user)
	}

	ret, found := cache.GetUserCollectionList(ctx, user.ID, false)
	if found {
		c.Set(fiber.HeaderETag, strconv.Quote(ret.ETag))

		return c.JSON(ret.Collections)
	}

	cols, err := models.GetAllCollections(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	etag = calcUserCollectionListETag(user.ID)
	cache.SetCollectionListETag(ctx, user.ID, etag)
	cache.SetUserCollectionList(ctx, user.ID, cache.MetaCollectionList{
		ETag:        etag,
		Collections: cols,
	}, false)

	fmt.Printf("Storing ETag: %q\n", etag)

	return c.JSON(cols)
}

func listCollectionSimpleTree(c *fiber.Ctx, ctx context.Context, user models.User) error {
	ret, found := cache.GetUserCollectionList(ctx, user.ID, true)
	if found {
		c.Set(fiber.HeaderETag, strconv.Quote(ret.ETag))

		return c.JSON(ret.Collections)
	}

	cols, err := models.GetAllCollectionsSimpleTree(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	etag := calcUserCollectionListETag(user.ID)
	cache.SetCollectionListETag(ctx, user.ID, etag)
	cache.SetUserCollectionList(ctx, user.ID, cache.MetaCollectionList{
		ETag:        etag,
		Collections: cols,
	}, true)

	fmt.Printf("Storing ETag: %q\n", etag)

	return c.JSON(cols)
}

func DeleteCollection(c *fiber.Ctx) error {
	colID, err := ParseUUID(c, "id")
	if err != nil {
		return rerr.RequestMalformed.With(err)
	}

	ctx := c.UserContext()
	user := getEntity(c)

	col, err := models.GetCollectionByID(ctx, user.ID, colID)
	if err != nil {
		return handleNoRows(err)
	}

	// TODO: Check instead for access_level
	if col.OwnerID != user.ID {
		return rerr.Unauthorized.WithLogMsg("requestee is not the owner of the collection")
	}

	if err := col.Delete(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	if err := models.DeleteCMSRefByIDOfUser(ctx, colID, user.ID); err != nil {
		if !models.IsNoRows(err) {
			log.Error("Unable to delete CMS ref of user",
				zap.Stringer("collection_id", colID), zap.Stringer("user_id", user.ID), zap.Error(err))
		}
	}

	// invalidate user collection list cache
	cache.DeleteUserCollectionList(ctx, user.ID)

	return c.JSON(fiber.Map{
		"status": "ok",
	})
}

func UpdateCollection(c *fiber.Ctx) error {
	colID, err := ParseUUID(c, "id")
	if err != nil {
		return rerr.RequestMalformed.With(err)
	}

	var req v1.CollectionUpdateRequest

	if err := c.BodyParser(&req); err != nil {
		return rerr.RequestMalformed.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.With(err).WithLogMsg("data not valid")
	}

	ctx := c.UserContext()
	user := getEntity(c)

	col, err := models.GetCollectionByID(ctx, user.ID, colID)
	if err != nil {
		return handleNoRows(err)
	}

	// TODO: Check instead for access_level
	if col.OwnerID != user.ID {
		return rerr.Unauthorized.WithLogMsg("requestee is not the owner of the collection")
	}

	mc := req.ToModel()
	mc.ID = col.ID

	if err := mc.Update(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	// invalidate user collection list cache
	cache.DeleteUserCollectionList(ctx, user.ID)

	return c.JSON(mc)
}

func scheduleUserColAuthRecalc(userID uuid.UUID) error {
	t, err := tasks.NewRecalcCollectionAuth(userID)
	if err != nil {
		return rerr.InternalServerError.With(err).WithLogMsg("unable to create task")
	}

	if err := task.Enqueue(t); err != nil {
		if errors.Is(err, asynq.ErrDuplicateTask) {
			return nil
		}

		return rerr.InternalServerError.With(err).WithLogMsg("unable to enqueue task")
	}

	return nil
}

func GetMergeSuggestions(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.CollectionMergeSuggestion) {
		return rerr.FeatureNotAvail
	}

	showDismissed := c.Query("show_dismissed", "false") == "true"

	sug, err := models.GetCollectionMergeSuggestions(ctx, user.ID, showDismissed)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	ret := v1.CollectionMergeSuggestionList{
		Suggestions: sug,
		Mapping:     make(map[uuid.UUID]string, len(sug)),
	}

	for i := range sug {
		tmp, err := models.ResolveCollectionsToFullPath(ctx, user.ID, sug[i].CollectionIDs)
		if err != nil {
			if !models.IsNoRows(err) {
				return rerr.InternalServerError.With(err)
			}
		}

		ret.Mapping = lo.Assign(ret.Mapping, tmp)
	}

	return c.JSON(ret)
}

func ResetMergeSuggestions(c *fiber.Ctx) error {
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.CollectionMergeSuggestion) {
		return rerr.FeatureNotAvail
	}

	resetAll := c.Query("reset_all", "false") == "true"

	t, err := tasks.NewCollectionMergeSuggestionTask(user.ID, resetAll)
	if err != nil {
		log.Error("Unable to create CollectionMergeSuggestionTask", zap.Stringer("user_id", user.ID), zap.Error(err))

		return nil
	}

	if err := task.Enqueue(t); err != nil {
		log.Error("Unable enqueue CollectionMergeSuggestionTask", zap.Stringer("user_id", user.ID), zap.Error(err))
	}

	return c.SendStatus(http.StatusNoContent)
}

func UpdateMergeSuggestion(c *fiber.Ctx) error {
	user := getEntity(c)
	ctx := c.UserContext()

	if !license.FeatureAvailable(user.ID, feature.CollectionMergeSuggestion) {
		return rerr.FeatureNotAvail
	}

	var req v1.CMSUpdateRequest

	if err := c.BodyParser(&req); err != nil {
		return rerr.RequestMalformed.With(err)
	}

	if err := models.UpdateDismissCMSOfUser(ctx, user.ID, req.ID, req.Dismissed); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{})
}

func MergeSuggestions(c *fiber.Ctx) error {
	var req v1.CMSMergeRequest

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.CollectionMergeSuggestion) {
		return rerr.FeatureNotAvail
	}

	if err := c.BodyParser(&req); err != nil {
		return rerr.RequestMalformed.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation of request not passed")
	}

	tx, err := models.StartTx(ctx, nil)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}
	defer tx.Rollback()

	exists, err := models.DoesCMSExistByIDOfUser(ctx, req.ID, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	} else if !exists {
		return rerr.EntityNotFound
	}

	err = models.MergeCMSOfUser(ctx, req.Target, user.ID, req.Collections, tx)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	if err := models.DeleteCMSByIDOfUser(ctx, req.ID, user.ID, tx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	// TODO: Clear cache of entries => the concatenated paths may not be correct anymore

	if err := tx.Commit(); err != nil {
		return rerr.InternalServerError.With(err)
	}

	// invalidate user collection list cache
	cache.DeleteUserCollectionList(ctx, user.ID)

	return c.SendStatus(http.StatusNoContent)
}

func calcUserCollectionListETag(userID uuid.UUID) string {
	h := xxh3.New()
	h.WriteString(userID.String())
	h.WriteString(strconv.FormatInt(time.Now().UnixMilli(), 10))
	sum := h.Sum128().Bytes()

	return hex.EncodeToString(append([]byte{sum[0]}, sum[1:]...))
}

func tryCachedUserCollectionList(c *fiber.Ctx, userID uuid.UUID, etag string) (bool, error) {
	ctx := c.UserContext()

	isLatest := cache.ExistsCollectionListETag(ctx, userID, etag)
	if isLatest {
		return true, c.SendStatus(fiber.StatusNotModified)
	}

	return false, nil
}
