package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
)

func GetDashboardStats(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	var err error
	var resp v1.UserDashboardStats

	resp.CollectionMergeSuggestions, err = models.GetAvailMergeSuggestionsOfUser(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	resp.DeadLinks, err = models.GetDeadLinksOfUser(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(resp)
}
