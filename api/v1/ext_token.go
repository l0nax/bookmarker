package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
)

func CreateExtensionToken(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	if isToken(c) {
		// TODO: Send custom error
		return c.SendStatus(fiber.StatusUnauthorized)
	}

	tok, err := models.CreateToken(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	// TODO: Send user email notification!!

	return c.JSON(tok)
}
