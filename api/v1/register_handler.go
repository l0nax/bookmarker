package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
)

func RegisterHandler(c *fiber.Ctx) error {
	var req v1.RegisterReq

	if err := c.BodyParser(&req); err != nil {
		return rerr.RequestMalformed.With(err)
	} else if !req.IsValid() {
		return rerr.RequestMalformed.WithLogMsg("validation of body failed")
	}

	user := models.User{
		GivenName: req.GivenName,
		Surname:   req.Surname,
		Email:     req.Email,
		Password:  req.Password,
		Active:    true,
	}

	ctx := c.UserContext()

	if err := user.Create(ctx); err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(fiber.Map{
		"status": "ok",
	})
}
