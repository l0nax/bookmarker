package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/internal/search"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

func SimpleSearchBookmark(c *fiber.Ctx) error {
	q := c.Query("q")
	if q == "" {
		return rerr.RequestMalformed.WithLogMsg("missing search query")
	}

	// TODO: Implement rate limit

	ctx := c.UserContext()
	user := getEntity(c)

	if !license.FeatureAvailable(user.ID, feature.AdvancedSearch) {
		return rerr.FeatureNotAvail
	}

	ret, err := search.SearchForBookmark(ctx, user.ID, q)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	return c.JSON(ret)
}
