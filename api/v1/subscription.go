package apiv1

import (
	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
	v1 "gitlab.com/l0nax/bookmarker/pkg/api/v1"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

func GetSystemInfo(c *fiber.Ctx) error {
	resp := v1.SystemInfo{
		IsSaaS: license.FeatureGlobalAvailable(feature.SaasOffering),
	}

	return c.JSON(resp)
}

func GetUserSubscriptionInfo(c *fiber.Ctx) error {
	if !license.FeatureGlobalAvailable(feature.SaasOffering) {
		return rerr.FeatureNotAvail
	}

	ctx := c.UserContext()
	user := getEntity(c)

	sub, err := models.GetHighestSubscription(ctx, user.ID)
	if err != nil {
		return rerr.InternalServerError.With(err)
	}

	userSub := v1.UserSubscription{
		CurrentPlan: sub.PlanID,
		PeriodStart: sub.PeriodStart,
		PeriodEnd:   sub.PeriodEnd,
	}

	return c.JSON(userSub)
}
