package apiv1

import (
	"net/http"

	"github.com/gofiber/fiber/v2"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
)

func GetUserTokens(c *fiber.Ctx) error {
	ctx := c.UserContext()
	user := getEntity(c)

	t, err := models.GetTokensOfUser(ctx, user.ID)
	if err != nil {
		if models.IsNoRows(err) {
			return c.JSON([]string{})
		}

		return rerr.InternalServerError.With(err)
	}

	return c.JSON(t)
}

func RevokeUserToken(c *fiber.Ctx) error {
	tid, err := ParseUUID(c, "id")
	if err != nil {
		return err
	}

	ctx := c.UserContext()
	user := getEntity(c)

	err = models.RevokeTokenOfUser(ctx, user.ID, tid)
	if err != nil {
		if models.IsNoRows(err) {
			return c.SendStatus(http.StatusNoContent)
		}

		return rerr.InternalServerError.With(err)
	}

	return c.SendStatus(http.StatusNoContent)
}
