package apiv1

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.fabmation.info/20000/71168/fabguard-go"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/license"
)

func isToken(c *fiber.Ctx) bool {
	loc := c.Locals("AUTH_IS_TOKEN", "")

	return loc != ""
}

func getEntity(c *fiber.Ctx) models.User {
	return c.Locals("ENTITY").(models.User)
}

// ParseUUID parses the UUID provided by a parameter.
// The function parses the ID in a backward compatible manner, meaning integer values
// are supported.
func ParseUUID(c *fiber.Ctx, param string) (uuid.UUID, error) {
	rawID := c.Params(param)
	if rawID == "" {
		return uuid.UUID{}, rerr.RequestMalformed.WithLogMsg("invalid/ empty UUID provided")
	}

	id, err := uuid.Parse(rawID)
	if err == nil {
		return id, nil
	}

	return uuid.Nil, rerr.RequestMalformed.With(err)
}

func requiresFeature(f fabguard.LicenseFeatureFlag) func(*fiber.Ctx) error {
	return func(c *fiber.Ctx) error {
		if !license.FeatureGlobalAvailable(f) {
			return rerr.FeatureNotAvail
		}

		return c.Next()
	}
}

func handleNoRows(err error) error {
	if models.IsNoRows(err) {
		return rerr.EntityNotFound
	}

	return rerr.InternalServerError.With(err)
}

func getClientETag(c *fiber.Ctx) string {
	str := c.Get("If-None-Match")
	if str == "" || !strings.Contains(str, " ") {
		return strings.ReplaceAll(str, "\"", "")
	}

	return strings.ReplaceAll(strings.SplitN(str, " ", 1)[0], "\"", "")
}
