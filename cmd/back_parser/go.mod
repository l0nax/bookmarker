module gitlab.com/l0nax/bookmarker/cmd/back_parser

go 1.20

require (
	github.com/go-resty/resty/v2 v2.7.0
	github.com/google/uuid v1.3.0
	github.com/pkg/errors v0.9.1
)

require golang.org/x/net v0.0.0-20211029224645-99673261e6eb // indirect
