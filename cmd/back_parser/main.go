package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

// Backup location file
//
//  Linux: 		~/.config/google-chrome/Default/Bookmarks.bak
//  Windows: 	AppData\\Local\\Google\\Chrome\\User Data\\Google\\Chrome\\Bookmarks.bak
//  Mac OS: 	Library/Application Support/Google/Chrome/Default/Bookmarks.bak

type EntryType string

const (
	TypeFolder = EntryType("folder")
	TypeURL    = EntryType("url")
)

type Entry struct {
	Name     string    `json:"name,omitempty"`
	Type     EntryType `json:"type,omitempty"`
	URL      string    `json:"url,omitempty"`
	Children []Entry   `json:"children,omitempty"`
}

type BookmarkRoot struct {
	Roots map[string]Entry `json:"roots,omitempty"`
}

func main() {
	dat, err := os.ReadFile(os.Args[1])
	if err != nil {
		log.Fatalf("Unable to read file: %+v\n", err)
	}

	var b BookmarkRoot

	if err := json.Unmarshal(dat, &b); err != nil {
		log.Fatalf("Unable unmarshal backup file: %+v\n", err)
	}

	host := os.Getenv("BACK_HOST")

	c := resty.New().
		SetHostURL(host + "/api/v1").
		SetCloseConnection(false)

	c.SetBasicAuth(os.Getenv("BACK_USERNAME"), os.Getenv("BACK_PASSWORD"))

	for k, v := range b.Roots {
		id, err := createRootEntry(c, k)
		if err != nil {
			log.Fatalf("Unable to create root entry %q: %+v\n", k, err)
		}

		if err := createSubColls(c, id, v); err != nil {
			log.Fatalf("Unable to create sub cols of %q: %+v\n", k, err)
		}
	}
}

func createSubColls(c *resty.Client, parent uuid.UUID, e Entry) error {
	if e.Type == TypeURL {
		return addURL(c, parent, e.Name, e.URL)
	} else if e.Type != TypeFolder {
		return nil
	}

	col := Collection{
		ParentID: uuid.NullUUID{
			UUID:  parent,
			Valid: true,
		},
		Title: e.Name,
	}

	_, err := c.R().
		SetResult(&col).
		SetBody(&col).
		Post("/collections")
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("unable to create collection %q", e.Name))
	}

	for i := range e.Children {
		if err := createSubColls(c, col.ID, e.Children[i]); err != nil {
			return errors.Wrap(err, fmt.Sprintf("unable to create sub-collection of %q --> %q", e.Name, e.Children[i].Name))
		}
	}

	return nil
}

type Collection struct {
	ID        uuid.UUID     `json:"id,omitempty"`
	CreatedAt time.Time     `json:"created_at,omitempty"`
	UpdatedAt time.Time     `json:"updated_at,omitempty"`
	OwnerID   uuid.UUID     `json:"owner_id,omitempty"`
	ParentID  uuid.NullUUID `json:"parent_id,omitempty"`
	Title     string        `json:"title,omitempty"`
}

func createRootEntry(c *resty.Client, name string) (uuid.UUID, error) {
	col := Collection{
		Title: name,
	}

	_, err := c.R().
		SetResult(&col).
		SetBody(&col).
		Post("/collections")
	if err != nil {
		return uuid.Nil, errors.Wrap(err, "unable to create collection")
	}

	return col.ID, nil
}

type BookmarkSimple struct {
	CollectionID uuid.UUID `json:"collection_id,omitempty"`
	Title        string    `json:"title,omitempty"`
	Description  string    `json:"description,omitempty"`
	URL          string    `json:"url,omitempty"`
}

func addURL(c *resty.Client, collection uuid.UUID, title, url string) error {
	b := BookmarkSimple{
		CollectionID: collection,
		Title:        title,
		URL:          url,
	}

	_, err := c.R().
		SetBody(&b).
		Post("/bookmarks")
	if err != nil {
		return errors.Wrap(err, "unable to add bookmark to collection")
	}

	return nil
}
