package cmd

import (
	"github.com/urfave/cli/v2"

	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

func newIndexCommand() *cli.Command {
	return &cli.Command{
		Name:   "index",
		Usage:  "Starts indexing all websites which have not been indexed",
		Action: indexAction,
	}
}

func indexAction(c *cli.Context) error {
	task.Init(nil)

	t, _ := tasks.NewWebsiteIndexMissing()
	if err := task.Enqueue(t); err != nil {
		return err
	}

	return nil
}
