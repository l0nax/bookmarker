package cmd

import "github.com/urfave/cli/v2"

func NewCommands() []*cli.Command {
	return []*cli.Command{
		newServeCommand(),
		newIndexCommand(),
	}
}
