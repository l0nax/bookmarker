package cmd

import (
	"github.com/urfave/cli/v2"
	"gitlab.com/l0nax/bookmarker/handler"
)

func newServeCommand() *cli.Command {
	return &cli.Command{
		Name:   "serve",
		Usage:  "Starts the application",
		Action: serveAction,
	}
}

func serveAction(c *cli.Context) error {
	return handler.StartServer(c.Context)
}
