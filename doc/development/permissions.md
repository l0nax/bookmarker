## Groups and Collections

Groups and collections can have the following visibility levels:
* public (`60`) – an entity is visible to everyone
* internal (`30`) – an entity is visible to authenticated users (within the containing group) **TODO; FUTURE**
* private (`0`) – (default) an entity is visible only to the approved members of the entity


### Members

Users can be members of multiple groups and collections. The following access levels are available:
- No access (`0`)
- [Minimal access](../user/permissions.md#users-with-minimal-access) (`5`)
- Guest (`10`)
- Reader (`20`)
- Editor (`30`)
- Maintainer (`40`)
- Owner (`50`)

If a user is the member of both a collection and the collections group, the
highest permission is the applied access level for the collection.

If a user is the member of a collection, but not the group, they
can still view the group and their entities (of the collection; but other collections from the group).

Collection membership (where the group membership is already taken into account)
is stored in the `collection_authorizations` table.
