#!/bin/bash

sleep 1

/bookmarker db init || exit 1
/bookmarker db migrate || exit 1

exec $@
