package ftracer

import (
	"github.com/gofiber/fiber/v2"
	"go.opentelemetry.io/otel"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/metric/global"
	"go.opentelemetry.io/otel/propagation"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	"go.opentelemetry.io/otel/trace"
)

// Config defines the config for middleware.
type Config struct {
	Tracer                *sdktrace.TracerProvider
	MeterProvider         otelmetric.MeterProvider
	Propagators           propagation.TextMapPropagator
	TracerStartAttributes []trace.SpanStartOption
	SpanName              string
	LocalKeyName          string
	SpanNameFormatter     func(*fiber.Ctx) string
}

// ConfigDefault is the default config
var ConfigDefault = Config{
	SpanName:     "http/request",
	LocalKeyName: LocalsCtxKey,
	TracerStartAttributes: []trace.SpanStartOption{
		trace.WithSpanKind(trace.SpanKindServer),
		trace.WithNewRoot(),
	},
}

// Helper function to set default values
func configDefault(config ...Config) Config {
	// Return default config if nothing provided
	if len(config) < 1 {
		return ConfigDefault
	}

	// Override default config
	cfg := config[0]

	if cfg.SpanName == "" {
		cfg.SpanName = ConfigDefault.SpanName
	}

	if cfg.LocalKeyName == "" {
		cfg.LocalKeyName = ConfigDefault.LocalKeyName
	}

	if len(cfg.TracerStartAttributes) == 0 {
		cfg.TracerStartAttributes = ConfigDefault.TracerStartAttributes
	}

	if cfg.MeterProvider == nil {
		cfg.MeterProvider = global.MeterProvider()
	}

	if cfg.Propagators == nil {
		cfg.Propagators = otel.GetTextMapPropagator()
	}

	if cfg.SpanNameFormatter == nil {
		cfg.SpanNameFormatter = defaultSpanNameFormatter
	}

	return cfg
}

// defaultSpanNameFormatter is the default formatter for spans created with the fiber
// integration. Returns the route pathRaw
func defaultSpanNameFormatter(ctx *fiber.Ctx) string {
	return ctx.Route().Path
}
