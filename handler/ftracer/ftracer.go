package ftracer

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"go.opentelemetry.io/otel/trace"
)

func FromCtx(ctx *fiber.Ctx) context.Context {
	// NOTE: The UserContext is set by the Tracer middleware (with span information)
	return ctx.UserContext()
}

func SpanFromCtx(ctx *fiber.Ctx) trace.Span {
	otelCtx := FromCtx(ctx)
	if otelCtx == nil {
		return trace.SpanFromContext(ctx.Context())
	}

	return trace.SpanFromContext(otelCtx)
}
