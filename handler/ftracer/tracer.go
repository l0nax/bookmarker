package ftracer

import (
	"context"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/utils"
	otelcontrib "go.opentelemetry.io/contrib"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/attribute"
	"go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/metric/instrument"
	"go.opentelemetry.io/otel/metric/unit"
	"go.opentelemetry.io/otel/propagation"
	semconv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.opentelemetry.io/otel/trace"
	oteltrace "go.opentelemetry.io/otel/trace"
)

const (
	LocalsCtxKey        = "otel-ctx"
	LocalsTracerKey     = "otel-tracer"
	instrumentationName = "fabmation-markfolio-otelfiber"

	metricNameHttpServerDuration       = "http.server.duration"
	metricNameHttpServerRequestSize    = "http.server.request.size"
	metricNameHttpServerResponseSize   = "http.server.response.size"
	metricNameHttpServerActiveRequests = "http.server.active_requests"
)

var Tracer = otel.Tracer("fiber-otel-router")

// New creates a new middleware handler
func New(config ...Config) fiber.Handler {
	// Set default config
	cfg := configDefault(config...)

	tracer := cfg.Tracer.Tracer(
		instrumentationName,
		oteltrace.WithInstrumentationVersion(otelcontrib.SemVersion()),
	)

	meter := cfg.MeterProvider.Meter(
		instrumentationName,
		metric.WithInstrumentationVersion(otelcontrib.SemVersion()),
	)

	httpServerDuration, err := meter.Float64Histogram(metricNameHttpServerDuration, instrument.WithUnit(unit.Milliseconds), instrument.WithDescription("measures the duration inbound HTTP requests"))
	if err != nil {
		otel.Handle(err)
	}
	httpServerRequestSize, err := meter.Int64Histogram(metricNameHttpServerRequestSize, instrument.WithUnit(unit.Bytes), instrument.WithDescription("measures the size of HTTP request messages"))
	if err != nil {
		otel.Handle(err)
	}
	httpServerResponseSize, err := meter.Int64Histogram(metricNameHttpServerResponseSize, instrument.WithUnit(unit.Bytes), instrument.WithDescription("measures the size of HTTP response messages"))
	if err != nil {
		otel.Handle(err)
	}
	httpServerActiveRequests, err := meter.Int64UpDownCounter(metricNameHttpServerActiveRequests, instrument.WithUnit(unit.Dimensionless), instrument.WithDescription("measures the number of concurrent HTTP requests that are currently in-flight"))
	if err != nil {
		otel.Handle(err)
	}

	return func(c *fiber.Ctx) error {
		c.Locals(LocalsTracerKey, tracer)
		savedCtx, cancel := context.WithCancel(c.UserContext())

		start := time.Now()

		requestMetricsAttrs := httpServerMetricAttributesFromRequest(c, cfg)
		httpServerActiveRequests.Add(savedCtx, 1, requestMetricsAttrs...)

		responseMetricAttrs := make([]attribute.KeyValue, len(requestMetricsAttrs))
		copy(responseMetricAttrs, requestMetricsAttrs)

		reqHeader := make(http.Header)
		c.Request().Header.VisitAll(func(k, v []byte) {
			reqHeader.Add(string(k), string(v))
		})

		ctx := cfg.Propagators.Extract(savedCtx, propagation.HeaderCarrier(reqHeader))

		opts := []oteltrace.SpanStartOption{
			oteltrace.WithAttributes(httpServerTraceAttributesFromRequest(c, cfg)...),
			oteltrace.WithSpanKind(oteltrace.SpanKindServer),
		}

		// temporary set to c.Path() first
		// update with c.Route().Path after c.Next() is called
		// to get pathRaw
		spanName := utils.CopyString(c.Path())

		ctx, span := tracer.Start(ctx, spanName, opts...)
		defer span.End()

		c.Locals(LocalsCtxKey, ctx)

		// pass the span through userContext
		c.SetUserContext(ctx)

		// serve the request to the next middleware
		if err := c.Next(); err != nil {
			span.RecordError(err)
			// invokes the registered HTTP error handler
			// to get the correct response status code
			_ = c.App().Config().ErrorHandler(c, err)
		}

		// extract common attributes from response
		responseAttrs := append(
			semconv.HTTPAttributesFromHTTPStatusCode(c.Response().StatusCode()),
			semconv.HTTPRouteKey.String(c.Route().Path), // no need to copy c.Route().Path: route strings should be immutable across app lifecycle
		)

		requestSize := int64(len(c.Request().Body()))
		responseSize := int64(len(c.Response().Body()))

		defer func() {
			responseMetricAttrs = append(
				responseMetricAttrs,
				responseAttrs...)

			httpServerActiveRequests.Add(savedCtx, -1, requestMetricsAttrs...)
			httpServerDuration.Record(savedCtx, float64(time.Since(start).Microseconds())/1000, responseMetricAttrs...)
			httpServerRequestSize.Record(savedCtx, requestSize, responseMetricAttrs...)
			httpServerResponseSize.Record(savedCtx, responseSize, responseMetricAttrs...)

			c.SetUserContext(savedCtx)
			cancel()
		}()

		span.SetAttributes(
			append(
				responseAttrs,
				semconv.HTTPResponseContentLengthKey.Int64(responseSize),
			)...)
		span.SetName(cfg.SpanNameFormatter(c))

		spanStatus, spanMessage := semconv.SpanStatusFromHTTPStatusCodeAndSpanKind(c.Response().StatusCode(), oteltrace.SpanKindServer)
		span.SetStatus(spanStatus, spanMessage)

		return nil
	}

	/*
		// Return new handler
		return func(c *fiber.Ctx) error {




			// concat all span options, dynamic and static
			spanOptions := concatSpanOptions(
				[]trace.SpanStartOption{
					trace.WithAttributes(semconv.HTTPMethodKey.String(c.Method())),
					trace.WithAttributes(semconv.HTTPTargetKey.String(string(c.Request().RequestURI()))),
					trace.WithAttributes(semconv.HTTPRouteKey.String(c.Route().Path)),
					trace.WithAttributes(semconv.HTTPURLKey.String(c.OriginalURL())),
					trace.WithAttributes(semconv.NetHostIPKey.String(c.IP())),
					trace.WithAttributes(semconv.HTTPUserAgentKey.String(string(c.Request().Header.UserAgent()))),
					trace.WithAttributes(semconv.HTTPRequestContentLengthKey.Int(c.Request().Header.ContentLength())),
					trace.WithAttributes(semconv.HTTPSchemeKey.String(c.Protocol())),
					trace.WithAttributes(semconv.NetTransportTCP),
					trace.WithSpanKind(trace.SpanKindServer),
					// TODO:
					// - x-forwarded-for
					// -
				},
				cfg.TracerStartAttributes,
			)

			otelCtx, span := Tracer.Start(
				c.Context(),
				c.Route().Path,
				spanOptions...,
			)

			c.Locals(LocalsCtxKey, otelCtx)
			defer span.End()

			err := c.Next()

			statusCode := c.Response().StatusCode()
			attrs := semconv.HTTPAttributesFromHTTPStatusCode(statusCode)
			spanStatus, spanMessage := semconv.SpanStatusFromHTTPStatusCode(statusCode)
			span.SetAttributes(attrs...)
			span.SetStatus(spanStatus, spanMessage)
			span.SetName(c.Route().Path)

			return err
		}
	*/
}

func concatSpanOptions(sources ...[]trace.SpanStartOption) []trace.SpanStartOption {
	var spanOptions []trace.SpanStartOption
	for _, source := range sources {
		spanOptions = append(spanOptions, source...)
	}

	return spanOptions
}
