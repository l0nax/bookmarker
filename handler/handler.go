package handler

import (
	"context"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/rueian/rueidis"
	"gitlab.fabmation.info/20000/71168/fabguard-go"
	"go.opentelemetry.io/otel"
	"go.opentelemetry.io/otel/exporters/jaeger"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/sdk/resource"
	sdktrace "go.opentelemetry.io/otel/sdk/trace"
	semconv "go.opentelemetry.io/otel/semconv/v1.4.0"
	_ "go.uber.org/automaxprocs"
	"go.uber.org/zap"

	apiv1 "gitlab.com/l0nax/bookmarker/api/v1"
	"gitlab.com/l0nax/bookmarker/handler/ftracer"
	"gitlab.com/l0nax/bookmarker/handler/middleware"
	"gitlab.com/l0nax/bookmarker/internal/cache"
	"gitlab.com/l0nax/bookmarker/internal/config"
	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/internal/redis"
	"gitlab.com/l0nax/bookmarker/internal/s3"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/models/entities"
	"gitlab.com/l0nax/bookmarker/models/listener"
	"gitlab.com/l0nax/bookmarker/pkg/ferret"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

var (
	app    *fiber.App
	tracer *sdktrace.TracerProvider
)

func StartServer(ctx context.Context) error {
	tracer = initTracer()
	defer func() {
		if err := tracer.Shutdown(context.Background()); err != nil {
			log.Error("Tracing error occurred while shutting down", zap.Error(err))
		}
	}()

	redisConnect()
	log.Info("Connected to redis")

	log.Info("Connecting to database")
	models.Connect(ctx, listener.Handler)
	log.Info("Database connection successfully established")

	log.Info("Connect to S3 Server")
	if err := s3.Init(); err != nil {
		log.Fatal("Unable to connect to S3 server", zap.Error(err))
	}

	ferret.Init()

	cacheConnect()

	license.Load()
	license.SetUserResolver(func(userID uuid.UUID, f fabguard.LicenseFeatureFlag) bool {
		user, err := entities.GetUserByID(context.Background(), userID)
		if err != nil {
			log.Error("Unable to retrieve user for feature check", zap.Stringer("user_id", userID), zap.Error(err))

			return false
		}

		sub, err := models.GetHighestSubscription(context.Background(), user.ID)
		if err != nil {
			log.Error("Unable to retrieve subscription for user: fallback to Free", zap.Stringer("user_id", user.ID), zap.Error(err))
			return feature.Free.IncludesFeature(f)
		}

		return sub.PlanID.ToLicenseGroup().IncludesFeature(f)
	})

	task.Init(nil)
	go task.InitWorker(nil)

	if err := tasks.RunTaskManager(); err != nil {
		log.Fatal("Unable to start periodic task manager", zap.Error(err))
	}

	// configure the fiber behavior
	appSettings := fiber.Config{
		BodyLimit: 1024 * 1024 * 1024 * 1024,
		// StrictRouting is very important to be able to use such routes:
		//      * /customer/:id
		//      * /customer/-/:iid
		StrictRouting:           true,
		CaseSensitive:           true,
		ErrorHandler:            ErrorHandler,
		EnableTrustedProxyCheck: true,
		Prefork:                 false,
		TrustedProxies:          []string{"127.0.0.1", "127.0.0.6"},
	}

	app = fiber.New(appSettings)
	registerMiddlewares(app)

	apiv1.AddApiV1(app.Group("/api/v1"))
	app.Get("/health", healthHandler)

	if err := app.Listen(config.C.Listen); err != nil {
		return errors.Wrap(err, "unable to listen")
	}

	models.Wait()

	return nil
}

const defaultFormat = "{\"level\":\"info\",\"log_type\":\"http_log\",\"ts\":\"${time}\",\"request_id\":\"${locals:request_id}\",\"method\":\"${method}\",\"path\":\"${path}\",\"resp_status\":\"${status}\",\"remote_ip\":\"${ip}\",\"remote_ips\":\"${ips}\",\"latency\":\"${latency}\",\"bytes_sent\":${bytesSent},\"bytes_received\":${bytesReceived},\"user_agent\":\"${ua}\",\"referer\":\"${referer}\"}\n"

func healthHandler(c *fiber.Ctx) error {
	return c.JSON(fiber.Map{
		"status": "ok",
	})
}

// registerMiddlewares registers all middlewares.
func registerMiddlewares(app *fiber.App) {
	app.Use(ftracer.New(ftracer.Config{
		Tracer: tracer,
	}))

	// the Request ID middleware must be executed before all the other middlewares.
	app.Use(requestid.New())

	app.Use(logger.New(logger.Config{
		// For more options, see the Config section
		Format: defaultFormat,
	}))

	/*
		if config.C.General.EnableCompression {
			app.Use(compress.New(compress.Config{
				Level: compress.Level(config.C.General.CompressionLevel),
			}))
		}
	*/

	app.Use(cors.New(cors.Config{
		AllowCredentials: true,
	}))

	app.Use(middleware.APIAuth())
}

func redisConnect() {
	rdb, err := rueidis.NewClient(rueidis.ClientOption{
		InitAddress: config.C.Redis.Address,
		ShuffleInit: true,
	})
	if err != nil {
		log.Fatal("Unable to connect to redis", zap.Error(err))
	}

	_ = redis.Connect(rdb)
}

func cacheConnect() {
	log.Info("Connecting to Redis")

	rdb, err := rueidis.NewClient(rueidis.ClientOption{
		InitAddress: config.C.Redis.Address,
		ShuffleInit: true,
	})
	if err != nil {
		log.Fatal("Unable to connect to redis", zap.Error(err))
	}

	cache.Connect(rdb)
}

func initTracer() *sdktrace.TracerProvider {
	exporter, err := jaeger.New(jaeger.WithCollectorEndpoint(jaeger.WithEndpoint("http://tempo.monitoring.svc.cluster.local:14268/api/traces")))
	if err != nil {
		log.Fatal("Unable to initialize jeager exporter", zap.Error(err))
	}

	tp := sdktrace.NewTracerProvider(
		sdktrace.WithSampler(sdktrace.AlwaysSample()),
		sdktrace.WithBatcher(exporter),
		sdktrace.WithResource(
			resource.NewWithAttributes(
				semconv.SchemaURL,
				semconv.ServiceNameKey.String("markfolio-server"),
			)),
	)

	otel.SetTracerProvider(tp)
	otel.SetTextMapPropagator(propagation.NewCompositeTextMapPropagator(propagation.TraceContext{}, propagation.Baggage{}))

	return tp
}
