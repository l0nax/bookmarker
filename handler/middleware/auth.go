package middleware

import (
	"context"
	"errors"
	"fmt"
	"os"
	"strings"
	"unsafe"

	"github.com/authorizerdev/authorizer-go"
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
)

var anonRoutes = make(map[string]struct{})

// isAnonRequestAllowed returns true if anonymous requests to the given
// route are allowed.
func isAnonRequestAllowed(route string) bool {
	_, ok := anonRoutes[route]

	return ok
}

// RegisterAnonymousRoute allows anonymous requests to the given route.
//
// NOTE: Only routes without parameters are supported!
func RegisterAnonymousRoute(route string) {
	anonRoutes[route] = struct{}{}
}

// APIAuth implements the authentication middleware for the API endpoints.
func APIAuth() func(*fiber.Ctx) error {
	cli, err := authorizer.NewAuthorizerClient(os.Getenv("AUTHORIZER_CLIENT_ID"), os.Getenv("AUHTORIZER_URL"), "", map[string]string{})
	if err != nil {
		log.Fatal("Unable to initialize authorizer", zap.Error(err))
	}

	return func(c *fiber.Ctx) error {
		head := c.Get("Authorization")

		split := strings.Split(head, " ")
		if len(split) < 2 || split[1] == "" {
			return rerr.Unauthorized
		}

		if split[0] == "Token" {
			return handleTokenAuth(c, split[1])
		}

		res, err := cli.ValidateJWTToken(&authorizer.ValidateJWTTokenInput{
			TokenType: authorizer.TokenTypeAccessToken,
			Token:     split[1],
		})
		if err != nil {
			return rerr.Unauthorized.With(err)
		} else if !res.IsValid {
			return rerr.Unauthorized.WithLogMsg("invalid token")
		}

		ctx := c.UserContext()

		prof, err := cli.GetProfile(map[string]string{
			"Authorization": fmt.Sprintf("Bearer %s", authorizer.StringValue(&split[1])),
		})
		if err != nil {
			return rerr.Unauthorized.With(err).WithLogMsg("unable to retrieve profile")
		}

		user, err := models.GetOrCreateUserByMail(ctx, prof)
		if err != nil {
			return rerr.InternalServerError.With(err).WithLogMsg("unable to find user")
		}

		c.Locals("ENTITY", user)

		return c.Next()
	}
}

func handleTokenAuth(c *fiber.Ctx, token string) error {
	ctx := c.UserContext()

	tok, err := models.GetTokenByString(ctx, token)
	if err != nil {
		if models.IsNoRows(err) {
			return rerr.Unauthorized
		}

		return rerr.InternalServerError.With(err)
	}

	c.Locals("AUTH_IS_TOKEN", "yes")
	c.Locals("ENTITY", tok.User)

	return c.Next()
}

func validateUserLogin(ctx context.Context, user, pwd string) (models.User, bool, error) {
	u, err := models.GetUserByMail(ctx, user)
	if err != nil {
		if errors.Is(err, models.ErrUserNotFound) {
			err = nil
		}

		// NOTE: Return false to prevent information disclosure
		return models.User{}, false, err
	}

	ok, err := models.ComparePasswordAndHash(pwd, u.Password)
	if err != nil {
		return models.User{}, false, err
	}

	return u, ok, nil
}

// byteSlice2String converts a byte slice to a string in a performant way.
func byteSlice2String(bs []byte) string {
	return *(*string)(unsafe.Pointer(&bs))
}
