package rerr

import "net/http"

// RequestMalformed describes that the request data (body, header, URI, ...) is malformed.
// NOTE: This error shall ONLY be used if information disclousure is acceptable.
var RequestMalformed = newErr(
	ecRequestMalformed,
	ERequest,
	"invalid request body/ header",
	http.StatusBadRequest,
)

// AccessTokenExpired describes that the provided access token has expired
// and should be refreshed.
var AccessTokenExpired = newErr(
	ecAccessTokenExpired,
	ERequest,
	"access token expired",
	http.StatusBadRequest,
)

// Unauthenticated describes that the client must authenticate itself
// before proceeding.
var Unauthenticated = newErr(
	ecUnauthenticated,
	ERequest,
	"unauthenticated",
	http.StatusBadRequest,
)

// Unauthorized describes that the client is not allowed to execute the action.
var Unauthorized = newErr(
	ecUnauthenticated,
	ERequest,
	"unauthorized",
	http.StatusUnauthorized,
)

var FeatureNotAvail = newErr(
	ecFeatureNotAvail,
	EServer,
	"feature is not available on this server",
	http.StatusInternalServerError,
)

var EntityNotFound = newErr(
	ecEntityNotFound,
	ERequest,
	"the entity could not be found",
	http.StatusNotFound,
)
