package utils

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/l0nax/bookmarker/handler/rerr"
	"gitlab.com/l0nax/bookmarker/pkg/null"
)

const (
	// maxPerPage is the maximum number documents per page.
	maxPerPage uint = 100
	// localPagingResp is the key holding [PaginationResponse] for next.
	localPagingResp = "fmc.pagination_response_next"
	// localPagingRespPrev is the key holding [PaginationResponse] for prev.
	localPagingRespPrev = "fmc.pagination_response_prev"
)

// PaginationDirection describes the direction of a pagination.
type PaginationDirection string

const (
	// PaginationDirDefault describes the "empty" direction case which defaults to [PaginationNext].
	PaginationDirDefault = PaginationDirection("")
	// PaginationPrev describes the action "previous".
	PaginationPrev = PaginationDirection("prev")
	// PaginationNext describes the action "next".
	PaginationNext = PaginationDirection("next")
)

var _emptyStruct = struct{}{}

var _paginationDirectionVals = map[PaginationDirection]struct{}{
	PaginationDirDefault: _emptyStruct,
	PaginationPrev:       _emptyStruct,
	PaginationNext:       _emptyStruct,
}

// IsValid returns whether d is known/ valid or not.
func (d PaginationDirection) IsValid() bool {
	_, ok := _paginationDirectionVals[d]
	return ok
}

// PaginationRequest holds the pagination information provided by the requester.
type PaginationRequest struct {
	// PerPage is the max. number of elements per page request.
	//
	// The server may or may not honor this value, depending on its configuration.
	PerPage uint `json:"per_page" query:"per_page"`

	// Direction describes the pagination direction of the request.
	Direction PaginationDirection `json:"direction" query:"pagination_direction"`

	// StartID is the UUID where the server should start its pagination.
	StartID uuid.UUID `json:"start_id" query:"start_id"`

	// StartTime is the time where the server should start its pagination.
	StartTime time.Time `json:"start_time" query:"start_time"`

	// WithTotalDocsCount describes whether the client asks the server how many
	// documents there exist.
	//
	// The server may or may not return the total document count.
	WithTotalDocsCount bool `json:"with_total_docs_count" query:"with_total_docs_count"`

	// WithTotalPagesCount describes whether the client asks the server how many
	// pages there exist.
	//
	// The server may or may not return the total page count.
	WithTotalPagesCount bool `json:"with_total_pages_count" query:"with_total_pages_count"`

	// valid describes whether the the requester has provided any parameters.
	valid bool
}

// IsValid returns the the requester has provided any parameters (StartID and StartTime).
func (p PaginationRequest) IsValid() bool { return p.valid }

// PaginationResponse holds the pagination information provided to the client by the server.
type PaginationResponse struct {
	// PerPage is the max. number of elements per page request.
	//
	// The server may or may not honor this value, depending on its configuration.
	PerPage uint `json:"per_page"`

	// StartID the ID part for the next page.
	StartID uuid.UUID `json:"start_id"`

	// StartTime the time part for the next page.
	StartTime time.Time `json:"start_time"`

	// TotalDocsCount contains the total documents count.
	TotalDocsCount null.Int `json:"total_docs_count"`

	// TotalPagesCount contains the total pages count.
	TotalPagesCount null.Int `json:"total_pages_count"`
}

// PaginationMetadata is the general response of paginated endpoints.
type PaginationMetadata[T any] struct {
	// Items holds the response items.
	Items []T `json:"items"`

	// Meta holds the pagination metadata information.
	Meta PaginationSubMeta `json:"_meta"`
}

// PaginationSubMeta describes a single meta entry of [PaginationMetadata].
type PaginationSubMeta struct {
	// Next holds the information for the next page.
	//
	// NOTE: When requesting the next page use [PaginationNext] as direction.
	Next PaginationResponse `json:"next"`

	// Prev holds the information for the previous page.
	//
	// NOTE: When requesting the previous page use [PaginationPrev] as direction.
	Prev PaginationResponse `json:"prev"`

	// TotalDocsCount contains the total documents count.
	TotalDocsCount null.Int `json:"total_docs_count"`

	// TotalPagesCount contains the total pages count.
	TotalPagesCount null.Int `json:"total_pages_count"`
}

// NewPaginatedResponse returns a fully initialized [PaginationMetadata] response.
func NewPaginatedResponse[T any](data []T, prev, next PaginationResponse) PaginationMetadata[T] {
	return PaginationMetadata[T]{
		Items: data,
		Meta: PaginationSubMeta{
			Next:            next,
			Prev:            prev,
			TotalDocsCount:  next.TotalDocsCount,
			TotalPagesCount: next.TotalPagesCount,
		},
	}
}

// PaginatedJSON is a helper function for [NewPaginatedResponse].
func PaginatedJSON[T any](c *fiber.Ctx, data []T) error {
	next, prev := extractPaginationData(c)

	return c.JSON(NewPaginatedResponse(data, prev, next))
}

// extractPaginationData returns the next and previous [PaginationResponse]
// stored in the Locals object of the Ctx.
func extractPaginationData(c *fiber.Ctx) (PaginationResponse, PaginationResponse) {
	nextRaw := c.Locals(localPagingResp)
	prevRaw := c.Locals(localPagingRespPrev)

	if nextRaw == nil || prevRaw == nil {
		return PaginationResponse{}, PaginationResponse{}
	}

	next := nextRaw.(PaginationResponse)
	prev := prevRaw.(PaginationResponse)

	return next, prev
}

// ParsePagination parses the pagination query from the request.
func ParsePagination(c *fiber.Ctx) (PaginationRequest, error) {
	var p PaginationRequest

	if err := c.QueryParser(&p); err != nil {
		return p, rerr.RequestMalformed.With(err).WithLogMsg("unable to parse pagination parameters")
	}

	if p.Direction == PaginationDirDefault {
		p.Direction = PaginationNext
	}

	p.valid = p.StartID != uuid.Nil && !p.StartTime.IsZero() && p.Direction.IsValid()
	p.PerPage = minNotZero(maxPerPage, p.PerPage)

	return p, nil
}

// SetPaginationResp stores the pagination response in the context
// for the Link middleware to process.
func SetPaginationResp(c *fiber.Ctx, next, prev PaginationResponse) {
	c.Locals(localPagingResp, next)
	c.Locals(localPagingRespPrev, prev)
}

// minNotZero returns the lowest number (x or y) which is not zero.
func minNotZero(x, y uint) uint {
	if x <= 0 {
		x = 1
	}
	if y <= 0 {
		y = 1
	}

	if x < y {
		return x
	}

	return y
}
