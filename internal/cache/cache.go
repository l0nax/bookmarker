package cache

import (
	"context"
	"encoding"
	"fmt"
	"time"

	"github.com/hashicorp/go-multierror"
	"github.com/pkg/errors"
	"github.com/rueian/rueidis"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
)

// defaultCacheTTL is the default TTL to cache commands on client-side.
const defaultCacheTTL = 10 * time.Minute

var c rueidis.Client

// Connect connects to redis and initializes the internal cache.
func Connect(client rueidis.Client) error {
	c = client

	return nil
}

// generateRedisValue returns the string representation of the provided value for use as
// a redis value.
func generateRedisValue(value any) string {
	switch v := value.(type) {
	case string:
		return v
	case []byte:
		return string(v)
	case encoding.BinaryMarshaler:
		if data, err := v.MarshalBinary(); err == nil {
			return rueidis.BinaryString(data)
		}
	case bool:
		if v {
			return "1"
		}
		return "0"
	}

	// TODO: This is the slowest way to get the string representation => we need to report this so we can improve our code
	return fmt.Sprint(value)
}

// SIsMember executes SISMEMBER on the provided key.
// If an error occurs, it will be logged and false will be returned.
func SIsMember(ctx context.Context, key Key, member any) bool {
	ok, err := ESIsMember(ctx, key, member)
	if err != nil {
		log.Error("An error occurred while executing SISMEMBER",
			zap.String("key", string(key)), zap.Error(err))

		return false
	}

	return ok
}

// ESIsMember works like [SIsMember] but it returns the error instead of logging it.
func ESIsMember(ctx context.Context, key Key, member any) (bool, error) {
	const ttl = 2 * time.Hour

	cmd := c.B().Sismember().
		Key(string(key)).
		Member(generateRedisValue(member)).
		Cache()

	ok, err := c.DoCache(ctx, cmd, ttl).AsBool()
	if err != nil {
		return false, errors.Wrap(err, "unable to execute SISMEMBER")
	}

	return ok, nil
}

// SAdd executes SAdd on the given data.
// If an error occurs, it will be logged.
func SAdd(ctx context.Context, key Key, val any, tags ...Tag) {
	err := ESAdd(ctx, key, val, tags...)
	if err != nil {
		log.Error("An error occurred while executing SADD", zap.String("key", string(key)), zap.Error(err))
	}
}

// ESAdd works similar to [SAdd] but it returns the error instead of logging it.
func ESAdd(ctx context.Context, key Key, val any, tags ...Tag) error {
	// TODO: Implement something that allows us to remove stale tags!!
	cmds := make(rueidis.Commands, 0, len(tags))

	for _, tag := range tags {
		cmd := c.B().Sadd().
			Key(string(tag)).
			Member(string(key)).
			Build()

		cmds = append(cmds, cmd)
	}

	cmds = append(cmds, c.B().Sadd().
		Key(string(key)).
		Member(generateRedisValue(val)).
		Build(),
	)

	var errs error

	for _, resp := range c.DoMulti(ctx, cmds...) {
		if err := resp.Error(); err != nil {
			errs = multierror.Append(err, errs)
		}
	}

	return errors.Wrap(errs, "unable to set cache entry")
}

// EDel deletes the key-value pair.
func EDel(ctx context.Context, key Key) error {
	cmd := c.B().Del().
		Key(string(key)).
		Build()

	log.Debug("Deleting redis key", zap.String("key", string(key)))

	err := c.Do(ctx, cmd).Error()
	if err != nil && rueidis.IsRedisNil(err) {
		err = nil
	}

	return errors.Wrap(err, "unable to delete key")
}

// ESetTTL stores the provided key-value pair with a TTL.
func ESetTTL(ctx context.Context, key Key, val any, ttl time.Duration, tags ...Tag) error {
	cmds := make(rueidis.Commands, 0, len(tags)+1)

	for _, tag := range tags {
		cmds = append(cmds,
			c.B().Sadd().
				Key(string(tag)).
				Member(string(key)).
				Build(),
		)
	}

	cmds = append(cmds,
		c.B().Setex().
			Key(string(key)).
			Seconds(int64(ttl.Seconds())).
			Value(generateRedisValue(val)).
			Build(),
	)

	var errs error

	for _, resp := range c.DoMulti(ctx, cmds...) {
		if err := resp.Error(); err != nil {
			errs = multierror.Append(err, errs)
		}
	}

	return errors.Wrap(errs, "unable to set TTL for key")
}

// ESet stores the provided key-value pair.
func ESet(ctx context.Context, key Key, val any, tags ...Tag) error {
	// TODO: Implement something that allows us to remove stale tags!!

	cmds := make(rueidis.Commands, 0, len(tags)+1)

	for _, tag := range tags {
		cmd := c.B().Sadd().
			Key(string(tag)).
			Member(string(key)).
			Build()

		cmds = append(cmds, cmd)
	}

	cmds = append(cmds,
		c.B().Set().
			Key(string(key)).
			Value(generateRedisValue(val)).
			Build(),
	)

	var errs error

	for _, resp := range c.DoMulti(ctx, cmds...) {
		if err := resp.Error(); err != nil {
			errs = multierror.Append(err, errs)
		}
	}

	return errors.Wrap(errs, "unable to set key")
}

// InvalidateByTag invalidates the cache entries by the provided tags.
func InvalidateByTag(ctx context.Context, tags ...Tag) error {
	keys := make([]string, 0)

	for _, tag := range tags {
		cmd := c.B().Smembers().
			Key(string(tag)).
			Build()

		members, err := c.Do(ctx, cmd).AsStrSlice()
		if err != nil {
			return errors.Wrap(err, "unable to retrieve keys")
		}

		keys = append(keys, string(tag))
		keys = append(keys, members...)
	}

	cmds := make(rueidis.Commands, 0, len(keys))

	for _, key := range keys {
		cmds = append(cmds,
			c.B().Del().Key(key).Build(),
		)
	}

	var errs error

	for _, resp := range c.DoMulti(ctx, cmds...) {
		if err := resp.Error(); err != nil {
			errs = multierror.Append(err, errs)
		}
	}

	return errors.Wrap(errs, "unable to invalidate keys by tag")
}
