package cache

import (
	"context"
	"encoding/json"

	"github.com/google/uuid"
	"github.com/rueian/rueidis"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
)

type MetaCollectionList struct {
	ETag        string              `json:"e_tag,omitempty"`
	Collections []models.Collection `json:"collections,omitempty"`
}

// MarshalBinary implements the Binary marshaller to be compatible with redis.
func (m MetaCollectionList) MarshalBinary() (data []byte, err error) {
	bytes, err := json.Marshal(m)
	return bytes, err
}

// UnmarshalBinary implements the Binary unmarshaller to be compatible with redis.
func (m *MetaCollectionList) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, m)
}

// ExistsCollectionListETag returns whether the provided userID + etag tuple
// is known as a collection list.
// The size parameter may be empty.
func ExistsCollectionListETag(ctx context.Context, userID uuid.UUID, etag string) bool {
	cmd := c.B().
		Sismember().
		Key(string(CollectionListETag(userID, etag))).
		Member("is_valid").
		Cache()

	known, err := c.DoCache(ctx, cmd, defaultCacheTTL).AsBool()
	if err != nil {
		if rueidis.IsRedisNil(err) {
			return false
		}

		log.Error("Unable to check if ETag is known of collection listr", zap.Stringer("user_id", userID), zap.String("client_etag", etag), zap.Error(err))

		return false
	}

	return known
}

// SetCollectionListETag caches/ sets the (userID, etag) tuple to allow us to make use of the ETag
// header for a collection list.
// The size parameter may be empty.
func SetCollectionListETag(ctx context.Context, userID uuid.UUID, etag string) {
	err := ESAdd(ctx, Key(string(CollectionListETag(userID, etag))), "is_valid", WithUserCollectionList(userID))
	if err != nil {
		log.Error("Unable to store userID+etag tuple", zap.Stringer("user_id", userID), zap.String("etag", etag), zap.Error(err))
	}
}

// DeleteCollectionListETag removes all cached ETag values (collection list of user).
func DeleteCollectionListETag(ctx context.Context, userID uuid.UUID) {
	if err := InvalidateByTag(ctx, WithUserCollectionList(userID)); err != nil {
		log.Error("Unable to invalidate bookmark thumbnail ETag cache by tags", zap.Stringer("user_id", userID), zap.Error(err))
	}
}

// SetUserCollectionList caches the list of collections of the user.
func SetUserCollectionList(ctx context.Context, userID uuid.UUID, data MetaCollectionList, simpleTree bool) {
	err := ESet(ctx, Key(string(UserCollectionList(userID, simpleTree))), data, WithUserCollectionList(userID))
	if err != nil {
		log.Error("Unable to store user collection list",
			zap.Stringer("user_id", userID), zap.Error(err))
	}
}

// DeleteUserCollectionList invalidates all stored collection lists of the user.
func DeleteUserCollectionList(ctx context.Context, userID uuid.UUID) {
	DeleteCollectionListETag(ctx, userID)
}

// GetUserCollectionList returns the cached [models.Collection] list of the user or false if it is not known.
func GetUserCollectionList(ctx context.Context, userID uuid.UUID, simpleTree bool) (MetaCollectionList, bool) {
	cmd := c.B().
		Get().
		Key(string(UserCollectionList(userID, simpleTree))).
		Cache()

	var ret MetaCollectionList

	err := c.DoCache(ctx, cmd, defaultCacheTTL).DecodeJSON(&ret)
	if err != nil {
		if !rueidis.IsRedisNil(err) {
			log.Error("Unable to retrieve cached version of user collection", zap.Stringer("user_id", userID), zap.Error(err))
		}

		return MetaCollectionList{}, false
	}

	return ret, true
}
