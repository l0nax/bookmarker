package cache

import (
	"encoding/json"

	"github.com/google/uuid"
)

// Key represents a cache key prefix.
type Key string

// MarshalBinary implements the Binary marshaller to be compatible with redis.
func (k Key) MarshalBinary() ([]byte, error) {
	return json.Marshal(string(k))
}

const (
	// cacheKeyThumbnailETag is the ETag of a bookmark thumbnail.
	cacheKeyThumbnailETag = Key("bookmask::thumbnail::")
	// cacheKeyCollectionListETag is the ETag of a collection list of a user.
	cacheKeyCollectionListETag = Key("etag::collection::list")
	// cacheKeyUserCollectionList is the key storing the latest collection list of a user.
	cacheKeyUserCollectionList = Key("collection::list")
)

// ThumbnailETag returns the cache [Key] for bookmark thumbnail ETag lookups.
func ThumbnailETag(bookmarkID uuid.UUID, etag string) Key {
	return cacheKeyThumbnailETag + Key(bookmarkID.String()+"::"+etag)
}

// CollectionListETag returns the cache [Key].
func CollectionListETag(userID uuid.UUID, etag string) Key {
	return cacheKeyCollectionListETag + Key(userID.String()+"::"+etag)
}

// UserCollectionList returns the cache [Key].
func UserCollectionList(userID uuid.UUID, simpleTree bool) Key {
	t := "full_tree"
	if simpleTree {
		t = "simple_tree"
	}

	return cacheKeyUserCollectionList + Key(userID.String()+":"+t)
}
