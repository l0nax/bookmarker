package cache

import "github.com/google/uuid"

// Tag represents a cache tag.
type Tag string

const (
	// tagBookmarkID is the Bookmark ID tag.
	tagBookmarkID = "server:bookmark-id:"
	// tagBookmarkThumbnailID is the Bookmark Thumbnail tag.
	// It works like the [tagBookmarkID] but once we remove the cached entry,
	// we will not erase all cached entries of the user/ bookmark.
	tagBookmarkThumbnailID = "server:bookmark:thumbnail-id:"
	// tagUserCollectionList is the User Collection List tag.
	tagUserCollectionList = "collection:list:user-id:"
)

// WithBookmarkThumbnailID returns a prepared [Tag] to tag
// a bookmark thumbnail cache entry.
func WithBookmarkThumbnailID(bookmarkID uuid.UUID) Tag {
	return Tag(tagBookmarkThumbnailID + bookmarkID.String())
}

// WithUserCollectionList returns the prepared [Tag] to tag
// a collection list of a user.
func WithUserCollectionList(userID uuid.UUID) Tag {
	return Tag(tagUserCollectionList + userID.String())
}
