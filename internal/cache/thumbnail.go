package cache

import (
	"context"

	"github.com/google/uuid"
	"github.com/rueian/rueidis"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
)

// getDefaultAvatarSize returns the default value of the "size" query for avatars.
func getDefaultAvatarSize(size string) string {
	if size == "" {
		return "default"
	}

	return size
}

// ExistsThumbnailETag returns whether the provided bookmarkID + etag tuple
// is known as a thumbnail entry.
// The size parameter may be empty.
func ExistsThumbnailETag(ctx context.Context, bookmarkID uuid.UUID, etag, size string) bool {
	size = getDefaultAvatarSize(size)

	cmd := c.B().
		Sismember().
		Key(string(ThumbnailETag(bookmarkID, etag))).
		Member(size).
		Cache()

	known, err := c.DoCache(ctx, cmd, defaultCacheTTL).AsBool()
	if err != nil {
		if rueidis.IsRedisNil(err) {
			return false
		}

		log.Error("Unable to check if ETag is known of thumbnail", zap.Stringer("bookmark_id", bookmarkID), zap.String("client_etag", etag), zap.Error(err))

		return false
	}

	return known
}

// SetThumbnailETag caches/ sets the (bookmarkID, etag) tuple to allow us to make use of the ETag
// header for thumbnails.
// The size parameter may be empty.
func SetThumbnailETag(ctx context.Context, bookmarkID uuid.UUID, etag, size string) {
	size = getDefaultAvatarSize(size)

	err := ESAdd(ctx, Key(string(ThumbnailETag(bookmarkID, etag))), size, WithBookmarkThumbnailID(bookmarkID))
	if err != nil {
		log.Error("Unable to store userID+etag tuple for avatar", zap.Stringer("user_id", bookmarkID), zap.String("etag", etag), zap.Error(err))
	}
}

// DeleteThumbnailETag removes all cached ETag values for bookmark thumbnail.
func DeleteThumbnailETag(ctx context.Context, bookmarkID uuid.UUID) {
	if err := InvalidateByTag(ctx, WithBookmarkThumbnailID(bookmarkID)); err != nil {
		log.Error("Unable to invalidate bookmark thumbnail ETag cache by tags", zap.Stringer("bookmark_id", bookmarkID), zap.Error(err))
	}
}
