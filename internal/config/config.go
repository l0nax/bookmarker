package config

import (
	"github.com/joomcode/errorx"
	"github.com/knadh/koanf/parsers/yaml"
	"github.com/knadh/koanf/providers/confmap"
	"github.com/knadh/koanf/providers/file"
	"github.com/knadh/koanf/v2"

	"gitlab.com/l0nax/bookmarker/internal/log"
)

// Config is the configuration structure.
type Config struct {
	// DB holds the database configuration.
	DB struct {
		// DSN is the simple way to initialize the DB connection.
		DSN string `koanf:"dsn"`
	} `koanf:"db"`
	// LogLevel defines the verbosity of the log.
	LogLevel string `koanf:"log_level"`
	// Listen defines where the server should listen.
	Listen string `koanf:"listen"`
	// Redis holds the redis specific configuration parameter.
	Redis struct {
		// Network defines the communication type.
		Network string `koanf:"network"`
		// Address is the host and port of the redis servers.
		Address []string `koanf:"address"`
		// Password is the authentication password.
		// It can be left blank.
		Password string `koanf:"password"`
		// DB is the database number which should be used.
		DB int `koanf:"db"`
	} `koanf:"redis"`
	S3 struct {
		Endpoint  string `koanf:"endpoint"`
		AccessKey string `koanf:"access_key"`
		SecretKey string `koanf:"secret_key"`
		Buckets   struct {
			Data string `koanf:"data"`
		} `koanf:"buckets"`
	} `koanf:"s3"`
	Ferret struct {
		Worker string `koanf:"worker"`
	} `koanf:"ferret"`
	SSRF struct {
		DisableIPv6  bool  `koanf:"disable_ipv6"`
		AllowedPorts []int `koanf:"allowed_ports"`
	} `koanf:"ssrf"`
}

// C is the current config state.
var C Config

// k is the global koanf instance of the configuration.
var k = koanf.New(".")

// InitConfig reads and parses the provided configuration.
func InitConfig(path string) error {
	f := file.Provider(path)

	loadDefaultValues()

	if err := k.Load(f, yaml.Parser()); err != nil {
		return errorx.Decorate(err, "unable to load configuration")
	}

	if err := k.Unmarshal("", &C); err != nil {
		return errorx.Decorate(err, "unable to parse configuration")
	}

	log.SetLevel(C.LogLevel)

	return nil
}

func loadDefaultValues() {
	k.Load(confmap.Provider(map[string]any{
		"ssrf.allowed_ports": []int{
			80,
			443,
		},
	}, "."), nil)
}
