package log

import (
	"fmt"
	"os"
	"strings"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	// logger is the logger instance.
	logger *zap.Logger
	logCfg zap.Config
	zapLvl zap.AtomicLevel
)

// InitLogging initializes the logger.
// If the initialization sequence fails, the function will call os.Exit(1).
func InitLogging(logLvl, logEnv string) {
	if logEnv == "" {
		logEnv = "development"
	}

	if logLvl == "" {
		// start with debug
		logLvl = "debug"
	}

	lvl := decodeLogLevel(logLvl)
	zapLvl = zap.NewAtomicLevelAt(lvl)

	switch strings.ToLower(logEnv) {
	case "production":
		logCfg = newProductionConfig(zapLvl)
	case "development":
		logCfg = newDevelopmentConfig(zapLvl)

		// enable colored log output
		logCfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
	default:
		fmt.Fprintf(os.Stderr, "Invalid log environment value %q provided. Exiting...\n", logEnv)
		os.Exit(1)
	}

	var err error

	logger, err = logCfg.Build(zap.AddStacktrace(zap.FatalLevel), zap.AddCallerSkip(1))
	if err != nil {
		panic(err)
	}
}

// SetLevel allows to set a new log level.
func SetLevel(newLevel string) {
	lvl := decodeLogLevel(newLevel)
	zapLvl.SetLevel(lvl)
}

// newProductionConfig is a reasonable production logging configuration.
// Logging is enabled at InfoLevel and above.
//
// It uses a JSON encoder, writes to standard error, and enables sampling.
// Stacktraces are automatically included on logs of ErrorLevel and above.
func newProductionConfig(lvl zap.AtomicLevel) zap.Config {
	return zap.Config{
		Level:       lvl,
		Development: false,
		Sampling: &zap.SamplingConfig{
			Initial:    100,
			Thereafter: 100,
		},
		Encoding:         "json",
		EncoderConfig:    zap.NewProductionEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}
}

// newDevelopmentConfig is a reasonable development logging configuration.
// Logging is enabled at DebugLevel and above.
//
// It enables development mode (which makes DPanicLevel logs panic), uses a
// console encoder, writes to standard error, and disables sampling.
// Stacktraces are automatically included on logs of WarnLevel and above.
func newDevelopmentConfig(lvl zap.AtomicLevel) zap.Config {
	return zap.Config{
		Level:            lvl,
		Development:      true,
		Encoding:         "console",
		EncoderConfig:    zap.NewDevelopmentEncoderConfig(),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
	}
}

// decodeLogLevel decodes a "log level string" into the zap.AtomicLevel.
func decodeLogLevel(l string) zapcore.Level {
	switch strings.ToLower(l) {
	case "debug":
		return zapcore.DebugLevel
	case "info":
		return zapcore.InfoLevel
	case "warn":
		return zapcore.WarnLevel
	case "error":
		return zapcore.ErrorLevel
	case "fatal":
		return zapcore.FatalLevel
	default:
		fmt.Printf("Unknown log level (%s) provided", l)
		os.Exit(1)
	}

	return zapcore.ErrorLevel
}
