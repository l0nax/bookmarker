package s3

import (
	"context"
	"io"
	"path"
	"strconv"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/config"
	"gitlab.com/l0nax/bookmarker/internal/log"
)

const (
	// prefThumbnail is the directory prefix where all thumbnails are getting stored.
	prefThumbnail = "thumbnails/"
)

var cli *minio.Client

func Init() error {
	var err error

	cli, err = minio.New(config.C.S3.Endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(config.C.S3.AccessKey, config.C.S3.SecretKey, ""),
		Secure: false,
	})
	if err != nil {
		return errorx.Decorate(err, "unable to connect to S3 server")
	}

	return createBuckets()
}

func createBuckets() error {
	buckets := []string{
		config.C.S3.Buckets.Data,
	}

	ctx := context.Background()

	for _, bucket := range buckets {
		ok, err := cli.BucketExists(ctx, bucket)
		if err != nil {
			return errorx.Decorate(err, "unable to check if bucket already exists")
		} else if ok {
			continue
		}

		err = cli.MakeBucket(ctx, bucket, minio.MakeBucketOptions{})
		if err != nil {
			return errorx.Decorate(err, "unable to create bucket %q", bucket)
		}
	}

	return nil
}

// UploadThumbnail uploads the orig thumbnail of a website.
func UploadThumbnail(ctx context.Context, image io.Reader, bookmarkID uuid.UUID, pref string) (int64, error) {
	filePath := GenThumbnailPath(bookmarkID, pref)

	log.Debug("Uploading thumbnail", zap.String("s3_file_path", filePath), zap.Stringer("bookmark_id", bookmarkID))

	info, err := cli.PutObject(ctx, config.C.S3.Buckets.Data, filePath, image, -1, minio.PutObjectOptions{})
	if err != nil {
		return 0, errors.Wrap(err, "unable to upload image")
	}

	return info.Size, nil
}

// GenThumbnailPref returns the prefix string to be used with [GenThumbnailPath] and friends.
func GenThumbnailPref(t time.Time) string {
	return strconv.FormatInt(t.Unix(), 10)
}

// GenThumbnailPath returns the file path of the original user avatar image.
func GenThumbnailPath(bookmarkID uuid.UUID, pref string) string {
	return path.Join(prefThumbnail, bookmarkID.String(), pref, "website_thumbnail.png")
}

// GetThumbnail returns the neweset thumbnail.
func GetThumbnail(ctx context.Context, bookmarkID uuid.UUID, pref string) ([]byte, error) {
	filePath := GenThumbnailPath(bookmarkID, pref)

	f, err := cli.GetObject(ctx, config.C.S3.Buckets.Data, filePath, minio.GetObjectOptions{})
	if err != nil {
		return nil, errors.Wrap(err, "unable to download object from S3")
	}
	defer f.Close()

	data, err := io.ReadAll(f)
	if err != nil {
		return nil, errors.Wrap(err, "unable to read file data")
	}

	return data, nil
}
