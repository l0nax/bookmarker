package search

import (
	"context"
	"encoding/json"

	"github.com/google/uuid"
	"github.com/pkg/errors"

	"gitlab.com/l0nax/bookmarker/models"
)

type meiliSearchResponse struct {
	Hits               json.RawMessage `json:"hits"`
	EstimatedTotalHits int64           `json:"estimatedTotalHits"` //nolint:tagliatelle
}

type WebsiteContent struct {
	// ID is the bookmark ID.
	ID          uuid.UUID `json:"id"`
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Content     string    `json:"content"`
	Tags        []string  `json:"tags"`
	// UserIDs holds the IDs of the user which have access to this content entry.
	UserIDs []uuid.UUID `json:"user_ids"`
}

type SimpleSearchResult struct {
	ID             uuid.UUID `json:"id,omitempty"`
	URL            string    `json:"url,omitempty"`
	Title          string    `json:"title,omitempty"`
	Description    string    `json:"description,omitempty"`
	WebsiteContent string    `json:"website_content,omitempty"`
	Tags           []string  `json:"tags,omitempty"`
	Score          int       `json:"score"`
}

func SearchForBookmark(ctx context.Context, userID uuid.UUID, query string) ([]models.BookmarkSearchResult, error) {
	/*
		q := &meilisearch.SearchRequest, args ...interface{}{
			Filter:                fmt.Sprintf("user_ids IN [%s]", userID.String()),
			AttributesToHighlight: []string{"content"},
			AttributesToCrop:      []string{"content"},
			HighlightPreTag:       "__ais-highlight__",
			HighlightPostTag:      "__/ais-highlight__",
			CropLength:            20,
		}
	*/

	ret, err := models.SearchForBookmark(ctx, userID, query, 15)
	if err != nil {
		return nil, errors.Wrap(err, "unable to search")
	}

	return ret[:], nil
}
