package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	"github.com/uptrace/bun/migrate"
	"github.com/urfave/cli/v2"

	"gitlab.com/l0nax/bookmarker/cmd"
	"gitlab.com/l0nax/bookmarker/internal/config"
	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/migrations"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/models/listener"
)

var (
	migrator  *migrate.Migrator
	ctx       context.Context
	ctxCancel context.CancelFunc
)

func main() {
	cmds := []*cli.Command{
		newDBCommand(),
	}
	cmds = append(cmds, cmd.NewCommands()...)

	log.InitLogging("", "")
	ctx, ctxCancel = context.WithCancel(context.Background())

	app := &cli.App{
		Name: "server",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "config",
				Usage:   "path to the config file",
				Aliases: []string{"c"},
				Value:   "/config/config.yaml",
			},
		},
		Before: func(c *cli.Context) error {
			return untilError(
				func() error { return config.InitConfig(c.String("config")) },
				func() error {
					migrator = migrate.NewMigrator(nil, migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))
					return nil
				},
			)
		},
		Commands: cmds,
	}

	fatalIf(app.Run(os.Args), "")

	ctxCancel()
}

func fatalIf(err error, msg string) {
	if err == nil {
		return
	}

	fmt.Fprintf(os.Stderr, msg+": %+v\n", err)
	os.Exit(1)
}

func untilError(f ...func() error) error {
	for _, fc := range f {
		if err := fc(); err != nil {
			return err
		}
	}

	return nil
}

func newDBCommand() *cli.Command {
	return &cli.Command{
		Name:  "db",
		Usage: "database migrations",
		Before: func(c *cli.Context) error {
			_ = c

			// models.Connect(ctx, listener.NopHandler)
			return nil
		},
		Subcommands: []*cli.Command{
			{
				Name:  "init",
				Usage: "create migration tables",
				Action: func(c *cli.Context) error {
					// reinit full DB connection
					models.Connect(ctx, listener.Handler)
					migrator = migrate.NewMigrator(models.GetDB(), migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))

					return migrator.Init(c.Context)
				},
			},
			{
				Name:  "migrate",
				Usage: "migrate database",
				Action: func(c *cli.Context) error {
					// reinit full DB connection
					models.Connect(ctx, listener.Handler)
					migrator = migrate.NewMigrator(models.GetDB(), migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))

					if err := migrator.Lock(c.Context); err != nil {
						return err
					}
					defer migrator.Unlock(c.Context) //nolint:errcheck
					defer func() {
						if r := recover(); r != nil {
							_ = migrator.Unlock(c.Context)
						}
					}()

					group, err := migrator.Migrate(c.Context)
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no new migrations to run (database is up to date)\n")
						return nil
					}
					fmt.Printf("migrated to %s\n", group)

					return nil
				},
			},
			{
				Name:  "rollback",
				Usage: "rollback the last migration group",
				Action: func(c *cli.Context) error {
					// reinit full DB connection
					models.Connect(ctx, listener.Handler)
					migrator = migrate.NewMigrator(models.GetDB(), migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))

					if err := migrator.Lock(c.Context); err != nil {
						return err
					}
					defer migrator.Unlock(c.Context) //nolint:errcheck

					group, err := migrator.Rollback(c.Context)
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no groups to roll back\n")
						return nil
					}
					fmt.Printf("rolled back %s\n", group)
					return nil
				},
			},
			{
				Name:  "lock",
				Usage: "lock migrations",
				Action: func(c *cli.Context) error {
					return migrator.Lock(c.Context)
				},
			},
			{
				Name:  "unlock",
				Usage: "unlock migrations",
				Action: func(c *cli.Context) error {
					return migrator.Unlock(c.Context)
				},
			},
			{
				Name:  "create_go",
				Usage: "create Go migration",
				Action: func(c *cli.Context) error {
					name := strings.Join(c.Args().Slice(), "_")
					mf, err := migrator.CreateGoMigration(c.Context, name)
					if err != nil {
						return err
					}
					fmt.Printf("created migration %s (%s)\n", mf.Name, mf.Path)
					return nil
				},
			},
			{
				Name:  "create_sql",
				Usage: "create up and down SQL migrations",
				Action: func(c *cli.Context) error {
					name := strings.Join(c.Args().Slice(), "_")
					files, err := migrator.CreateSQLMigrations(c.Context, name)
					if err != nil {
						return err
					}

					for _, mf := range files {
						fmt.Printf("created migration %s (%s)\n", mf.Name, mf.Path)
					}

					return nil
				},
			},
			{
				Name:  "status",
				Usage: "print migrations status",
				Action: func(c *cli.Context) error {
					// reinit full DB connection
					models.Connect(ctx, listener.Handler)
					migrator = migrate.NewMigrator(models.GetDB(), migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))

					ms, err := migrator.MigrationsWithStatus(c.Context)
					if err != nil {
						return err
					}
					fmt.Printf("migrations: %s\n", ms)
					fmt.Printf("unapplied migrations: %s\n", ms.Unapplied())
					fmt.Printf("last migration group: %s\n", ms.LastGroup())
					return nil
				},
			},
			{
				Name:  "mark_applied",
				Usage: "mark migrations as applied without actually running them",
				Action: func(c *cli.Context) error {
					// reinit full DB connection
					models.Connect(ctx, listener.Handler)
					migrator = migrate.NewMigrator(models.GetDB(), migrations.Migrations, migrate.WithMarkAppliedOnSuccess(true))

					group, err := migrator.Migrate(c.Context, migrate.WithNopMigration())
					if err != nil {
						return err
					}
					if group.IsZero() {
						fmt.Printf("there are no new migrations to mark as applied\n")
						return nil
					}
					fmt.Printf("marked as applied %s\n", group)
					return nil
				},
			},
		},
	}
}
