SET statement_timeout = 0;

CREATE EXTENSION "uuid-ossp";

--bun:split

CREATE TABLE "user_info" (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  given_name TEXT NOT NULL,
  surname TEXT NOT NULL,
  email TEXT NOT NULL UNIQUE,
  active BOOLEAN NOT NULL DEFAULT TRUE,
  password TEXT NOT NULL
);

CREATE TABLE "token" (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  user_id UUID NOT NULL,
  token TEXT NOT NULL UNIQUE
);

ALTER TABLE ONLY "token"
  ADD CONSTRAINT fk_token_user_id_user_info
    FOREIGN KEY (user_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;

--bun:split

CREATE TABLE collection (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  owner_id UUID NOT NULL,
  parent_id UUID,
  title TEXT NOT NULL
);

ALTER TABLE ONLY "collection"
  ADD CONSTRAINT fk_collection_owner_id_user_info
    FOREIGN KEY (owner_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;

ALTER TABLE ONLY "collection"
  ADD CONSTRAINT fk_collection_parent_id_collection
    FOREIGN KEY (parent_id) REFERENCES collection(id) ON DELETE CASCADE DEFERRABLE;

--bun:split

CREATE TABLE bookmark (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  collection_id UUID NOT NULL,
  owner_id UUID NOT NULL,
  title TEXT NOT NULL,
  description TEXT,
  url TEXT NOT NULL
);

ALTER TABLE ONLY "bookmark"
  ADD CONSTRAINT fk_bookmark_owner_id_user_info
    FOREIGN KEY (owner_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;

ALTER TABLE ONLY "bookmark"
  ADD CONSTRAINT fk_bookmark_collection_id_collection
    FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE DEFERRABLE;

CREATE TABLE tag (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  owner_id UUID NOT NULL,
  name TEXT NOT NULL,
  description TEXT
);

ALTER TABLE ONLY "tag"
  ADD CONSTRAINT uniq_tag_owner_id_and_name
    UNIQUE(owner_id, name);

ALTER TABLE ONLY "tag"
  ADD CONSTRAINT fk_tag_owner_id_user_info
    FOREIGN KEY (owner_id) REFERENCES tag(id) ON DELETE CASCADE DEFERRABLE;


CREATE TABLE tag_to_bookmark(
  bookmark_id UUID NOT NULL,
  tag_id UUID NOT NULL,
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,

  PRIMARY KEY (bookmark_id, tag_id)
);

ALTER TABLE ONLY "tag_to_bookmark"
  ADD CONSTRAINT uniq_tag_to_bookmark_tag_id_bookmark_id
    UNIQUE(bookmark_id, tag_id);

ALTER TABLE ONLY "tag_to_bookmark"
  ADD CONSTRAINT fk_tag_to_bookmark_bookmark_id_tag
    FOREIGN KEY (bookmark_id) REFERENCES tag(id) ON DELETE CASCADE DEFERRABLE;

ALTER TABLE ONLY "tag_to_bookmark"
  ADD CONSTRAINT fk_tag_to_bookmark_tag_id_tag
    FOREIGN KEY (tag_id) REFERENCES tag(id) ON DELETE CASCADE DEFERRABLE;
