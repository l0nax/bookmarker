SET statement_timeout = 0;

--bun:split

CREATE TABLE collection_authorizations(
  user_id UUID NOT NULL,
  collection_id UUID NOT NULL,
  access_level SMALLINT NOT NULL,

  PRIMARY KEY (user_id, collection_id)
);

ALTER TABLE collection_authorizations
  ADD CONSTRAINT fk_collection_authorization_user_id_user_info
    FOREIGN KEY (user_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;
ALTER TABLE collection_authorizations
  ADD CONSTRAINT fk_collection_authorization_collection_id_collection
    FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE DEFERRABLE;
