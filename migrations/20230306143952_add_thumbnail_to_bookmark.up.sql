SET statement_timeout = 0;

--bun:split

CREATE TABLE upload_s3(
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  size BIGINT NOT NULL,
  path TEXT NOT NULL,
  checksum BYTEA,
  created_at TIMESTAMPTZ NOT NULL DEFAULT now(),
  model_id UUID,
  model_type SMALLINT
);

ALTER TABLE bookmark
  ADD COLUMN thumbnail_id UUID;
ALTER TABLE bookmark
  ADD CONSTRAINT fk_bookmark_thumbnail_id_upload_s3
    FOREIGN KEY (thumbnail_id) REFERENCES upload_s3(id) ON DELETE CASCADE DEFERRABLE;

--bun:split

CREATE TABLE gc_upload_s3_review_queue (
  path TEXT NOT NULL PRIMARY KEY,
  review_after TIMESTAMPTZ NOT NULL DEFAULT now() + INTERVAL '1 day',
  review_count INTEGER NOT NULL DEFAULT 0
);

CREATE INDEX idx_gc_upload_s3_review_queue_on_review_after
  ON gc_upload_s3_review_queue USING btree (review_after);


CREATE FUNCTION gc_track_upload_s3_delete()
  RETURNS TRIGGER
  AS $$
  DECLARE
    jitter_s INTERVAL;
    review_after_dt TIMESTAMPTZ;
BEGIN
  SELECT
    (random() * (60 - 5 + 1) + 5) * INTERVAL '1 second' INTO jitter_s;
  SELECT
    now() + INTERVAL '1 day' + jitter_s INTO review_after_dt;

  INSERT INTO gc_upload_s3_review_queue (path, review_after)
    VALUES (OLD.path, review_after_dt)
  ON CONFLICT (path)
    DO UPDATE SET
      review_after = review_after_dt;
  RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER gc_track_upload_s3_delete_trigger
  AFTER DELETE ON upload_s3 REFERENCING OLD TABLE AS old_table
  FOR EACH ROW
  EXECUTE PROCEDURE gc_track_upload_s3_delete();


CREATE FUNCTION gc_track_upload_s3_upload()
  RETURNS TRIGGER
  AS $$
  DECLARE
    jitter_s INTERVAL;
    review_after_dt TIMESTAMPTZ;
BEGIN
  SELECT
    (random() * (60 - 5 + 1) + 5) * INTERVAL '1 second' INTO jitter_s;
  SELECT
    now() + INTERVAL '1 day' + jitter_s INTO review_after_dt;

  INSERT INTO gc_upload_s3_review_queue (path, review_after)
    VALUES (NEW.path, review_after_dt)
  ON CONFLICT (path)
    DO UPDATE SET
      review_after = review_after_dt;
  RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE TRIGGER gc_track_upload_s3_upload_trigger
  AFTER INSERT ON upload_s3
  FOR EACH ROW
  EXECUTE PROCEDURE gc_track_upload_s3_upload();

CREATE TRIGGER gc_track_upload_s3_update_trigger
  AFTER UPDATE ON upload_s3
  FOR EACH ROW
  EXECUTE PROCEDURE gc_track_upload_s3_upload();
