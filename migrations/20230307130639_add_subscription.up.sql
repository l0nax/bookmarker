SET statement_timeout = 0;

--bun:split

CREATE TABLE subscription (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  user_id UUID,
  period_start TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  period_end TIMESTAMPTZ,
  plan_id SMALLINT NOT NULL
);

CREATE INDEX index_subscription_on_user_id
  ON subscription (user_id);

CREATE INDEX index_subscription_on_period_start_and_period_end
  ON subscription (period_start, period_end);

-- If the user gets deleted, we "assign" the subscription data to the ghost user.
ALTER TABLE ONLY subscription
  ADD CONSTRAINT fk_subscription_user_id_user_info
    FOREIGN KEY (user_id) REFERENCES user_info(id)
    ON DELETE SET NULL;
