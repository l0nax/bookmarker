SET statement_timeout = 0;

--bun:split

CREATE OR REPLACE FUNCTION bookmark_created_trigger()
  RETURNS TRIGGER
  AS $$
BEGIN
  -- Notify listeners allowing us to index the website
  PERFORM pg_notify('bookmark:created', NEW.id::TEXT);

  RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION bookmark_deleted_trigger()
  RETURNS TRIGGER
  AS $$
BEGIN
  -- Notify listeners allowing us to index the website
  PERFORM pg_notify('bookmark:deleted', OLD.id::TEXT);

  RETURN NULL;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE TRIGGER bookmark_after_insert_index
  AFTER INSERT ON bookmark
  FOR EACH ROW
  EXECUTE PROCEDURE bookmark_created_trigger();

CREATE OR REPLACE TRIGGER bookmark_after_delete_cleanup
  AFTER DELETE ON bookmark
  FOR EACH ROW
  EXECUTE PROCEDURE bookmark_deleted_trigger();
