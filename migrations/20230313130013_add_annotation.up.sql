SET statement_timeout = 0;

--bun:split

CREATE TABLE annotation (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  owner_id UUID NOT NULL,
  url TEXT NOT NULL,
  page_title TEXT,
  page_url TEXT NOT NULL,
  body TEXT,
  comment TEXT,
  bookmark_id UUID
);

ALTER TABLE annotation
  ADD CONSTRAINT fk_annotation_owner_id_user_info
    FOREIGN KEY (owner_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;

ALTER TABLE annotation
  ADD CONSTRAINT fk_annotation_bookmark_id_bookmark
    FOREIGN KEY (bookmark_id) REFERENCES bookmark(id) ON DELETE CASCADE DEFERRABLE;

CREATE TABLE highlight_anchor (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  annotation_id UUID NOT NULL,
  quote TEXT NOT NULL DEFAULT ''
);

ALTER TABLE highlight_anchor
  ADD CONSTRAINT fk_highlight_anchor_annotation_id_annotation
    FOREIGN KEY (annotation_id) REFERENCES annotation(id) ON DELETE CASCADE DEFERRABLE;

CREATE TABLE highlight_anchor_descriptor (
  anchor_id UUID NOT NULL,
  strategy TEXT NOT NULL,
  content JSONB
);


ALTER TABLE highlight_anchor_descriptor
  ADD CONSTRAINT fk_highlight_anchor_descriptor_anchor_id_highlight_anchor
    FOREIGN KEY (anchor_id) REFERENCES highlight_anchor(id) ON DELETE CASCADE DEFERRABLE;
