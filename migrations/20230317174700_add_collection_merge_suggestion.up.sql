SET statement_timeout = 0;

--bun:split

CREATE OR REPLACE FUNCTION enqueue_collection_merge_suggestions(user_id UUID, name TEXT, ids UUID[])
  RETURNS VOID
  AS $$
  DECLARE
    s UUID;
    new_cms_id UUID;
BEGIN
  SELECT uuid_generate_v4()
    INTO new_cms_id;

  INSERT INTO collection_merge_suggestion(id, name, user_id)
    VALUES(new_cms_id, name, user_id);

  FOREACH s IN ARRAY ids
  LOOP
    INSERT INTO collection_merge_suggestion_ref(cms_id, collection_id)
      VALUES(new_cms_id, s);
  END LOOP;
END;
$$
LANGUAGE plpgsql;

--bun:split

CREATE TABLE collection_merge_suggestion (
  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
  created_at TIMESTAMPTZ DEFAULT CURRENT_TIMESTAMP NOT NULL,
  user_id UUID NOT NULL,
  name TEXT NOT NULL,
  dismissed BOOLEAN NOT NULL DEFAULT FALSE
);

CREATE TABLE collection_merge_suggestion_ref (
  cms_id UUID NOT NULL,
  collection_id UUID NOT NULL
);

CREATE INDEX index_collection_merge_suggestion_ref_on_cms_id
  ON collection_merge_suggestion_ref (cms_id);

CREATE INDEX index_collection_merge_suggestion_on_user_id
  ON collection_merge_suggestion (user_id);

ALTER TABLE ONLY "collection_merge_suggestion_ref"
  ADD CONSTRAINT fk_collection_merge_suggestion_ref_collection_merge_suggestion_id_collection_merge_suggestion
    FOREIGN KEY (cms_id) REFERENCES collection_merge_suggestion(id) ON DELETE CASCADE DEFERRABLE;

ALTER TABLE ONLY "collection_merge_suggestion"
  ADD CONSTRAINT fk_collection_merge_suggestion_user_info_user_id
    FOREIGN KEY (user_id) REFERENCES user_info(id) ON DELETE CASCADE DEFERRABLE;
