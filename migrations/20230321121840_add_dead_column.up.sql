SET statement_timeout = 0;

--bun:split

ALTER TABLE bookmark
  ADD COLUMN is_dead BOOLEAN NOT NULL DEFAULT FALSE;

ALTER TABLE bookmark
  ADD COLUMN last_alive_check TIMESTAMPTZ;

--bun:split

CREATE RECURSIVE VIEW view_collection_tree(id, parent_id, title) AS
  SELECT
    id,
    parent_id,
    title
  FROM
    collection
  WHERE
    parent_id IS NULL

UNION ALL

  SELECT
    c.id,
    c.parent_id,
    ct.title || ' / ' || c.title
  FROM
    collection c
  INNER JOIN view_collection_tree ct
    ON c.parent_id = ct.id;
