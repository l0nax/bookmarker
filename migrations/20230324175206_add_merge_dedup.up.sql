SET statement_timeout = 0;

--bun:split

CREATE OR REPLACE FUNCTION migrate_annotations_to_bookmark(target_bookmark_id UUID, ids UUID[])
  RETURNS VOID
  AS $$
  DECLARE
    b UUID;
BEGIN
  FOREACH b IN ARRAY ids
  LOOP
    UPDATE annotation
    SET
      bookmark_id = target_bookmark_id
    WHERE
      bookmark_id = b;

    DELETE FROM bookmark
    WHERE id = b;
  END LOOP;
END;
$$
LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION process_duplicate_bookmarks()
  RETURNS VOID
  AS $$
  DECLARE
    b UUID[];
    target_bookmark UUID;
    ids UUID[];
    entry UUID;
    data_count INTEGER;
BEGIN
  SELECT COALESCE(ARRAY_AGG(id), ARRAY []::UUID[])
  FROM tmp_duplicate_bookmarks
    INTO b;
  SELECT ARRAY_LENGTH(b, 1)
    INTO data_count;

  IF data_count < 0 THEN
    RETURN;
  END IF;

  FOREACH entry IN ARRAY b
  LOOP
    SELECT bookmark_ids[1]
    FROM tmp_duplicate_bookmarks
    WHERE id = entry
      INTO target_bookmark;

    SELECT bookmark_ids[2:]
    FROM tmp_duplicate_bookmarks
    WHERE id = entry
      INTO ids;

    PERFORM migrate_annotations_to_bookmark(target_bookmark, ids);
  END LOOP;
END;
$$
LANGUAGE plpgsql;
