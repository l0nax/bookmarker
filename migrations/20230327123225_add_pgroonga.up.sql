SET statement_timeout = 0;

--bun:split

ALTER TABLE bookmark
  ADD COLUMN IF NOT EXISTS website_content TEXT;

--bun:split

CREATE INDEX IF NOT EXISTS index_bookmark_on_website_content_and_title_and_description
  ON bookmark
  USING pgroonga ((ARRAY[title, description, website_content]));

UPDATE bookmark
SET ft_indexed = FALSE;
