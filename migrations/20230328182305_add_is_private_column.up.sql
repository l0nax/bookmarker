SET statement_timeout = 0;

--bun:split

ALTER TABLE bookmark
  ADD COLUMN IF NOT EXISTS is_private BOOLEAN NOT NULL DEFAULT FALSE;

-- We force the server to re-do the existence checks
UPDATE bookmark
SET is_dead = FALSE,
    last_alive_check = now() - INTERVAL '90 days';
