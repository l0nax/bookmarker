package models

// Access describes an access level.
type Access int16

const (
	// AccessNoAccess describes the "no access" level.
	AccessNoAccess   = Access(0)
	AccessGuest      = Access(10)
	AccessReader     = Access(20)
	AccessEditor     = Access(30)
	AccessMaintainer = Access(40)
	AccessOwner      = Access(50)
)
