package models

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"
)

// bookmarkFilterFunc wrapps [bunSelectFilterFunc] and can be used for
// [Bookmark].
type bookmarkFilterFunc[T bunQuery] bunSelectFilterFunc[T]

// bookmarkFilters represent multiple [bookmarkFilterFunc]s.
type bookmarkFilters[T bunQuery] []bookmarkFilterFunc[T]

// bookmarkSelectFilters reprents select filters of [bookmarkFilterFunc].
type bookmarkSelectFilters bookmarkFilters[*bun.SelectQuery]

func applyBookmarkSelectFilters(q *bun.SelectQuery, funcs bookmarkSelectFilters) *bun.SelectQuery {
	if len(funcs) == 0 {
		return q
	}

	for _, f := range funcs {
		q = f(q)
	}

	return q
}

// Bookmark is a single bookmark entry.
type Bookmark struct {
	bun.BaseModel `bun:"bookmark"`

	ID             uuid.UUID     `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt      time.Time     `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	UpdatedAt      time.Time     `bun:",nullzero,notnull,default:current_timestamp" json:"updated_at"`
	ThumbnailID    uuid.NullUUID `bun:",nullzero,type:uuid" json:"thumbnail_id"`
	OwnerID        uuid.UUID     `json:"owner_id"`
	CollectionID   uuid.UUID     `json:"collection_id"`
	Title          string        `bun:",type:text,nullzero" json:"title"`
	Description    string        `json:"description"`
	URL            string        `json:"url"`
	WebsiteContent string        `json:"website_content"`

	// FTIndexed describes if the website content has been indexed.
	FTIndexed bool `bun:"ft_indexed" json:"ft_indexed"`

	IsDead         bool      `bun:"is_dead" json:"is_dead"`
	LastAliveCheck time.Time `bun:"last_alive_check,nullzero" json:"last_alive_check"`

	IsPrivate bool `bun:"is_private" json:"is_private"`
}

func (b *Bookmark) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewInsert().
		Model(b).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert bookmark")
	}

	return nil
}

func (b *Bookmark) Update(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	q := db.NewUpdate().
		Model(b).
		Set("updated_at = NOW()").
		Where("id = ?", b.ID)

	if b.ThumbnailID.Valid {
		q.Set("thumbnail_id = ?", b.ThumbnailID.UUID)
	}

	if b.Title != "" {
		q.Set("title = ?", b.Title)
	}

	if b.Description != "" {
		q.Set("description = ?", b.Description)
	}

	if b.CollectionID != uuid.Nil {
		q.Set("collection_id = ?", b.CollectionID)
	}

	if _, err := q.Exec(ctx); err != nil {
		return errorx.Decorate(err, "unable to update bookmark entry")
	}

	return nil
}

func (b *Bookmark) UpdateForUser(ctx context.Context, userID uuid.UUID, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	q := db.NewUpdate().
		Model(b).
		Set("updated_at = NOW()").
		Where("id = ?", b.ID).
		Where("owner_id = ?", userID)

	if b.Title != "" {
		q.Set("title = ?", b.Title)
	}

	if b.Description != "" {
		q.Set("description = ?", b.Description)
	}

	if b.CollectionID != uuid.Nil {
		q.Set("collection_id = ?", b.CollectionID)
	}

	if b.IsPrivate {
		q.Set("is_private = TRUE")
	}

	if _, err := q.Exec(ctx); err != nil {
		return errorx.Decorate(err, "unable to update bookmark entry")
	}

	return nil
}

func (b *Bookmark) UpdateLiveness(ctx context.Context, isAlive bool, date time.Time) error {
	_, err := db.NewUpdate().
		Model(b).
		Where("id = ?", b.ID).
		Set("is_private = FALSE").
		Set("is_dead = ?", !isAlive).
		Set("last_alive_check = ?", date).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to update liveness metadata")
	}

	return nil
}

func (b *Bookmark) UpdatePrivate(ctx context.Context, isPrivate bool) error {
	_, err := db.NewUpdate().
		Model(b).
		Where("id = ?", b.ID).
		Set("is_private = ?", isPrivate).
		Set("last_alive_check = now()").
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to update liveness metadata")
	}

	return nil
}

func AddBookmarkWebsiteContent(ctx context.Context, id uuid.UUID, content string) error {
	_, err := db.NewUpdate().
		Model((*Bookmark)(nil)).
		Where("id = ?", id).
		Set("ft_indexed = TRUE").
		Set("website_content = ?", content).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to add website_content to bookmark")
	}

	return nil
}

func GetBookmarkThumbnail(ctx context.Context, user, bookmarkID uuid.UUID) (UploadS3, error) {
	var ret UploadS3

	err := db.NewSelect().
		With("bookmarks_cte", bookmarkAuthCTE(user)).
		Table("upload_s3").
		ColumnExpr(`upload_s3.*`).
		Join(`INNER JOIN bookmarks_cte b
				ON b.thumbnail_id = upload_s3.id`).
		Where("b.id = ?", bookmarkID).
		Limit(1).
		Scan(ctx, &ret)
	if err != nil {
		return ret, errorx.Decorate(err, "unable to fetch bookmark")
	}

	return ret, nil
}

func GetBookmarkByID(ctx context.Context, id uuid.UUID) (Bookmark, error) {
	var b Bookmark

	err := db.NewSelect().
		Model(&b).
		Where("id = ?", id).
		Scan(ctx)
	if err != nil {
		return b, errorx.Decorate(err, "unable to fetch bookmark by ID")
	}

	return b, nil
}

func GetBookmarkByIDAuth(ctx context.Context, userID, id uuid.UUID) (Bookmark, error) {
	var b Bookmark

	err := db.NewSelect().
		With("bookmarks_cte", bookmarkAuthCTE(userID)).
		TableExpr(`"bookmarks_cte" AS "b"`).
		Where("b.id = ?", id).
		Scan(ctx, &b)
	if err != nil {
		return b, errorx.Decorate(err, "unable to fetch bookmark by ID")
	}

	return b, nil
}

func GetBookmarkURLByID(ctx context.Context, id uuid.UUID) (string, error) {
	var url string

	err := db.NewSelect().
		Model((*Bookmark)(nil)).
		Column("url").
		Where("id = ?", id).
		Scan(ctx, &url)
	if err != nil {
		return "", errorx.Decorate(err, "unable to fetch bookmark by ID")
	}

	return url, nil
}

func GetBookmarkByURL(ctx context.Context, userID uuid.UUID, url string) (Bookmark, error) {
	var err error

	url, err = normalizeURL(url)
	if err != nil {
		return Bookmark{}, errors.Wrap(err, "unable to normalize URL")
	}

	var b Bookmark

	err = db.NewSelect().
		With("bookmarks_cte", bookmarkAuthCTE(userID)).
		TableExpr(`"bookmarks_cte" AS "b"`).
		Where("b.url = ?", url).
		Scan(ctx, &b)
	if err != nil {
		return Bookmark{}, errors.Wrap(err, "unable to select bookmark by URL")
	}

	return b, nil
}

func GetBookmarksOfCollection(ctx context.Context, user, cid uuid.UUID, filter bookmarkSelectFilters) ([]Bookmark, error) {
	ret := make([]Bookmark, 0, 10)

	q := db.NewSelect().
		With("bookmarks_cte", bookmarkAuthCTE(user)).
		TableExpr(`"bookmarks_cte" AS "b"`).
		ColumnExpr("b.*").
		Where("b.collection_id = ?", cid)

	q = applyBookmarkSelectFilters(q, filter)

	if err := q.Scan(ctx, &ret); err != nil {
		return nil, errorx.Decorate(err, "unable to retrieve bookmarks")
	}

	return ret, nil
}

func UpdateBookmarkFTIndexed(ctx context.Context, id uuid.UUID, indexed bool) error {
	_, err := db.NewUpdate().
		Model((*Bookmark)(nil)).
		Set("ft_indexed = ?", indexed).
		Where("id = ?", id).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to update ft_indexed of bookmark")
	}

	return nil
}

func bookmarkAuthCTE(userID uuid.UUID, dbs ...bun.IDB) *bun.SelectQuery {
	db := getDB(dbs...)

	return db.NewSelect().
		TableExpr("bookmark b").
		ColumnExpr(`"b".*`).
		Join(`
			INNER JOIN
			  "collection_authorizations" ca ON
			    b."collection_id" = ca."collection_id"`).
		Where("ca.user_id = ?", userID).
		Where("ca.access_level > ?", AccessNoAccess)
}

func GetWebsitesNotIndexed(ctx context.Context) ([]Bookmark, error) {
	ret := make([]Bookmark, 0, 1024)

	err := db.NewSelect().
		Model((*Bookmark)(nil)).
		Column("id").
		Column("title").
		Column("description").
		Column("url").
		Column("owner_id").
		Where("is_private IS NOT TRUE").
		Where("ft_indexed IS NOT TRUE").
		Where("is_dead IS NOT TRUE").
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to fetch missing bookmark data")
	}

	return ret, nil
}

func GetBookmarksToCheck(ctx context.Context, limit uint) ([]uuid.UUID, error) {
	ret := make([]uuid.UUID, 0, limit)

	err := db.NewSelect().
		Model((*Bookmark)(nil)).
		Column("id").
		Where("last_alive_check IS NULL").
		WhereOr("last_alive_check <= now() + '-10 days'").
		Limit(int(limit)).
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to retrieve bookmarks")
	}

	return ret, nil
}

func GetDeadLinksOfUser(ctx context.Context, userID uuid.UUID) (uint32, error) {
	var count uint32

	err := db.NewSelect().
		ColumnExpr(`COUNT(1) AS dead_links`).
		Table("bookmark").
		Where("owner_id = ?", userID).
		Where("is_dead IS TRUE").
		Where("is_private IS NOT TRUE").
		Scan(ctx, &count)
	if err != nil {
		return 0, errors.Wrap(err, "unable to count dead links")
	}

	return count, nil
}

type BrokenBookmark struct {
	ID            uuid.UUID `bun:"id"`
	URL           string    `bun:"url"`
	Title         string    `bun:"title"`
	ComponentPath string    `bun:"component_path"`
}

func GetBrokenBookmarksOfUser(ctx context.Context, userID uuid.UUID) ([]BrokenBookmark, error) {
	ret := make([]BrokenBookmark, 0, 100)

	err := db.NewSelect().
		ColumnExpr("b.id").
		ColumnExpr("b.title").
		ColumnExpr("b.url").
		ColumnExpr(`
		(
			SELECT vct.title
			FROM view_collection_tree vct
			WHERE vct.id = b.collection_id
		) AS component_path
	`).
		TableExpr("bookmark b").
		Where("owner_id = ?", userID).
		Where("is_dead IS TRUE").
		Where("is_private IS TRUE").
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to retrieve links")
	}

	return ret, nil
}

func DeleteBookmark(ctx context.Context, userID, bookmarkID uuid.UUID) error {
	_, err := db.NewDelete().
		Model((*Bookmark)(nil)).
		Where("id = ?", bookmarkID).
		Where("owner_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to delete bookmark")
	}

	return nil
}

type BookmarkSearchResult struct {
	ID             uuid.UUID `json:"id,omitempty" bun:"id"`
	URL            string    `json:"url,omitempty" bun:"url"`
	Title          string    `json:"title,omitempty" bun:"title"`
	Description    string    `json:"description,omitempty" bun:"description"`
	QueryKeywords  []string  `bun:"query_keywords,array" json:"query_keywords"`
	WebsiteContent string    `json:"website_content,omitempty" bun:"highlighted_content"`
	Tags           []string  `json:"tags,omitempty" bun:"tags"`
	Score          float32   `json:"score,omitempty" bun:"score"`
}

func SearchForBookmark(ctx context.Context, userID uuid.UUID, query string, limit uint) ([]BookmarkSearchResult, error) {
	ret := make([]BookmarkSearchResult, 0, limit)

	// TODO: Allow searching in shared bookmarks/ collections

	err := db.NewSelect().
		Model((*Bookmark)(nil)).
		Column("id").
		Column("url").
		Column("title").
		Column("description").
		ColumnExpr(`
			(
			  '…' ||
			  ARRAY_TO_STRING(
			    pgroonga_snippet_html(website_content, pgroonga_query_extract_keywords(?), 50),
			    '…'
			  )
			)	AS highlighted_content
		`, query).
		ColumnExpr(`pgroonga_query_extract_keywords(?) AS query_keywords`, query).
		ColumnExpr(`pgroonga_score(tableoid, ctid)	   AS score`).
		Where("owner_id = ?", userID).
		Where(`
			ARRAY[title, description, website_content] &@~
			  (
       		     ?,
       		     ARRAY[15, 12, 10],
       		     'index_bookmark_on_website_content_and_title_and_description'
       		  )::pgroonga_full_text_search_condition
		`, query).
		Order("score DESC").
		Limit(int(limit)).
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to search for bookmark")
	}

	return ret, nil
}
