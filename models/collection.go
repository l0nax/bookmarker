package models

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"

	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

// collectionBaseFilter applies base filter to the collection.
func collectionBaseFilter(userID uuid.UUID, q *bun.SelectQuery) *bun.SelectQuery {
	if !license.FeatureAvailable(userID, feature.NestedCollections) {
		return q.ColumnExpr("NULL AS parent_id")
	}

	return q
}

type Collection struct {
	bun.BaseModel `bun:"collection"`

	ID        uuid.UUID     `bun:",pk,nullzero,type:uuid" json:"id,omitempty"`
	CreatedAt time.Time     `bun:",nullzero,notnull,default:current_timestamp" json:"created_at,omitempty"`
	UpdatedAt time.Time     `bun:",nullzero,notnull,default:current_timestamp" json:"updated_at,omitempty"`
	OwnerID   uuid.UUID     `json:"owner_id,omitempty"`
	ParentID  uuid.NullUUID `json:"parent_id,omitempty"`
	Title     string        `json:"title,omitempty"`
}

func (c *Collection) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewInsert().
		Model(c).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert collection")
	}

	return nil
}

func (c *Collection) Delete(ctx context.Context) error {
	_, err := db.NewDelete().
		Model(c).
		Where("id = ?", c.ID).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete collection")
	}

	return nil
}

func (c *Collection) Update(ctx context.Context) error {
	q := db.NewUpdate().
		Model(c).
		Where("id = ?", c.ID).
		Set("updated_at = NOW()")

	if c.ParentID.Valid {
		if c.ParentID.UUID == uuid.Nil {
			q.Set("parent_id = NULL")
		} else {
			q.Set("parent_id = ?", c.ParentID.UUID)
		}
	}
	if c.Title != "" {
		q.Set("title = ?", c.Title)
	}

	err := q.
		Returning("*").
		Scan(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to update collection")
	}

	return nil
}

func GetCollectionByID(ctx context.Context, userID, colID uuid.UUID) (Collection, error) {
	ret := Collection{}

	err := db.NewSelect().
		With("collections_cte", collectionAuthCTE(userID)).
		TableExpr(`"collections_cte" AS "c"`).
		ColumnExpr("c.*").
		Where("c.id = ?", colID).
		Scan(ctx, &ret)
	if err != nil {
		return ret, errorx.Decorate(err, "unable to retrieve collection by ID")
	}

	return ret, nil
}

func collectionAuthCTE(userID uuid.UUID, dbs ...bun.IDB) *bun.SelectQuery {
	return collectionAuthCTELvl(userID, AccessNoAccess, dbs...)
}

func collectionAuthCTELvl(userID uuid.UUID, lvl Access, dbs ...bun.IDB) *bun.SelectQuery {
	db := getDB(dbs...)

	q := db.NewSelect().
		TableExpr("collection c").
		ColumnExpr(`"c".*`).
		Join(`
			INNER JOIN
			  "collection_authorizations" ca ON
			    c."id" = ca."collection_id"`).
		Where("ca.user_id = ?", userID).
		Where("ca.access_level > ?", lvl)

	return collectionBaseFilter(userID, q)
}

func GetAllCollectionsSimpleTree(ctx context.Context, userID uuid.UUID) ([]Collection, error) {
	if !license.FeatureAvailable(userID, feature.NestedCollections) {
		return []Collection{}, nil
	}

	col := make([]Collection, 0, 10)

	// NOTE: Current solution: Return ID+ParentID and use something like `https://www.npmjs.com/package/performant-array-to-tree`
	// to build the folder structure.
	// Why?
	//  ==> We may return the full PRE-GENERATED TREE from the API (in a later stage)
	//      but for now, it is sufficient to return a simple array.

	rec := db.NewSelect().
		ColumnExpr("c.id").
		ColumnExpr("c.parent_id").
		ColumnExpr("ct.title || ' / ' || c.title").
		TableExpr("collection c").
		Join(`
		INNER JOIN collection_tree ct
		  ON c.parent_id = ct.id`).
		Where("c.owner_id = ?", userID)

	cteSel := db.NewSelect().
		Model((*Collection)(nil)).
		Column("id").
		Column("parent_id").
		Column("title").
		Where("owner_id = ?", userID).
		Where("parent_id IS NULL").
		UnionAll(rec)

	q := db.NewSelect().
		TableExpr("collection_tree").
		WithRecursive("collection_tree", cteSel).
		ColumnExpr("DISTINCT(id)").
		ColumnExpr("*")

	err := q.Scan(ctx, &col)
	if err != nil {
		return nil, errorx.Decorate(err, "unable to retrieve collection list")
	}

	return col, nil
}

func ResolveCollectionsToFullPath(ctx context.Context, userID uuid.UUID, ids []uuid.UUID) (map[uuid.UUID]string, error) {
	m := make(map[uuid.UUID]string, len(ids))

	rec := db.NewSelect().
		ColumnExpr("c.id").
		ColumnExpr("c.parent_id").
		ColumnExpr("ct.title || ' / ' || c.title").
		TableExpr("collection c").
		Join(`
		INNER JOIN collection_tree ct
		  ON c.parent_id = ct.id`).
		Where("c.owner_id = ?", userID)

	cteSel := db.NewSelect().
		Model((*Collection)(nil)).
		Column("id").
		Column("parent_id").
		Column("title").
		Where("owner_id = ?", userID).
		Where("parent_id IS NULL").
		UnionAll(rec)

	// WARN: This is a n+1 query!!
	type tmpData struct {
		ID    uuid.UUID
		Title string
	}

	for i := range ids {
		var tmp tmpData

		err := db.NewSelect().
			WithRecursive("collection_tree", cteSel).
			ColumnExpr("id").
			ColumnExpr("title").
			TableExpr("collection_tree").
			Where("id = ?", ids[i]).
			Scan(ctx, &tmp)
		if err != nil {
			return nil, errors.Wrap(err, "unable to resolve collections to full path")
		}

		m[tmp.ID] = tmp.Title
	}

	return m, nil
}

func GetAllCollections(ctx context.Context, userID uuid.UUID) ([]Collection, error) {
	col := make([]Collection, 0, 10)

	// NOTE: Current solution: Return ID+ParentID and use something like `https://www.npmjs.com/package/performant-array-to-tree`
	// to build the folder structure.
	// Why?
	//  ==> We may return the full PRE-GENERATED TREE from the API (in a later stage)
	//      but for now, it is sufficient to return a simple array.

	q := db.NewSelect().
		Model((*Collection)(nil)).
		ColumnExpr("*").
		Where("owner_id = ?", userID)
	q = collectionBaseFilter(userID, q)

	err := q.Scan(ctx, &col)
	if err != nil {
		return nil, errorx.Decorate(err, "unable to retrieve collection list")
	}

	/**
	CORRECT QUERY:

	CREATE OR REPLACE VIEW rec_collection_view AS
		SELECT
			col."id",
			col."parent_id",
			replace(col."title", '/', '\/') AS "title"
		FROM collection col
	;



	WITH RECURSIVE "cols" AS (
		SELECT
			"id",
			"parent_id",
			"title",
			"title" AS "path",
			ARRAY["id", "parent_id"] AS "constel"
		FROM rec_collection_view

		UNION ALL

		SELECT
			d."id",
			d."parent_id",
			d."title",
			cols."path" || '/' || d."title" AS "path",
			(d."parent_id" || ARRAY[cols."parent_id"]) AS "constel"
		FROM rec_collection_view d
		INNER JOIN cols
		  ON d.parent_id = cols.id
		  AND cols."parent_id" != ALL(cols."constel")
	)
	SELECT COUNT(1)
	FROM cols;

		**/
	/*

		recSel := db.NewSelect().
			TableExpr("collection c_rec").
			ColumnExpr(`c_rec."id"`).
			ColumnExpr(`c_rec."created_at"`).
			ColumnExpr(`c_rec."updated_at"`).
			ColumnExpr(`c_rec."parent_id"`).
			ColumnExpr(`c_rec."title"`).
			ColumnExpr(`cols.path || '/' || c_rec."title"`).
			Join(`
				INNER JOIN cols
				  ON cols.parent_id = c_rec.id
			`)

			//Model((*Collection)(nil)).
		q := db.NewSelect().
			TableExpr("collection c").
			ColumnExpr(`c."id"`).
			ColumnExpr(`c."created_at"`).
			ColumnExpr(`c."updated_at"`).
			ColumnExpr(`c."parent_id"`).
			ColumnExpr(`c."title"`).
			ColumnExpr(`REPLACE(title, '/', '\/') AS path`).
			Where("owner_id = ?", userID).
			UnionAll(recSel)

		err := db.NewSelect().
			WithRecursive("cols", q).
			Table("cols").
			Scan(ctx, &col)
		if err != nil {
			return nil, errorx.Decorate(err, "unable to retrieve collections")
		}
	*/

	return col, nil
}

// MarshalBinary implements the Binary marshaller to be compatible with redis.
func (c Collection) MarshalBinary() (data []byte, err error) {
	bytes, err := json.Marshal(c)
	return bytes, err
}

// UnmarshalBinary implements the Binary unmarshaller to be compatible with redis.
func (c *Collection) UnmarshalBinary(data []byte) error {
	return json.Unmarshal(data, c)
}
