package models

import (
	"context"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/uptrace/bun"
)

/*
    Column    |  Type   | Collation | Nullable | Default | Storage | Compression | Stats target | Description
--------------+---------+-----------+----------+---------+---------+-------------+--------------+-------------
 user_id      | integer |           | not null |         | plain   |             |              |
 project_id   | integer |           | not null |         | plain   |             |              |
 access_level | integer |           | not null |         | plain   |             |              |
Indexes:
    "project_authorizations_pkey" PRIMARY KEY, btree (user_id, project_id, access_level)
    "index_unique_project_authorizations_on_project_id_user_id" UNIQUE, btree (project_id, user_id)
*/

// CollectionAuthorizations is a helper/ caching table holding user and collection authorization.
type CollectionAuthorizations struct {
	bun.BaseModel `bun:"collection_authorizations"`

	UserID       uuid.UUID `bun:"user_id,zeronull,notnull,type:uuid" json:"user_id"`
	CollectionID uuid.UUID `bun:"collection_id,type:uuid" json:"collection_id"`
	AccessLevel  Access    `bun:"access_level,type:smallint" json:"access_level"`
}

func RecalcCollectionAuths(ctx context.Context, userID uuid.UUID) error {
	db, err := StartTx(ctx, nil)
	if err != nil {
		return errorx.Decorate(err, "unable to start transaction")
	}
	defer db.Rollback()

	if err := delUserCollectionAuths(ctx, userID, db); err != nil {
		return errorx.Decorate(err, "unable to delete user entries")
	}

	_, err = db.QueryContext(ctx, `
		INSERT INTO collection_authorizations(user_id, collection_id, access_level)
		  SELECT
		    ?::UUID AS user_id,
		    id AS collection_id,
		    ? AS access_level
		  FROM
		    collection
		  WHERE
		  owner_id = ?::UUID
		ON CONFLICT DO NOTHING
		`, userID, AccessOwner, userID)
	if err != nil {
		return errorx.Decorate(err, "unable to insert authorization")
	}

	return db.Commit()
}

func delUserCollectionAuths(ctx context.Context, userID uuid.UUID, db bun.IDB) error {
	_, err := db.NewDelete().
		Table("collection_authorizations").
		Where("user_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete")
	}

	return nil
}
