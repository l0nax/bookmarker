package models

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"github.com/samber/lo"
	"github.com/uptrace/bun"
	"github.com/uptrace/bun/dialect/pgdialect"
)

type CollectionMergeSuggestion struct {
	bun.BaseModel `bun:"collection_merge_suggestion"`

	ID        uuid.UUID `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	UserID    uuid.UUID `bun:",type:uuid" json:"user_id"`
	Name      string    `bun:",type:text" json:"name"`
	Dismissed bool      `bun:"dismissed" json:"dismissed"`
}

func (c *CollectionMergeSuggestion) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewInsert().
		Model(c).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert collection_merge_suggestion")
	}

	return nil
}

type CollectionMergeSuggestionRef struct {
	bun.BaseModel `bun:"collection_merge_suggestion"`

	CMSID        uuid.UUID `bun:"cms_id,pk,nullzero,type:uuid" json:"cms_id"`
	CollectionID uuid.UUID `bun:"collection_id,type:uuid" json:"collection_id"`
}

func (c *CollectionMergeSuggestionRef) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewInsert().
		Model(c).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert collection_merge_suggestion_ref")
	}

	return nil
}

func UpdateDismissCMSOfUser(ctx context.Context, userID, cmsID uuid.UUID, val bool) error {
	_, err := db.NewUpdate().
		Model((*CollectionMergeSuggestion)(nil)).
		Set("dismissed = ?", val).
		Where("id = ?", cmsID).
		Where("user_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to update CMS entry")
	}

	return nil
}

func DeleteCMSByIDOfUser(ctx context.Context, id, userID uuid.UUID, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewDelete().
		Model((*CollectionMergeSuggestion)(nil)).
		Where("id = ?", id).
		Where("user_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete CMS")
	}

	return nil
}

func DeleteCMSRefByIDOfUser(ctx context.Context, collectionID, userID uuid.UUID, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewDelete().
		Model((*CollectionMergeSuggestionRef)(nil)).
		Where("collection_id = ?", collectionID).
		Where("user_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete CMS Ref")
	}

	return nil
}

func DoesCMSExistByIDOfUser(ctx context.Context, id, userID uuid.UUID, dbs ...bun.IDB) (bool, error) {
	db := getDB(dbs...)

	exists, err := db.NewSelect().
		Model((*CollectionMergeSuggestion)(nil)).
		Where("id = ?", id).
		Where("user_id = ?", userID).
		Exists(ctx)
	if err != nil {
		return false, errorx.Decorate(err, "unable to check existence")
	}

	return exists, nil
}

func MergeCMSOfUser(ctx context.Context, target, userID uuid.UUID, cols []uuid.UUID, db bun.IDB) error {
	_, err := db.QueryContext(ctx, `
		CREATE TEMPORARY TABLE tmp_collection_members (
			id UUID NOT NULL
		)
		ON COMMIT DROP
	`)
	if err != nil {
		return errorx.Decorate(err, "unable to create temporary table")
	}

	type tmpCollectionMember struct {
		bun.BaseModel `bun:"tmp_collection_members"`

		ID uuid.UUID `bun:",type:uuid"`
	}

	// filter target from cols, if the user did not submit it correctly
	cols = lo.Filter(cols, func(id uuid.UUID, _ int) bool {
		return id != target
	})

	collectionIDS := lo.Map(cols, func(id uuid.UUID, _ int) tmpCollectionMember {
		return tmpCollectionMember{
			ID: id,
		}
	})

	_, err = db.NewInsert().
		Column("id").
		Model(&collectionIDS).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert data into tmp table")
	}

	// We first move all Sub-Collections to the new parent
	colCTE := collectionAuthCTELvl(userID, AccessMaintainer, db)

	selIDs := db.NewSelect().
		Column("id").
		TableExpr("tmp_collection_members")

	colsSel := db.NewSelect().
		With("collections_cte", colCTE).
		Column("col.id").
		TableExpr(`"collections_cte" AS "col"`).
		Where("col.id IN (?)", selIDs)

	dataSelect := db.NewSelect().
		With("cols", colsSel).
		Column("id").
		TableExpr(`"cols" AS c`).
		Where("id IS NOT NULL")

	_, err = db.NewUpdate().
		Model((*Collection)(nil)).
		Set("parent_id = ?", target).
		Set("updated_at = NOW()").
		Where("parent_id IN (?)", dataSelect).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to update sub-collections")
	}

	// now we can move all the bookmarks of the collection
	_, err = db.NewUpdate().
		Model((*Bookmark)(nil)).
		Set("collection_id = ?", target).
		Where("collection_id IN (?)", dataSelect).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to move bookmarks")
	}

	_, err = db.QueryContext(ctx, `
		CREATE TEMPORARY TABLE tmp_duplicate_bookmarks (
		  id UUID PRIMARY KEY NOT NULL DEFAULT uuid_generate_v4(),
		  bookmark_ids UUID[] NOT NULL
		)
		ON COMMIT DROP
	`)
	if err != nil {
		return errorx.Decorate(err, "unable to create temporary table for deduplication")
	}

	_, err = db.QueryContext(ctx, `
		INSERT INTO tmp_duplicate_bookmarks(bookmark_ids)
		SELECT data.bookmark_ids
		FROM (
		  SELECT
		    ARRAY_AGG(id) AS bookmark_ids,
		    url
		  FROM
		    bookmark
		  WHERE collection_id = ?
		  GROUP BY (url)
		  HAVING COUNT(url) > 1
		) data;
	`, target)
	if err != nil {
		return errorx.Decorate(err, "unable to insert data for deduplication")
	}

	_, err = db.QueryContext(ctx, `
		SELECT process_duplicate_bookmarks()
	`)
	if err != nil {
		return errorx.Decorate(err, "unable to execute deduplication")
	}

	// finally, we delete all the old collections
	_, err = db.NewDelete().
		TableExpr("collection").
		Where("id IN (?)", dataSelect).
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete old collections")
	}

	return nil
}

// CreateCollectionMergeSuggestion generates merge suggestions for the user.
func CreateCollectionMergeSuggestion(ctx context.Context, userID uuid.UUID, resetAll bool) error {
	db, err := StartTx(ctx, nil)
	if err != nil {
		return errorx.Decorate(err, "unable to create transaction")
	}
	defer db.Rollback()

	del := db.NewDelete().
		Model((*CollectionMergeSuggestion)(nil)).
		Where("user_id = ?", userID)

	if !resetAll {
		del.Where("dismissed IS FALSE")
	}

	_, err = del.Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to delete prev. suggestions")
	}

	selQ := db.NewSelect().
		ColumnExpr("?::UUID       AS user_id", userID).
		ColumnExpr("LOWER(title)  AS name").
		ColumnExpr("ARRAY_AGG(id) AS collection_ids").
		Table("collection").
		Where("owner_id = ?", userID).
		GroupExpr("name").
		Having("COUNT(1) > 1")

	// NOTE: As a simple "quick-and-dirty" solution, we process the results in go...
	type tmpData struct {
		UserID         uuid.UUID   `bun:"user_id,type:uuid"`
		SuggestionName string      `bun:"name"`
		CollectionIDs  []uuid.UUID `bun:"collection_ids,array"`
	}

	res := make([]tmpData, 0, 100)

	_, err = db.NewSelect().
		With("suggestions", selQ).
		TableExpr("suggestions s").
		Exec(ctx, &res)
	if err != nil {
		return errorx.Decorate(err, "unable to create suggestions")
	}

	for i := range res {
		tmpRes := []string{}

		err := db.NewSelect().
			ColumnExpr("enqueue_collection_merge_suggestions(?, ?, ?)", res[i].UserID, res[i].SuggestionName, pgdialect.Array(res[i].CollectionIDs)).
			Scan(ctx, &tmpRes)
		if err != nil {
			return errorx.Decorate(err, "unable to insert new suggestions into DB")
		}
	}

	return db.Commit()
}

func GetAvailMergeSuggestionsOfUser(ctx context.Context, userID uuid.UUID) (uint32, error) {
	var r uint32

	err := db.NewSelect().
		ColumnExpr("COUNT(1)").
		Table("collection_merge_suggestion").
		Where("user_id = ?", userID).
		Where("dismissed IS FALSE").
		Scan(ctx, &r)
	if err != nil {
		return 0, errors.Wrap(err, "unable to count avail. collection merge suggestions")
	}

	return r, nil
}

type RawCollectionMergeSuggestions struct {
	// ID is the [CollectionMergeSuggestion] ID.
	ID             uuid.UUID `bun:"id,type:uuid" json:"id"`
	SuggestionName string    `bun:"suggestion_name" json:"suggestion_name"`
	Dismissed      bool      `bun:"dismissed" json:"dismissed"`
	// CollectionIDs holds all the suggested collection IDs.
	CollectionIDs []uuid.UUID `bun:"collection_ids,array" json:"collection_ids"`
}

func GetCollectionMergeSuggestions(ctx context.Context, userID uuid.UUID, showDismissed bool) ([]RawCollectionMergeSuggestions, error) {
	ret := make([]RawCollectionMergeSuggestions, 0, 12)

	err := db.NewSelect().
		ColumnExpr(`cms."id"`).
		ColumnExpr(`cms."dismissed"`).
		ColumnExpr(`cms."name"                   AS suggestion_name`).
		ColumnExpr(`ARRAY_AGG(ref.collection_id) AS collection_ids`).
		TableExpr("collection_merge_suggestion cms").
		Join(`
		INNER JOIN collection_merge_suggestion_ref ref
		  ON ref.cms_id = cms.id
		`).
		Where(`cms."user_id" = ?`, userID).
		Where("cms.dismissed = ?", showDismissed).
		GroupExpr(`cms."id"`).
		Order("suggestion_name ASC").
		Limit(100). // TODO: Implement pagination
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to retrieve suggestions")
	}

	return ret, nil
}
