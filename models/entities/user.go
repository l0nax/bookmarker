package entities

import (
	"context"

	"github.com/google/uuid"

	"gitlab.com/l0nax/bookmarker/models"
)

// GetUserByMail tries to resolve the user from the cache and resorts to query the database if
// it is not cached.
func GetUserByMail(ctx context.Context, mail string) (models.User, error) {
	// TODO: Add caching

	return models.GetUserByMail(ctx, mail)
}

// GetUserByID tries to resolve the user from the cache and resorts to query the database if
// it is not cached.
func GetUserByID(ctx context.Context, id uuid.UUID) (models.User, error) {
	// TODO: Add caching

	return models.GetUserByID(ctx, id)
}
