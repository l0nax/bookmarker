package models

import (
	"context"
	"encoding/json"
	"net/url"
	"time"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"
	"gitlab.fabmation.info/20043/null"
)

type Annotation struct {
	bun.BaseModel `bun:"annotation"`

	ID        uuid.UUID `bun:",pk,nullzero,type:uuid" json:"id,omitempty"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"created_at,omitempty"`
	UpdatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"updated_at,omitempty"`
	// OwnerID holds the owner ID.
	//
	// NOTE: This field will not show up if marshalled into a JSON object for security reasons.
	OwnerID uuid.UUID `json:"-"`

	// URL is the annotation URL, used to point directly to the Highlighted element.
	//
	// Example:
	//   rheingoldheavy.com/kicad-bom-management-part-2-clean-exports/#1678815233310
	URL       string      `json:"url"`
	PageTitle null.String `json:"page_title,omitempty"`
	// PageURL is the URL of the page.
	PageURL    string        `json:"page_url"`
	Body       null.String   `json:"body,omitempty"`
	Comment    null.String   `json:"comment,omitempty"`
	BookmarkID uuid.NullUUID `json:"bookmark_id,omitempty"`

	Anchor *HighlightAnchor `bun:"rel:has-one,join:id=annotation_id" json:"selector,omitempty"`
}

type HighlightAnchor struct {
	bun.BaseModel `bun:"highlight_anchor"`

	ID           uuid.UUID         `bun:",pk,nullzero,type:uuid" json:"id,omitempty"`
	AnnotationID uuid.UUID         `bun:"annotation_id,pk,nullzero,type:uuid" json:"annotation_id,omitempty"`
	Quote        string            `json:"quote"`
	Descriptor   *AnchorDescriptor `bun:"rel:has-one,join:id=anchor_id" json:"descriptor"`
}

type AnchorDescriptor struct {
	bun.BaseModel `bun:"highlight_anchor_descriptor"`

	AnchorID uuid.UUID       `bun:",pk,nullzero,type:uuid" json:"anchor_id,omitempty"`
	Strategy string          `bun:",type:text" json:"strategy"`
	Content  json.RawMessage `bun:",type:jsonb" json:"content"`
}

func (a *Annotation) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	var err error

	a.PageURL, err = normalizeURL(a.PageURL)
	if err != nil {
		return errors.Wrap(err, "unable to normalize URL")
	}

	_, err = db.NewInsert().
		Model(a).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to insert annotation")
	} else if a.Anchor == nil {
		return nil
	}

	a.Anchor.AnnotationID = a.ID

	_, err = db.NewInsert().
		Model(a.Anchor).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to insert annotation anchor")
	} else if a.Anchor.Descriptor == nil {
		return nil
	}

	a.Anchor.Descriptor.AnchorID = a.Anchor.ID

	_, err = db.NewInsert().
		Model(a.Anchor.Descriptor).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to insert annotation descriptor")
	}

	return nil
}

func (a *Annotation) Update(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	var err error

	a.PageURL, err = normalizeURL(a.PageURL)
	if err != nil {
		return errors.Wrap(err, "unable to normalize URL")
	}

	q := db.NewUpdate().
		Model(a).
		Set("comment = ?", a.Comment).
		Where("owner_id = ?", a.OwnerID)

	if a.ID != uuid.Nil {
		q.Where("id = ?", a.ID)
	} else {
		q.Where("url = ?", a.URL)
	}

	_, err = q.
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to insert annotation")
	}

	return nil
}

func (a *Annotation) IsValid() bool {
	// TODO: Implement validation
	return a.OwnerID != uuid.Nil
}

func DeleteAnnotationByURL(ctx context.Context, userID uuid.UUID, url string) error {
	_, err := db.NewDelete().
		Model((*Annotation)(nil)).
		Where("owner_id = ?", userID).
		Where("url = ?", url).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to delete annotation")
	}

	return nil
}

func GetAnnotationsOfURL(ctx context.Context, url string, userID uuid.UUID) ([]Annotation, error) {
	var err error
	anot := make([]Annotation, 0, 5)

	url, err = normalizeURL(url)
	if err != nil {
		return nil, errors.Wrap(err, "unable to normalize URL")
	}

	err = db.NewSelect().
		Model((*Annotation)(nil)).
		Relation("Anchor").
		Where("owner_id = ?", userID).
		Where("page_url = ?", url).
		Scan(ctx, &anot)
	if err != nil {
		return nil, errors.Wrap(err, "unable to retrieve annotations")
	}

	for i := range anot {
		if anot[i].Anchor == nil {
			continue
		}

		anot[i].Anchor.Descriptor = &AnchorDescriptor{}

		err = db.NewSelect().
			Model(anot[i].Anchor.Descriptor).
			Where("anchor_id = ?", anot[i].Anchor.ID).
			Scan(ctx)
		if err != nil {
			return nil, errors.Wrap(err, "unable to retrieve anchor descriptor of anchor")
		}
	}

	return anot, nil
}

func GetAnnotationsOfIDURL(ctx context.Context, url string, userID uuid.UUID) (Annotation, error) {
	anot := Annotation{}

	err := db.NewSelect().
		Model((*Annotation)(nil)).
		Relation("Anchor").
		Where("owner_id = ?", userID).
		Where("url = ?", url).
		Scan(ctx, &anot)
	if err != nil {
		return Annotation{}, errors.Wrap(err, "unable to retrieve annotations")
	}

	if anot.Anchor != nil {
		anot.Anchor.Descriptor = &AnchorDescriptor{}

		err = db.NewSelect().
			Model(anot.Anchor.Descriptor).
			Where("anchor_id = ?", anot.Anchor.ID).
			Scan(ctx)
		if err != nil {
			return Annotation{}, errors.Wrap(err, "unable to retrieve anchor descriptor of anchor")
		}
	}

	return anot, nil
}

func normalizeURL(uri string) (string, error) {
	u, err := url.Parse(uri)
	if err != nil {
		return "", errors.Wrap(err, "unable to parse URL")
	}

	u.RawQuery = ""
	u.Fragment = ""
	u.RawFragment = ""

	return u.String(), nil
}
