package models

import (
	"context"
	"sync"

	"github.com/pkg/errors"
	"github.com/uptrace/bun/driver/pgdriver"
)

const (
	ChanBookmarkCreated = "bookmark:created"
	ChanBookmarkDeleted = "bookmark:deleted"
)

type ListenHandler func(*sync.WaitGroup, *pgdriver.Listener)

func startListener(wg *sync.WaitGroup, ctx context.Context, fn ListenHandler) error {
	ln := pgdriver.NewListener(db)

	chans := []string{
		ChanBookmarkCreated,
		ChanBookmarkDeleted,
	}

	if err := ln.Listen(ctx, chans...); err != nil {
		return errors.Wrap(err, "unable to listen to channels")
	}

	wg.Add(1)
	go fn(wg, ln)

	return nil
}
