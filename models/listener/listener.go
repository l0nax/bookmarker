package listener

import (
	"context"
	"sync"

	"github.com/google/uuid"
	"github.com/pkg/errors"
	"github.com/uptrace/bun/driver/pgdriver"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

func NopHandler(*sync.WaitGroup, *pgdriver.Listener) {}

// Handler is the global listen handler.
func Handler(wg *sync.WaitGroup, ln *pgdriver.Listener) {
	defer wg.Done()

	for notif := range ln.Channel(pgdriver.WithChannelSize(1024)) {
		wg.Add(1)
		go func(n pgdriver.Notification) {
			handleChannelNotification(n)
			wg.Done()
		}(notif)
	}
}

func handleChannelNotification(not pgdriver.Notification) {
	var err error

	switch not.Channel {
	case models.ChanBookmarkCreated:
		err = nBookmarkCreated(not.Payload)
	default:
		log.Error("Received notification from unkown channel", zap.String("channel", not.Channel))
		return
	}

	if err != nil {
		log.Error("An error occurred while handling notification from channel", zap.String("channel", not.Channel), zap.Error(err))
	}
}

func nBookmarkCreated(payload string) error {
	ctx := context.Background()

	bid, err := uuid.Parse(payload)
	if err != nil {
		return errors.Wrapf(err, "unable to parse bookmark ID %q", payload)
	}

	b, err := models.GetBookmarkByID(ctx, bid)
	if err != nil {
		return errors.Wrap(err, "unable to retrieve bookmark by ID")
	}

	t, err := tasks.NewWebsiteIndex(b.OwnerID, b)
	if err != nil {
		return errors.Wrap(err, "unable to create task to index website")
	}

	if err := task.Enqueue(t); err != nil {
		return errors.Wrap(err, "unable to enqueue task")
	}

	return nil
}
