package models

import (
	"context"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/uptrace/bun"
	"gitlab.fabmation.info/20000/71168/fabguard-go"

	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

// SubscriptionPlan represents the mapping of our license group to a SaaS subscription.
type SubscriptionPlan int16

// ToLicenseGroup returns the [fabguard.LicenseGroup] its mapped to.
func (s SubscriptionPlan) ToLicenseGroup() fabguard.LicenseGroup {
	switch s {
	case PlanFree:
		return feature.Free
	case PlanPro:
		return feature.ProSaaS
	case PlanEnterprise:
		return feature.Enterprise
	default:
		panic(fmt.Sprintf("unkown subscription plan %d", s))
	}
}

const (
	// PlanFree represents the free version.
	PlanFree = SubscriptionPlan(0)
	// PlanPro represents the [feature.ProSaaS] tier.
	PlanPro = SubscriptionPlan(1)
	// PlanEnterprise represents the [feature.Enterprise] (SaaS) tier.
	PlanEnterprise = SubscriptionPlan(2)
)

// Subscription represents a SaaS subscription.
type Subscription struct {
	bun.BaseModel `bun:"subscription"`

	ID          uuid.UUID        `bun:",pk,nullzero,type:uuid" json:"id"`
	UserID      uuid.UUID        `bun:",nullzero,type:uuid" json:"user_id"`
	PeriodStart time.Time        `bun:",nullzero,notnull,default:current_timestamp" json:"period_start"`
	PeriodEnd   bun.NullTime     `bun:",nullzero" json:"period_end"`
	PlanID      SubscriptionPlan `bun:",type:smallint"`
}

func (s *Subscription) Create(ctx context.Context) error {
	_, err := db.NewInsert().
		Model(s).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert subscription")
	}

	return nil
}

// GetHighestSubscription returns the highes [Subscription] of the user.
func GetHighestSubscription(ctx context.Context, userID uuid.UUID) (Subscription, error) {
	var s Subscription

	err := db.NewSelect().
		Table("subscription").
		Where("user_id = ?", userID).
		Where("period_start <= now()").
		Where("COALESCE(period_end, NOW() + '1 day') > now()").
		Order("plan_id DESC").
		Limit(1).
		Scan(ctx, &s)
	if err != nil {
		return s, errorx.Decorate(err, "unable to retrieve subscription of user")
	}

	return s, nil
}
