package models

import (
	"time"

	"github.com/google/uuid"
	"github.com/uptrace/bun"
)

type Tag struct {
	bun.BaseModel `bun:"tag"`

	ID          uuid.UUID `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt   time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	OwnerID     uuid.UUID `bun:",type:uuid"`
	Name        string
	Description string `bun:",type:text,nullzero"`
}
