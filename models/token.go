package models

import (
	"context"
	"crypto/rand"
	"log"
	"time"
	"unsafe"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"
	"go.uber.org/zap"
)

type Token struct {
	bun.BaseModel `bun:"token"`

	ID        uuid.UUID `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	UserID    uuid.UUID `bun:",type:uuid"`
	Token     string    `json:"token"`

	// User holds the referenced user object.
	User User `bun:"rel:belongs-to,join:user_id=id" json:"-"`
}

func GetFullTokenByToken(ctx context.Context, token string) (Token, error) {
	var t Token

	err := db.NewSelect().
		Model(&t).
		Relation("user").
		Where("token = ?", token).
		Scan(ctx)

	return t, errorx.Decorate(err, "unable to retrieve token")
}

func GetTokenByString(ctx context.Context, token string) (Token, error) {
	var ret Token

	err := db.NewSelect().
		Model((*Token)(nil)).
		Where("token = ?", token).
		Relation("User").
		Scan(ctx, &ret)
	if err != nil {
		return Token{}, errorx.Decorate(err, "no token found")
	}

	return ret, nil
}

func CreateToken(ctx context.Context, userID uuid.UUID) (Token, error) {
	ret := Token{
		UserID: userID,
		Token:  RandString(30),
	}

	_, err := db.NewInsert().
		Model(&ret).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return Token{}, errorx.Decorate(err, "unable to create new token")
	}

	return ret, nil
}

func GetTokensOfUser(ctx context.Context, userID uuid.UUID) ([]Token, error) {
	ret := make([]Token, 0, 10)

	err := db.NewSelect().
		Model((*Token)(nil)).
		Column("id").
		Column("created_at").
		Where("user_id = ?", userID).
		Limit(100).
		Scan(ctx, &ret)
	if err != nil {
		return nil, errors.Wrap(err, "unable to retrieve tokens")
	}

	return ret, nil
}

func RevokeTokenOfUser(ctx context.Context, userID, tokenID uuid.UUID) error {
	_, err := db.NewDelete().
		Model((*Token)(nil)).
		Where("id = ?", tokenID).
		Where("user_id = ?", userID).
		Exec(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to delete token")
	}

	return nil
}

// RandString generates a random string with n characters.
// If the generation fails, the function panics to prevent security problems.
func RandString(n uint) string {
	const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	data := make([]byte, n)

	if _, err := rand.Read(data); err != nil {
		log.Panic("Unable to read random data", zap.Error(err))
	}

	for i, b := range data {
		data[i] = letterBytes[b%byte(len(letterBytes))]
	}

	return *(*string)(unsafe.Pointer(&data))
}
