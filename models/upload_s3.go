package models

import (
	"context"
	"time"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/uptrace/bun"
)

// UploadModelType is the model type used in the [UploadS3] table to reference different
// tables.
type UploadModelType int16

const (
	// UploadModelUser describes the [User] table.
	UploadModelUser UploadModelType = iota + 1
	// UploadModelBookmarkThumbnail describes a thumnail of the [Bookmark] table.
	UploadModelBookmarkThumbnail
)

// UploadS3 holds references of uploaded data.
// Such data can be, for example, a user avatar or a project icon.
type UploadS3 struct {
	bun.BaseModel `bun:"upload_s3"`

	ID        uuid.UUID       `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt time.Time       `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	Size      int64           `bun:",type:bigint" json:"size"`
	Path      string          `json:"path"`
	Checksum  []byte          `bun:",type:bytea" json:"checksum"`
	ModelID   uuid.NullUUID   `bun:",nullzero,type:uuid" json:"model_id"`
	ModelType UploadModelType `bun:",type:smallint" json:"model_type"`
}

func (u *UploadS3) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewInsert().
		Model(u).
		Returning("*").
		Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to insert asset")
	}

	return nil
}

func (u *UploadS3) Update(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	q := db.NewUpdate().
		Model(u).
		Where("id = ?", u.ID).
		Set("size = ?", u.Size)
	if u.Checksum != nil {
		q.Set("checksum = ?", u.Checksum)
	}

	_, err := q.Exec(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to update entry")
	}

	return nil
}

// CheckUploadAssetExists executes the SQL query to check the existence if an asset
// GC job may be executed within the next hour.
func CheckUploadAssetExists(ctx context.Context, path string, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	_, err := db.NewSelect().
		Table("gc_upload_s3_review_queue").
		Where("path = ?", path).
		Where("review_after < now() + INTERVAL '1 hour'").
		For("UPDATE").
		Exists(ctx)
	if err != nil {
		return errorx.Decorate(err, "unable to execute existence check for upload asset entry")
	}

	return nil
}
