package models

import (
	"context"
	"crypto/rand"
	"crypto/subtle"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	"github.com/authorizerdev/authorizer-go"
	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"github.com/uptrace/bun"
	"go.uber.org/zap"
	"golang.org/x/crypto/argon2"

	"gitlab.com/l0nax/bookmarker/internal/log"
)

// Error codes returned by functions implemented in this file.
var (
	ErrUserNotActive       = errors.New("user already inactive")
	ErrUserNotInactive     = errors.New("user already active")
	ErrInvalidPassword     = errors.New("invalid password")
	ErrInvalidMail         = errors.New("invalid mail address")
	ErrUserNotFound        = errors.New("user not found")
	ErrInvalidHash         = errors.New("the encoded hash is not in the correct format")
	ErrIncompatibleVersion = errors.New("incompatible version of argon2")
)

// User represents the User model for the database
type User struct {
	bun.BaseModel `bun:"user_info"`

	ID        uuid.UUID `bun:",pk,nullzero,type:uuid" json:"id"`
	CreatedAt time.Time `bun:",nullzero,notnull,default:current_timestamp" json:"created_at"`
	GivenName string    `bun:",type:text" json:"given_name"`
	Surname   string    `bun:",type:text" json:"surname"`
	Email     string    `bun:"email,type:text" json:"email"`
	Password  string    `bun:"password,type:text" json:"password"`
	Active    bool      `bun:"active" json:"active"`
}

func GetOrCreateUserByMail(ctx context.Context, u *authorizer.User) (User, error) {
	var user User

	err := db.NewSelect().
		Model(&user).
		Where("email = ?", u.Email).
		Scan(ctx)
	if err != nil {
		if !IsNoRows(err) {
			return user, errorx.Decorate(err, "unable to retrieve user")
		}

		return createAuthorizerUser(ctx, u)
	}

	return user, nil
}

func createAuthorizerUser(ctx context.Context, u *authorizer.User) (User, error) {
	randPwd, err := generateRandomBytes(64)
	if err != nil {
		return User{}, errorx.Decorate(err, "unable to generate random bytes")
	}

	user := User{
		Email:    u.Email,
		Active:   true,
		Password: string(randPwd), // NOTE: We simply generate a random password, since this column might get removed in the next few updates
	}

	if u.GivenName != nil {
		user.GivenName = *u.GivenName
	}
	if u.FamilyName != nil {
		user.Surname = *u.FamilyName
	}

	if err := user.Create(ctx); err != nil {
		return User{}, errorx.Decorate(err, "unable to create user")
	}

	// create free subscription
	sub := Subscription{
		UserID:      user.ID,
		PeriodStart: time.Now(),
		PlanID:      PlanFree,
	}
	if err := sub.Create(ctx); err != nil {
		return User{}, errorx.Decorate(err, "unable to insert subscription info")
	}

	return user, nil
}

func GetUserByID(ctx context.Context, id uuid.UUID) (User, error) {
	var user User

	err := db.NewSelect().
		Model(&user).
		Where("id = ?", id).
		Scan(ctx)
	if err != nil {
		return user, errorx.Decorate(err, "unable to retrieve user")
	}

	return user, nil
}

func GetUserByMail(ctx context.Context, mail string) (User, error) {
	var user User

	err := db.NewSelect().
		Model(&user).
		Where("email = ?", mail).
		Scan(ctx)
	if err != nil {
		return user, errorx.Decorate(err, "unable to retrieve user")
	}

	return user, nil
}

// Create creates a user in the database from a user struct
func (u *User) Create(ctx context.Context, dbs ...bun.IDB) error {
	db := getDB(dbs...)

	var err error

	u.ID = uuid.New()

	if (*u).Password == "" {
		return ErrInvalidPassword
	}

	if (*u).Email == "" {
		return ErrInvalidMail
	}

	// hash the password
	paramTmp := &HashParams{}

	u.Password, err = HashPassword(u.Password, &paramTmp)
	if err != nil {
		log.DPanic("Unable to Hash password", zap.Error(err))

		return errors.WithStack(errors.Wrap(err,
			"Error while hashing Password"))
	}

	_, err = db.NewInsert().
		Model(u).
		Returning("*").
		Exec(ctx)

	return err
}

// ==========> Internal Functions <==========

// HashParams contains the parameters for Argon2 to hash the password(s).
type HashParams struct {
	memory     uint32 `json:"memory"`
	iterations uint32 `json:"iterations"`
	threads    uint8  `json:"threads"`
	saltLength uint32 `json:"salt_length"`
	keyLength  uint32 `json:"key_length"`
}

func generateRandomBytes(n uint32) ([]byte, error) {
	ret := make([]byte, n)

	_, err := rand.Read(ret)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

// HashPassword hashes a password with the given Params
// set @p to nil to use the Env Configuration (set by the user), ONLY set @p
// if want to compare two hashes.
func HashPassword(password string, p **HashParams) (string, error) {
	const saltLen = 16

	var param *HashParams

	// generate cryptographically secure salt
	salt, err := generateRandomBytes(saltLen)
	if err != nil {
		return "", err
	}

	if (HashParams{}) == **p {
		param = &HashParams{}

		(*param).iterations = 4
		(*param).memory = 512
		(*param).threads = 4
		(*param).keyLength = 32
		(*param).saltLength = saltLen
	} else {
		param = *p
	}

	// hash the Password
	hash := argon2.IDKey([]byte(password), salt, (*param).iterations,
		(*param).memory, (*param).threads,
		(*param).keyLength)

	// encode salt and hash since they are in binary format
	_salt := base64.RawStdEncoding.EncodeToString(salt)
	_hash := base64.RawStdEncoding.EncodeToString(hash)

	// "encode" all needed data into a ASCII string
	// which is:
	//	$argon2id$v=19$m=65536,t=3,p=2$c29tZXNhbHQ$RdescudvJCsgt3ub+b+dWRWJTmaaJObG
	//
	// * $argon2id				— the variant of Argon2 being used.
	// * $v=19				— the version of Argon2 being used.
	// * $m=65536,t=3,p=2			— the memory (m),
	//					  iterations (t) and
	//					  parallelism (p) parameters being used.
	// * $c29tZXNhbHQ			— the base64-encoded salt,
	//					  using standard base64-encoding
	//					  and no padding.
	// * $RdescudvJCsgt3ub+b+dWRWJTmaaJObG	— the base64-encoded hashed
	//					  password (derived key),
	//					  using standard base64-encoding
	//					  and no padding.
	hashedPwd := fmt.Sprintf("$argon2id$v=%d$m=%d,t=%d,p=%d$%s$%s",
		argon2.Version, (*param).memory, (*param).iterations,
		(*param).threads, _salt, _hash)

	return hashedPwd, nil
}

// decodeArgonHash decodes an Argon2 hash and returns all relevant/ needed data
// to generate an identical hash.
func decodeArgonHash(hash string) (p *HashParams, salt, rawHash []byte, err error) {
	vals := strings.Split(hash, "$")
	if len(vals) != 6 {
		return nil, nil, nil, ErrInvalidHash
	}

	var version int

	_, err = fmt.Sscanf(vals[2], "v=%d", &version)
	if err != nil {
		return nil, nil, nil, err
	}

	if version != argon2.Version {
		return nil, nil, nil, ErrIncompatibleVersion
	}

	p = &HashParams{}

	_, err = fmt.Sscanf(vals[3], "m=%d,t=%d,p=%d", &p.memory, &p.iterations,
		&p.threads)
	if err != nil {
		return nil, nil, nil, err
	}

	salt, err = base64.RawStdEncoding.DecodeString(vals[4])
	if err != nil {
		return nil, nil, nil, err
	}

	p.saltLength = uint32(len(salt))

	rawHash, err = base64.RawStdEncoding.DecodeString(vals[5])
	if err != nil {
		return nil, nil, nil, err
	}

	p.keyLength = uint32(len(rawHash))

	return p, salt, rawHash, nil
}

// ComparePasswordAndHash compares an raw Password and an Argon2 hash
func ComparePasswordAndHash(password, encodedHash string) (match bool, err error) {
	// Extract the parameters, salt and derived key from the encoded password
	// hash.
	p, salt, hash, err := decodeArgonHash(encodedHash)
	if err != nil {
		return false, err
	}

	// Derive the key from the other password using the same parameters.
	otherHash := argon2.IDKey([]byte(password), salt, p.iterations, p.memory,
		p.threads, p.keyLength)

	// Check that the contents of the hashed passwords are identical. Note
	// that we are using the subtle.ConstantTimeCompare() function for this
	// to help prevent timing attacks.
	if subtle.ConstantTimeCompare(hash, otherHash) == 1 {
		return true, nil
	}

	return false, nil
}
