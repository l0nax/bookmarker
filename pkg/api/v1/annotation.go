package v1

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"gitlab.fabmation.info/20043/null"
)

type AnnotationIDDeleteReq struct {
	// URL is the generated page URL.
	//
	// Example:
	//   rheingoldheavy.com/kicad-bom-management-part-2-clean-exports/#1678815233310
	URL string `json:"url,omitempty"`
}

func (a AnnotationIDDeleteReq) IsValid() bool {
	return a.URL != ""
}

type AnnotationIDGetReq struct {
	// PageURL is the generated page URL.
	//
	// Example:
	//   rheingoldheavy.com/kicad-bom-management-part-2-clean-exports/#1678815233310
	PageURL string `json:"page_url,omitempty"`
}

func (a AnnotationIDGetReq) IsValid() bool {
	return a.PageURL != ""
}

type AnnotationGetReq struct {
	PageURL string `json:"page_url,omitempty"`
}

func (a AnnotationGetReq) IsValid() bool {
	return a.PageURL != ""
}

type Annotation struct {
	ID        uuid.UUID `json:"id,omitempty"`
	CreatedAt time.Time `json:"created_at,omitempty"`
	UpdatedAt time.Time `json:"updated_at,omitempty"`
	// OwnerID holds the owner ID.
	//
	// NOTE: This field will not show up if marshalled into a JSON object for security reasons.
	OwnerID uuid.UUID `json:"-,omitempty"`

	// URL is the annotation URL, used to point directly to the Highlighted element.
	URL       string      `json:"url,omitempty"`
	PageTitle null.String `json:"page_title,omitempty"`
	// PageURL is the URL of the page.
	PageURL    string        `json:"page_url,omitempty"`
	Body       null.String   `json:"body,omitempty"`
	Comment    null.String   `json:"comment,omitempty"`
	BookmarkID uuid.NullUUID `json:"bookmark_id,omitempty"`

	Anchor *HighlightAnchor `json:"selector,omitempty"`
}

type HighlightAnchor struct {
	ID           uuid.UUID         `json:"id,omitempty"`
	AnnotationID uuid.UUID         `json:"annotation_id,omitempty"`
	Quote        string            `json:"quote,omitempty"`
	Descriptor   *AnchorDescriptor `json:"descriptor,omitempty"`
}

type AnchorDescriptor struct {
	AnchorID uuid.UUID       `json:"anchor_id,omitempty"`
	Strategy string          `json:"strategy,omitempty"`
	Content  json.RawMessage `json:"content,omitempty"`
}
