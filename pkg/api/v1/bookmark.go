package v1

import (
	"github.com/google/uuid"
	"github.com/samber/lo"

	"gitlab.com/l0nax/bookmarker/models"
)

type BookmarkSimple struct {
	CollectionID uuid.UUID `json:"collection_id,omitempty"`
	Title        string    `json:"title,omitempty"`
	Description  string    `json:"description,omitempty"`
	URL          string    `json:"url,omitempty"`
}

func (b *BookmarkSimple) IsValid() bool {
	if b.CollectionID == uuid.Nil {
		return false
	}

	return mustNotEmpty(b.Title, b.URL)
}

func (b BookmarkSimple) ToModel() models.Bookmark {
	return models.Bookmark{
		CollectionID: b.CollectionID,
		Title:        b.Title,
		Description:  b.Description,
		URL:          b.URL,
	}
}

type BookmarkCheck struct {
	URL string `json:"url"`
}

func (b BookmarkCheck) IsValid() bool {
	return b.URL != ""
}

type BrokenBookmark struct {
	ID            uuid.UUID `json:"id,omitempty"`
	URL           string    `json:"url,omitempty"`
	Title         string    `json:"title,omitempty"`
	ComponentPath string    `json:"component_path,omitempty"`
}

func BrokenBookmarksFromModels(b []models.BrokenBookmark) []BrokenBookmark {
	return lo.Map(b, func(item models.BrokenBookmark, _ int) BrokenBookmark {
		return BrokenBookmark{
			ID:            item.ID,
			URL:           item.URL,
			Title:         item.Title,
			ComponentPath: item.ComponentPath,
		}
	})
}
