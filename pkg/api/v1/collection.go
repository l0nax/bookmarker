package v1

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"

	"gitlab.com/l0nax/bookmarker/models"
)

type CollectionBookmarks struct {
	ID        uuid.UUID     `json:"id,omitempty"`
	CreatedAt time.Time     `json:"created_at,omitempty"`
	UpdatedAt time.Time     `json:"updated_at,omitempty"`
	OwnerID   uuid.UUID     `json:"owner_id,omitempty"`
	ParentID  uuid.NullUUID `json:"parent_id,omitempty"`
	Title     string        `json:"title,omitempty"`

	Bookmarks []models.Bookmark `json:"bookmarks"`
}

type Collection struct {
	ID        uuid.UUID     `json:"id,omitempty"`
	CreatedAt time.Time     `json:"created_at,omitempty"`
	UpdatedAt time.Time     `json:"updated_at,omitempty"`
	OwnerID   uuid.UUID     `json:"owner_id,omitempty"`
	ParentID  uuid.NullUUID `json:"parent_id,omitempty"`
	Title     string        `json:"title,omitempty"`
}

func (c Collection) ToModel() models.Collection {
	return models.Collection{
		ID:        c.ID,
		CreatedAt: c.CreatedAt,
		UpdatedAt: c.UpdatedAt,
		OwnerID:   c.OwnerID,
		ParentID:  c.ParentID,
		Title:     c.Title,
	}
}

func (c Collection) IsValid() bool {
	return true
}

// Optional is a helper type allowing to distinguish between
// an empty value (or null) and a not defined one.
//
// NOTE: The current way of using a pointer might not be performant
// for UUID and needs further investigation.
type Optional[T any] struct {
	Defined bool
	Value   *T
}

// UnmarshalJSON is implemented by deferring to the wrapped type (T).
// It will be called only if the value is defined in the JSON payload.
func (o *Optional[T]) UnmarshalJSON(data []byte) error {
	o.Defined = true
	return json.Unmarshal(data, &o.Value)
}

type CollectionUpdateRequest struct {
	ID       uuid.UUID               `json:"id,omitempty"`
	ParentID Optional[uuid.NullUUID] `json:"parent_id,omitempty"`
	Title    string                  `json:"title,omitempty"`
}

func (c CollectionUpdateRequest) IsValid() bool {
	return !(c.Title == "" && !c.ParentID.Defined)
}

func (c CollectionUpdateRequest) ToModel() models.Collection {
	col := models.Collection{
		ID:    c.ID,
		Title: c.Title,
	}

	col.ParentID.Valid = c.ParentID.Defined
	if c.ParentID.Defined && c.ParentID.Value != nil {
		col.ParentID.UUID = c.ParentID.Value.UUID
	}

	return col
}
