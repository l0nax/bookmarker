package v1

type UserDashboardStats struct {
	// CollectionMergeSuggestions holds the number of collection merge suggestions.
	CollectionMergeSuggestions uint32 `json:"collection_merge_suggestions,omitempty"`

	DeadLinks uint32 `json:"dead_links,omitempty"`
}
