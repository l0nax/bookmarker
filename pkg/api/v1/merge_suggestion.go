package v1

import (
	"github.com/google/uuid"

	"gitlab.com/l0nax/bookmarker/models"
)

// CollectionMergeSuggestionList holds the list of collection merge suggestions.
type CollectionMergeSuggestionList struct {
	// Suggestions holds the raw data.
	Suggestions []models.RawCollectionMergeSuggestions `json:"suggestions"`
	// Mapping holds the mapping from a collection ID to its full path.
	Mapping map[uuid.UUID]string `json:"mapping"`
}

type CMSMergeRequest struct {
	ID          uuid.UUID   `json:"id"`
	Collections []uuid.UUID `json:"collections"`
	Target      uuid.UUID   `json:"target"`
}

func (c CMSMergeRequest) IsValid() bool {
	return c.ID != uuid.Nil && len(c.Collections) > 0 && c.Target != uuid.Nil
}

type CMSUpdateRequest struct {
	ID        uuid.UUID `json:"id"`
	Dismissed bool      `json:"dismissed"`
}
