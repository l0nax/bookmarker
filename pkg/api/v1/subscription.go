package v1

import (
	"time"

	"github.com/uptrace/bun"
	"gitlab.com/l0nax/bookmarker/models"
)

type UserSubscription struct {
	CurrentPlan models.SubscriptionPlan `json:"current_plan"`

	PeriodStart time.Time    `json:"period_start"`
	PeriodEnd   bun.NullTime `json:"period_end,omitempty"`
}
