package v1

type RegisterReq struct {
	GivenName string `json:"given_name,omitempty"`
	Surname   string `json:"surname,omitempty"`
	Username  string `json:"username,omitempty"`
	Email     string `json:"email,omitempty"`
	Password  string `json:"password,omitempty"`
}

func (r RegisterReq) IsValid() bool {
	return mustNotEmpty(r.GivenName, r.Surname, r.Username, r.Email, r.Password)
}
