package v1

func mustNotEmpty(strs ...string) bool {
	for i := range strs {
		if strs[i] == "" {
			return false
		}
	}

	return true
}
