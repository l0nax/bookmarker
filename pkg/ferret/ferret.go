package ferret

import (
	"embed"
	"time"
	"unsafe"

	"github.com/go-resty/resty/v2"
	"github.com/joomcode/errorx"

	"gitlab.com/l0nax/bookmarker/internal/config"
)

// schemaName is the name of a ferret script.
type schemaName string

const (
	ScriptScreenshot   = schemaName("screenshot.fql")
	ScriptFetchContent = schemaName("website_content.fql")
)

//go:embed fql/*.fql
var scripts embed.FS

var c *resty.Client

func Init() {
	c = resty.New().
		SetBaseURL(config.C.Ferret.Worker).
		SetTimeout(1 * time.Minute)
}

type ferretRequest struct {
	Text   string            `json:"text"`
	Params map[string]string `json:"params"`
}

func Exec(script schemaName, params map[string]string) (string, error) {
	return execHelper[string](script, params)
}

func ExecArray(script schemaName, params map[string]string) ([]string, error) {
	return execHelper[[]string](script, params)
}

func execHelper[T any](script schemaName, params map[string]string) (T, error) {
	var res T

	data, err := scripts.ReadFile("fql/" + string(script))
	if err != nil {
		return res, errorx.Decorate(err, "unable to read script")
	}

	reqData := ferretRequest{
		Text:   byteSlice2String(data),
		Params: params,
	}

	_, err = c.R().
		SetBody(&reqData).
		SetResult(&res).
		Post("")
	if err != nil {
		return res, errorx.Decorate(err, "unable to retrieve data from Ferret")
	}

	return res, nil
}

// byteSlice2String converts a byte slice to a string in a performant way.
func byteSlice2String(bs []byte) string {
	return *(*string)(unsafe.Pointer(&bs))
}
