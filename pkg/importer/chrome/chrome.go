package chrome

import (
	"context"

	"github.com/google/uuid"
	"github.com/joomcode/errorx"
	"github.com/uptrace/bun"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/task"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

type EntryType string

const (
	TypeFolder = EntryType("folder")
	TypeURL    = EntryType("url")
)

type Entry struct {
	Name     string    `json:"name,omitempty"`
	Type     EntryType `json:"type,omitempty"`
	URL      string    `json:"url,omitempty"`
	Children []Entry   `json:"children,omitempty"`
}

type BookmarkRoot struct {
	Roots map[string]Entry `json:"roots,omitempty"`
}

func Import(ctx context.Context, userID uuid.UUID, b BookmarkRoot) error {
	tx, err := models.StartTx(ctx, nil)
	if err != nil {
		return errorx.Decorate(err, "unable to start transaction")
	}
	defer tx.Rollback()

	for k, v := range b.Roots {
		id, err := importRootEnty(ctx, tx, userID, k)
		if err != nil {
			return err
		}

		if err := importSubColls(ctx, tx, userID, id, v); err != nil {
			return err
		}
	}

	log.Info("Import finished: reclculating collection auths")

	if err := models.RecalcCollectionAuths(ctx, userID); err != nil {
		return errorx.Decorate(err, "unable to recalc user collection auth")
	}

	if err := tx.Commit(); err != nil {
		return errorx.Decorate(err, "unable to commit transaction")
	}

	t, err := tasks.NewCollectionMergeSuggestionTask(userID, true)
	if err != nil {
		log.Error("Unable to create CollectionMergeSuggestionTask", zap.Stringer("user_id", userID), zap.Error(err))

		return nil
	}

	if err := task.Enqueue(t); err != nil {
		log.Error("Unable enqueue CollectionMergeSuggestionTask", zap.Stringer("user_id", userID), zap.Error(err))
	}

	return nil
}

func importRootEnty(ctx context.Context, tx bun.IDB, userID uuid.UUID, title string) (uuid.UUID, error) {
	return importCollection(ctx, tx, userID, uuid.NullUUID{}, title)
}

func importCollection(ctx context.Context, tx bun.IDB, userID uuid.UUID, parentID uuid.NullUUID, title string) (uuid.UUID, error) {
	c := models.Collection{
		OwnerID:  userID,
		ParentID: parentID,
		Title:    title,
	}

	if err := c.Create(ctx, tx); err != nil {
		return uuid.Nil, errorx.Decorate(err, "unable to import collection")
	}

	return c.ID, nil
}

func importSubColls(ctx context.Context, tx bun.IDB, userID, coll uuid.UUID, e Entry) error {
	if e.Type == TypeURL {
		return importBookmark(ctx, tx, userID, coll, e.Name, e.URL)
	} else if e.Type != TypeFolder {
		return nil
	}

	parent := uuid.NullUUID{
		Valid: true,
		UUID:  coll,
	}

	id, err := importCollection(ctx, tx, userID, parent, e.Name)
	if err != nil {
		return errorx.Decorate(err, "unable to import collection")
	}

	for i := range e.Children {
		if err := importSubColls(ctx, tx, userID, id, e.Children[i]); err != nil {
			return errorx.Decorate(err, "unable to import collection children")
		}
	}

	return nil
}

func importBookmark(ctx context.Context, tx bun.IDB, userID, coll uuid.UUID, title, url string) error {
	b := models.Bookmark{
		OwnerID:      userID,
		CollectionID: coll,
		Title:        title,
		URL:          url,
	}
	if b.Title == "" {
		// TODO: crawl the title
		b.Title = "--UNKOWN--"
	}

	if err := b.Create(ctx, tx); err != nil {
		return errorx.Decorate(err, "unable to create bookmark entry")
	}

	tt, err := tasks.NewThumbnailFetchTask(b)
	if err != nil {
		return errorx.Decorate(err, "unable to create task for fetching thumbnail")
	}

	if err := task.Enqueue(tt); err != nil {
		return errorx.Decorate(err, "unable to enqueue task")
	}

	return nil
}
