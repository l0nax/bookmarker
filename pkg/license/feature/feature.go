package feature

import "gitlab.fabmation.info/20000/71168/fabguard-go"

// AdvancedSearch allows to perform a full-text search on the stored bookmarks.
var AdvancedSearch = fabguard.NewFeatureFlag("advanced_search")

// SaasOffering is the flag used by FABMation to distinguish between the SaaS Server
// and a self-hosted one.
var SaasOffering = fabguard.NewFeatureFlag("saas_server")

// NestedCollections allows collections to be nested.
var NestedCollections = fabguard.NewFeatureFlag("nested_collections")

// QuoteMarking allows to mark and comment phrases on a website.
var QuoteMarking = fabguard.NewFeatureFlag("quote_marking")

// CollectionMergeSuggestion provides the user merging suggestions.
var CollectionMergeSuggestion = fabguard.NewFeatureFlag("collection_merge_suggestion")
