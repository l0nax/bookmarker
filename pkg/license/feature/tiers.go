package feature

import "gitlab.fabmation.info/20000/71168/fabguard-go"

// Free represents the free plan.
var Free = fabguard.NewLicenseGroup("free")

// SaaS represents the SaaS license group which is only used by FABMation.
var SaaS = fabguard.NewLicenseGroup("saas_offering").
	Includes(Pro).
	AddFeatureFlag(SaasOffering)

// ProSaaS is the "Pro" tier which is only used by FABMation.
var ProSaaS = fabguard.NewLicenseGroup("pro_saas").
	Includes(Pro).
	Includes(SaaS)

// Enterprise is the "Enterprise" tier (SaaS only).
var Enterprise = fabguard.NewLicenseGroup("enterprise").
	Includes(ProSaaS)

// Pro is the "Pro" tier of the non-SaaS edition.
var Pro = fabguard.NewLicenseGroup("pro").
	AddFeatureFlag(AdvancedSearch).
	AddFeatureFlag(NestedCollections).
	AddFeatureFlag(QuoteMarking).
	AddFeatureFlag(CollectionMergeSuggestion)
