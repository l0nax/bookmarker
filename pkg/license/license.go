package license

import (
	"context"
	"encoding/base64"
	"os"

	"github.com/google/uuid"
	fabguard "gitlab.fabmation.info/20000/71168/fabguard-go"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/internal/version"
	_ "gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

const productID = "f2bbde86-7abd-457a-b7a7-33848551e2da"

// UserResolverFn is a helper function to check whether a user can use the defined feature.
type UserResolverFn func(userID uuid.UUID, feature fabguard.LicenseFeatureFlag) bool

var (
	resUser    UserResolverFn
	ctx        = context.Background()
	productKey = []byte{
		0x0, 0x14, 0x3d, 0x49, 0x43, 0xc1, 0xc6, 0xd0, 0x75, 0x33, 0x6b,
		0x99, 0x9a, 0xdf, 0xf8, 0x22, 0xa8, 0x4b, 0xac, 0xd9, 0x19,
		0x3, 0x0, 0x13, 0x2b, 0xca, 0x67, 0x39, 0xc6, 0x5, 0xb2,
		0xf2, 0xd1, 0x88, 0xf8, 0x4d, 0xe2, 0x96, 0xa8, 0x0, 0xc0,
		0x48, 0x89, 0x0, 0x14, 0x61, 0xb2, 0x47, 0xa7, 0xa0, 0xa9,
		0xb8, 0xf7, 0x13, 0xf3, 0xdb, 0x2d, 0x19, 0xe7, 0x2b, 0xdd,
		0xda, 0x15, 0x50, 0x3, 0x0, 0x14, 0xf9, 0x14, 0x3, 0x79,
		0x21, 0x5b, 0x83, 0x17, 0x1f, 0xc6, 0x2c, 0x13, 0xf8, 0x42,
		0xd0, 0xa9, 0xf7, 0x32, 0xc4, 0x1,
	}
)

func init() {
	fabguard.NewProduct(ctx, productID, version.Version, productKey)

	fabguard.SetShutdownCB(func() {
		log.Warn("Received shutdown action from licensing system: shutdown component")
	})
}

func Load() {
	fabguard.SetStorageLayer(&guardStorage{})

	file := os.Getenv("LICENSE_FILE")
	if file == "" {
		// TODO: Allow "community edition"
		log.Fatal("Missing license file. Please set 'LICENSE_FILE'")
	}

	encData, err := os.ReadFile(file)
	if err != nil {
		log.Fatal("Unable to read license file", zap.String("license_file", file), zap.Error(err))
	}

	data, err := base64.StdEncoding.DecodeString(string(encData))
	if err != nil {
		log.Fatal("Unable to decode license", zap.Error(err))
	}

	lic, err := fabguard.LoadLicense(data)
	if err != nil {
		log.Fatal("Unable to load license", zap.String("license_file", file), zap.Error(err))
	}

	v := lic.IsValid()
	switch v {
	case fabguard.LicenseValid:
		log.Info("License is valid")
	case fabguard.LicenseNeedsActivation:
		log.Fatal("License requires activation...")
	default:
		log.Error("Invalid license provided", zap.Stringer("reason", v))
	}

	log.Info("License successfully loaded!")
}

func SetUserResolver(fn UserResolverFn) {
	resUser = fn
}

// FeatureAvailable returns whether the defined feature flag is available in the specified license or not.
func FeatureAvailable(userID uuid.UUID, feature fabguard.LicenseFeatureFlag) bool {
	return fabguard.LicenseIncludesFeature(feature) && resUser(userID, feature)
}

// FeatureGlobalAvailable returns whether the defined feature flag is available instance wide.
func FeatureGlobalAvailable(feature fabguard.LicenseFeatureFlag) bool {
	return fabguard.LicenseIncludesFeature(feature)
}
