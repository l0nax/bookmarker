package license

import fabguard "gitlab.fabmation.info/20000/71168/fabguard-go"

// guardStorage is the in-memory fabguard storage implementation.
type guardStorage struct {
	activationKey string
}

// LoadActivationKey should return the activation key and a boolean describing if
// the activation key has been stored before.
func (g *guardStorage) LoadActivationKey() (string, bool) {
	return g.activationKey, g.activationKey != ""
}

// SaveActivationKey should save the given activation key.
func (g *guardStorage) SaveActivationKey(key string) error {
	g.activationKey = key

	return nil
}

var _ fabguard.StorageLayer = (*guardStorage)(nil)
