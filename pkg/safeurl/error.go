package safeurl

import (
	"errors"
	"fmt"
)

type BlockedError interface {
	IsBlockedError() bool
}

type AllowedPortError struct {
	port string
}

func (e *AllowedPortError) Error() string {
	return fmt.Sprintf("port: %v not found in allowlist", e.port)
}

func (e *AllowedPortError) IsBlockedError() bool { return true }

type AllowedSchemeError struct {
	scheme string
}

func (e *AllowedSchemeError) Error() string {
	return fmt.Sprintf("scheme: %v not found in allowlist", e.scheme)
}

func (e *AllowedSchemeError) IsBlockedError() bool { return true }

type InvalidHostError struct {
	host string
}

func (e *InvalidHostError) Error() string {
	return fmt.Sprintf("host: %v is not valid", e.host)
}
func (e *InvalidHostError) IsBlockedError() bool { return true }

type AllowedHostError struct {
	host string
}

func (e *AllowedHostError) Error() string {
	return fmt.Sprintf("host: %v not found in allowlist", e.host)
}
func (e *AllowedHostError) IsBlockedError() bool { return true }

type AllowedIPError struct {
	ip string
}

func (e *AllowedIPError) Error() string {
	return fmt.Sprintf("ip: %v not found in allowlist", e.ip)
}
func (e *AllowedIPError) IsBlockedError() bool { return true }

type IPv6BlockedError struct {
	ip string
}

func (e *IPv6BlockedError) Error() string {
	return fmt.Sprintf("ipv6 blocked. connection to %v dropped", e.ip)
}
func (e *IPv6BlockedError) IsBlockedError() bool { return true }

type SendingCredentialsBlockedError struct{}

func (e *SendingCredentialsBlockedError) Error() string {
	return fmt.Sprintf("sending credentials blocked.")
}
func (e *SendingCredentialsBlockedError) IsBlockedError() bool { return true }

func unwrap(err error) error {
	wrapped, ok := err.(interface{ Unwrap() error })
	if !ok {
		return err
	}
	inner := wrapped.Unwrap()
	return unwrap(inner)
}

func IsBlockedError(err error) bool {
	if v, ok := err.(BlockedError); ok && v.IsBlockedError() {
		return true
	}

	for {
		err = errors.Unwrap(err)
		if err == nil {
			return false
		}

		if v, ok := err.(BlockedError); ok && v.IsBlockedError() {
			return true
		}
	}
}
