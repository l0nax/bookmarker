package safeurl

import (
	"fmt"
	"net"
	"syscall"

	"gitlab.com/l0nax/bookmarker/internal/config"
	"gitlab.com/l0nax/bookmarker/internal/log"
	"go.uber.org/zap"
)

func BuildTransportControlFunc() func(network, address string, c syscall.RawConn) error {
	return func(network, address string, _ syscall.RawConn) error {
		log.Debug("Connecting to external address", zap.String("address", address))

		if config.C.SSRF.DisableIPv6 && network == "tcp6" {
			log.Debug("IPv6 connections have been disabled in SSRF config block")

			return &IPv6BlockedError{
				ip: address,
			}

		}

		host, port, _ := net.SplitHostPort(address)

		if !isPortAllowed(port, config.C.SSRF.AllowedPorts) {
			log.Debug("Connection to site has disallowed port", zap.String("address", address), zap.String("port", port))

			return &AllowedPortError{
				port: port,
			}
		}

		ip := net.ParseIP(host)
		if ip == nil {
			panic(fmt.Sprintf("invalid ip: %v", host))
		}

		if isIPBlocked(ip) {
			log.Debug("Connection to site results in blocked IP", zap.Stringer("ip", ip))

			return &AllowedIPError{
				ip: ip.String(),
			}
		}

		return nil
	}
}
