package task

import (
	"log"

	"github.com/hibiken/asynq"
	"github.com/rueian/rueidis"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/config"
	"gitlab.com/l0nax/bookmarker/pkg/task/tasks"
)

var (
	client *asynq.Client
	srv    *asynq.Server
)

func Init(_ rueidis.Client) {
	var opt asynq.RedisConnOpt

	opt = asynq.RedisClientOpt{
		Addr: config.C.Redis.Address[0],
	}
	if len(config.C.Redis.Address) > 1 {
		opt = asynq.RedisClusterClientOpt{
			Addrs: config.C.Redis.Address,
		}
	}

	client = asynq.NewClient(opt)
	tasks.SetClient(client)
}

func InitWorker(_ rueidis.Client) {
	var opt asynq.RedisConnOpt

	opt = asynq.RedisClientOpt{
		Addr: config.C.Redis.Address[0],
	}
	if len(config.C.Redis.Address) > 1 {
		opt = asynq.RedisClusterClientOpt{
			Addrs: config.C.Redis.Address,
		}
	}

	srv = asynq.NewServer(
		opt,
		asynq.Config{
			Concurrency: 20,
			Queues: map[string]int{
				tasks.PrioCritical: 10,
				tasks.PrioDefault:  5,
				tasks.PrioLow:      1,
			},
			StrictPriority: true,
		},
	)

	mux := asynq.NewServeMux()
	mux.HandleFunc(tasks.TFetchThumbnail, tasks.HandleThumbnailFetch)
	mux.HandleFunc(tasks.TIndexWebsite, tasks.HandleWebsiteFetch)
	mux.HandleFunc(tasks.TUpdateUserCollectionAuth, tasks.HandleRecalcCollectionAuth)
	mux.HandleFunc(tasks.TGenerateCollectionMergeSuggestions, tasks.HandleCollectionMergeSuggestionTask)
	mux.HandleFunc(tasks.TIndexMissingWebsites, tasks.HandleWebsiteIndexMissing)
	mux.HandleFunc(tasks.TWebsiteLivenessCheck, tasks.HandleWebsiteLivenessCheck)
	mux.HandleFunc(tasks.TScheduleWebsiteCheck, tasks.HandleScheduleWebsiteCheck)

	if err := srv.Start(mux); err != nil {
		log.Fatal("Unable to start asynq server", zap.Error(err))
	}
}

func Enqueue(t *asynq.Task) error {
	_, err := client.Enqueue(t)

	return err
}
