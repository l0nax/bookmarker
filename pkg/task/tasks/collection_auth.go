package tasks

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
)

type RecalcCollectionAuthTask struct {
	UserID uuid.UUID `json:"user_id"`
}

func NewRecalcCollectionAuth(userID uuid.UUID) (*asynq.Task, error) {
	obj := RecalcCollectionAuthTask{
		UserID: userID,
	}

	jobData, err := json.Marshal(obj)
	if err != nil {
		return nil, errors.Wrap(err, "unable to encode job data")
	}

	return asynq.NewTask(TUpdateUserCollectionAuth, jobData,
		asynq.Queue(PrioCritical), asynq.ProcessIn(time.Second),
		asynq.Unique(time.Hour), asynq.TaskID(TUpdateUserCollectionAuth+userID.String()),
	), nil
}

func HandleRecalcCollectionAuth(ctx context.Context, t *asynq.Task) error {
	var job RecalcCollectionAuthTask

	if err := json.Unmarshal(t.Payload(), &job); err != nil {
		log.Error("Unable to decode job description of RecalcCollectionAuth task", zap.Error(err))

		return err
	}

	log.Debug("Recalculating collection_authorization for user", zap.Stringer("user_id", job.UserID))

	if err := models.RecalcCollectionAuths(ctx, job.UserID); err != nil {
		return errors.Wrap(err, "unable to recalculate CTE")
	}

	return nil
}
