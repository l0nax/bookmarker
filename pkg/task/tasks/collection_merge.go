package tasks

import (
	"context"
	"encoding/json"
	"time"

	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/license"
	"gitlab.com/l0nax/bookmarker/pkg/license/feature"
)

type CollectionMergeSuggestionTask struct {
	UserID   uuid.UUID `json:"user_id"`
	ResetAll bool      `json:"reset_all"`
}

func NewCollectionMergeSuggestionTask(userID uuid.UUID, resetAll bool) (*asynq.Task, error) {
	obj := CollectionMergeSuggestionTask{
		UserID:   userID,
		ResetAll: resetAll,
	}

	jobData, err := json.Marshal(obj)
	if err != nil {
		return nil, errors.Wrap(err, "unable to encode job data")
	}

	return asynq.NewTask(TGenerateCollectionMergeSuggestions, jobData,
		asynq.Queue(PrioDefault),
		asynq.Unique(12*time.Hour), asynq.TaskID(TGenerateCollectionMergeSuggestions+userID.String()),
	), nil
}

func HandleCollectionMergeSuggestionTask(ctx context.Context, t *asynq.Task) error {
	var job CollectionMergeSuggestionTask

	if err := json.Unmarshal(t.Payload(), &job); err != nil {
		log.Error("Unable to decode job description of CollectionMergeSuggestionTask task", zap.Error(err))

		return err
	}

	if !license.FeatureAvailable(job.UserID, feature.CollectionMergeSuggestion) {
		log.Info("User cannot run collection merge suggestion", zap.Stringer("user_id", job.UserID))

		return nil
	}

	if err := models.CreateCollectionMergeSuggestion(ctx, job.UserID, job.ResetAll); err != nil {
		return errors.Wrap(err, "unable to generate merge suggestions")
	}

	return nil
}
