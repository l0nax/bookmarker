package tasks

import (
	"context"
	"encoding/json"
	"strings"

	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/pkg/errors"

	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/ferret"
)

func NewWebsiteIndexMissing() (*asynq.Task, error) {
	return asynq.NewTask(TIndexMissingWebsites, nil, asynq.Queue(PrioLow)), nil
}

func HandleWebsiteIndexMissing(ctx context.Context, _ *asynq.Task) error {
	d, err := models.GetWebsitesNotIndexed(ctx)
	if err != nil {
		return errors.Wrap(err, "unable to fetch missing websites")
	}

	for i := range d {
		t, err := NewWebsiteIndex(d[i].OwnerID, d[i])
		if err != nil {
			return errors.Wrap(err, "unable to create website index job")
		}

		if _, err := client.Enqueue(t); err != nil {
			return errors.Wrap(err, "unable to enque website index job")
		}

	}

	return nil
}

type WebsiteIndexTask struct {
	UserID   uuid.UUID
	Bookmark models.Bookmark
}

func NewWebsiteIndex(uid uuid.UUID, b models.Bookmark) (*asynq.Task, error) {
	obj := WebsiteIndexTask{
		UserID:   uid,
		Bookmark: b,
	}

	jobData, err := json.Marshal(obj)
	if err != nil {
		return nil, errors.Wrap(err, "unable to encode job data")
	}

	return asynq.NewTask(TIndexWebsite, jobData, asynq.Queue(PrioDefault)), nil
}

func HandleWebsiteFetch(ctx context.Context, t *asynq.Task) error {
	var job WebsiteIndexTask

	if err := json.Unmarshal(t.Payload(), &job); err != nil {
		return err
	}

	if err := indexWebsite(ctx, job); err != nil {
		return errors.Wrap(err, "unable to index website")
	}

	return nil
}

func indexWebsite(ctx context.Context, job WebsiteIndexTask) error {
	data, err := ferret.ExecArray(ferret.ScriptFetchContent, map[string]string{
		"url": job.Bookmark.URL,
	})
	if err != nil {
		return errors.Wrap(err, "unable to fetch website content")
	}

	err = models.AddBookmarkWebsiteContent(ctx, job.Bookmark.ID, strings.Join(data, "\n"))
	if err != nil {
		return errors.Wrap(err, "unable to add website_content")
	}

	return nil
}
