package tasks

import (
	"context"
	"encoding/json"
	"net"
	"net/http"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/google/uuid"
	"github.com/hibiken/asynq"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/safeurl"
)

type WebsiteLivenessCheck struct {
	BookmarkID uuid.UUID `json:"bookmark_id,omitempty"`
}

func NewWebsiteLivenessCheck(bookmarkID uuid.UUID) (*asynq.Task, error) {
	obj := WebsiteLivenessCheck{
		BookmarkID: bookmarkID,
	}

	jobData, err := json.Marshal(obj)
	if err != nil {
		return nil, errors.Wrap(err, "unable to encode job data")
	}

	return asynq.NewTask(TWebsiteLivenessCheck, jobData,
		asynq.Queue(PrioDefault), asynq.TaskID(bookmarkID.String()), asynq.Unique(24*time.Hour),
	), nil
}

func HandleWebsiteLivenessCheck(ctx context.Context, t *asynq.Task) error {
	var job WebsiteLivenessCheck

	if err := json.Unmarshal(t.Payload(), &job); err != nil {
		log.Error("Unable to decode job description of WebsiteLivenessCheck task", zap.Error(err))

		return err
	}

	b, err := models.GetBookmarkByID(ctx, job.BookmarkID)
	if err != nil {
		if models.IsNoRows(err) {
			log.Warn("Bookmark does not exist anymore, ignoring")

			return nil
		}

		return err
	}

	isAlive := false

	transport := &http.Transport{
		DialContext: (&net.Dialer{
			Control: safeurl.BuildTransportControlFunc(),
		}).DialContext,
	}

	resp, err := resty.New().
		SetHeader("User-Agent", "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36").
		SetTransport(transport).
		SetBaseURL(b.URL).
		SetRedirectPolicy(resty.FlexibleRedirectPolicy(20)).
		R().Get("")
	if err == nil && resp.StatusCode() >= 200 && resp.StatusCode() <= 399 {
		isAlive = true
	}

	if safeurl.IsBlockedError(err) {
		if err := b.UpdatePrivate(ctx, true); err != nil {
			return errors.Wrap(err, "unable to update liveness check of bookmark")
		}
	}

	if err := b.UpdateLiveness(ctx, isAlive, time.Now()); err != nil {
		return errors.Wrap(err, "unable to update liveness check of bookmark")
	}

	return nil
}

func HandleScheduleWebsiteCheck(ctx context.Context, _ *asynq.Task) error {
	ids, err := models.GetBookmarksToCheck(ctx, 1024)
	if err != nil {
		return errors.Wrap(err, "unable to retrieve bookmarks")
	}

	for _, id := range ids {
		t, err := NewWebsiteLivenessCheck(id)
		if err != nil {
			return err
		}

		if _, err := client.Enqueue(t); err != nil {
			return err
		}
	}

	return nil
}
