package tasks

import (
	"time"

	"github.com/hibiken/asynq"
	"github.com/pkg/errors"

	"gitlab.com/l0nax/bookmarker/internal/config"
)

// periodicTConfigProvider a periodic task config provider.
// It implements [asynq.PeriodicTaskConfigProvider].
type periodicTConfigProvider struct{}

// GetConfigs implements [asynq.PeriodicTaskConfigProvider].
func (p *periodicTConfigProvider) GetConfigs() ([]*asynq.PeriodicTaskConfig, error) {
	return []*asynq.PeriodicTaskConfig{
		{
			Cronspec: "@every 1m",
			Task:     asynq.NewTask(TScheduleWebsiteCheck, []byte{}, asynq.Unique(24*time.Hour)),
		},
	}, nil
}

// RunTaskManager inits and starts the periodic task manager.
func RunTaskManager() error {
	provider := &periodicTConfigProvider{}

	taskManager, err := asynq.NewPeriodicTaskManager(
		asynq.PeriodicTaskManagerOpts{
			RedisConnOpt: asynq.RedisClientOpt{
				Addr: config.C.Redis.Address[0],
			},
			PeriodicTaskConfigProvider: provider,
			SyncInterval:               time.Hour,
		},
	)
	if err != nil {
		return errors.Wrap(err, "unable to create a periodic task manager")
	}

	if err := taskManager.Start(); err != nil {
		return errors.Wrap(err, "unable to start a periodic task manager")
	}

	return nil
}

// exponentialBackoff represents 'Exponential backoff' algorithm which will be used
// to postpone GC processing.
func exponentialBackoff(count int) time.Duration {
	const base = 5 * time.Minute
	const max = 24 * time.Hour

	// this should never happen, but just in case...
	if count < 0 {
		return base
	}

	// avoif int64 overflow
	if count > 30 {
		return max
	}

	backoff := base * time.Duration(1<<uint(count))
	if backoff > max {
		backoff = max
	}

	return backoff
}
