package tasks

import "github.com/hibiken/asynq"

var client *asynq.Client

func SetClient(c *asynq.Client) { client = c }

const (
	PrioCritical = "citical"
	PrioDefault  = "default"
	PrioLow      = "low"
)

const (
	TFetchThumbnail                     = "website:thumbnail:fetch"
	TIndexWebsite                       = "website:index"
	TIndexMissingWebsites               = "website:index:missing"
	TWebsiteLivenessCheck               = "website:liveness:check"
	TUpdateUserCollectionAuth           = "auth:user:collection_cte"
	TGenerateCollectionMergeSuggestions = "collection:merge:suggestion"
	TScheduleWebsiteCheck               = "period:website:check"
)
