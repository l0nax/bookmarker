package tasks

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"image/jpeg"
	"image/png"
	"io"
	"strconv"
	"time"

	"github.com/disintegration/imaging"
	"github.com/hibiken/asynq"
	"github.com/joomcode/errorx"
	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/l0nax/bookmarker/internal/log"
	"gitlab.com/l0nax/bookmarker/internal/s3"
	"gitlab.com/l0nax/bookmarker/models"
	"gitlab.com/l0nax/bookmarker/pkg/ferret"
)

type ThumbnailFetchTask struct {
	Bookmark models.Bookmark
}

func NewThumbnailFetchTask(bookmark models.Bookmark) (*asynq.Task, error) {
	obj := ThumbnailFetchTask{
		Bookmark: bookmark,
	}

	jobData, err := json.Marshal(obj)
	if err != nil {
		return nil, errors.Wrap(err, "unable to encode job data")
	}

	return asynq.NewTask(TFetchThumbnail, jobData,
		asynq.Queue(PrioDefault), asynq.ProcessIn(time.Minute),
		asynq.Unique(12*time.Hour), asynq.TaskID(bookmark.ID.String()),
	), nil
}

func HandleThumbnailFetch(ctx context.Context, t *asynq.Task) error {
	var job ThumbnailFetchTask

	if err := json.Unmarshal(t.Payload(), &job); err != nil {
		log.Error("Unable to decode job description of hook-script-execution", zap.Error(err))

		return err
	}

	// check if bookmark exists
	_, err := models.GetBookmarkByID(ctx, job.Bookmark.ID)
	if err != nil {
		if models.IsNoRows(err) {
			log.Warn("Unable to fetch bookmark thumbnail: Bookmark entry does not exist in DB anymore")

			return nil
		}

		return errorx.Decorate(err, "unable to check if bookmark exists")
	}

	jpeg, err := ferret.Exec(ferret.ScriptScreenshot, map[string]string{
		"url": job.Bookmark.URL,
	})
	if err != nil {
		return errorx.Decorate(err, "unable to fetch thumbnail")
	}

	rawData, err := base64.StdEncoding.DecodeString(jpeg)
	if err != nil {
		return errorx.Decorate(err, "unable to decode JPEG base64 image")
	}

	jpegData := bytes.NewBuffer(rawData)

	png, err := jpegToPNG(jpegData)
	if err != nil {
		if errors.Is(err, io.EOF) {
			log.Error("Received EOF for PNG: skipping image", zap.Error(err))

			return nil
		}

		return errorx.Decorate(err, "unable to convert JPEG to PNG")
	}

	tx, err := models.StartTx(ctx, nil)
	if err != nil {
		return errorx.Decorate(err, "unable to start transaction")
	}
	defer tx.Rollback()

	avTime := time.Now()
	thumbPref := strconv.FormatInt(avTime.Unix(), 10)
	newPath := s3.GenThumbnailPath(job.Bookmark.ID, thumbPref)

	if err := models.CheckUploadAssetExists(ctx, newPath, tx); err != nil {
		return errorx.Decorate(err, "unable to exec existence check")
	}

	asset := models.UploadS3{
		CreatedAt: avTime,
		Path:      newPath,
		Size:      0,
		ModelType: models.UploadModelBookmarkThumbnail,
	}
	if err := asset.Create(ctx, tx); err != nil {
		return errorx.Decorate(err, "unable to create asset")
	}

	size, err := s3.UploadThumbnail(ctx, bytes.NewReader(png), job.Bookmark.ID, thumbPref)
	if err != nil {
		return errorx.Decorate(err, "unable to upload thumbnail image")
	}

	asset.Size = size
	if err := asset.Update(ctx, tx); err != nil {
		return errorx.Decorate(err, "unable to update size of asset")
	}

	job.Bookmark.ThumbnailID.Valid = true
	job.Bookmark.ThumbnailID.UUID = asset.ID

	if err := job.Bookmark.Update(ctx, tx); err != nil {
		return errorx.Decorate(err, "unable to update bookmark entry")
	}

	return tx.Commit()
}

func jpegToPNG(r io.Reader) ([]byte, error) {
	img, err := jpeg.Decode(r)
	if err != nil {
		return nil, errorx.Decorate(err, "unable to decode JPEG image")
	}

	buf := new(bytes.Buffer)
	if err := png.Encode(buf, img); err != nil {
		return nil, errorx.Decorate(err, "unable to re-encode image as PNG")
	}

	pngImg, err := png.Decode(bytes.NewReader(buf.Bytes()))
	if err != nil {
		return nil, errorx.Decorate(err, "unable to decode PNG image")
	}

	resImg := imaging.Resize(pngImg, 100, 57, imaging.Lanczos)

	outBuf := new(bytes.Buffer)
	pngEncoder := png.Encoder{
		CompressionLevel: 6,
	}

	if err := pngEncoder.Encode(outBuf, resImg); err != nil {
		return nil, errorx.Decorate(err, "unable to re-encode PNG image")
	}

	return outBuf.Bytes(), nil
}
