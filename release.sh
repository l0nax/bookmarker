#!/bin/bash

[[ -z ${SKIP_TAG+y} ]] && git tag -a -s "${1}" -m "${1}"

buildah build -t registry.fab/applications/markfolio/server:${1} .
buildah push registry.fab/applications/markfolio/server:${1}
